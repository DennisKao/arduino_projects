#include "PLCParameterModel.h"
#include "ModbusPoll.h"
#include "LedModbusTranslation.h"
#ifdef UBRR3H	// 判斷是mega還是nano
#include "NextionObjectDefine.h"
#include "NextionExtension.h"
#include "ReadLMag.h"
#include "EstimatedEndTime.h"
#endif // UBRR3H



// ---- Modbus ----
//#include <ModbusRtu.h>
#define DATA_LEN 16
uint16_t modbus_data_array[DATA_LEN] = {0}; // 13 ~ 16 要給led指令用

uint16_t total_water_value_left = 3700;
#ifdef UBRR2H
#define MODBUS_SERIAL_PORT 2
#define MODBUS_CONTROL_PIN 31
#else
#define MODBUS_SERIAL_PORT 0
#define MODBUS_CONTROL_PIN 10
#endif // UBRR2H

Modbus slave(3, MODBUS_SERIAL_PORT, MODBUS_CONTROL_PIN);

#ifdef UBRR3H
ReadLMag mag_0 = ReadLMag(1, 1.0, 65);
ReadLMag mag_1 = ReadLMag(2, 1.0, 65);
ReadLMag mag_2 = ReadLMag(3, 1.0, 65);
ReadLMag mag_3 = ReadLMag(4, 1.0, 65);
// ---- Mags ----
ReadLMag *mags[] = {
	&mag_0,
	&mag_1,
	&mag_2,
	&mag_3,
	NULL
};

EstimatedEndTime estimated_end_time = EstimatedEndTime(total_water_value_left, 5);
#endif // UBRR3H

void setup() {
	// modbus
	slave.begin(57600);
	//Led
	Wire.begin();
	// hmi
#ifdef UBRR3H
	Serial.begin(9600);
    nexInit();
	attach_all_hmi_user_func();
	// mag
	readLMagInit(millis(), mags);
	mag_0.send_reset_total_flow_command(master);
	mag_1.send_reset_total_flow_command(master);
	mag_2.send_reset_total_flow_command(master);
	mag_3.send_reset_total_flow_command(master);
	Serial.println("10 inch v2.03 init");
#endif // UBRR3H

#ifdef Estimated_End_Time_Test_mode
	Serial.println("估計水量測試模式啟用中  'Estimated_End_Time_Test_mode' is active ");
#endif // Estimated_End_Time_Test_mode

}


void loop() {
    // ---- Modbus ----
    slave.poll(modbus_data_array, DATA_LEN);
	write_modbus_data_to_struct();
	send_data_to_led_contriller(millis());
	

#ifdef UBRR3H
    // 讀取流量計
	read_plc_parameter();
	modbusDevicePoll(millis());
	magDecoder(mags);
	// 更新
	read_data_from_mag_model();
	update_estimated_end_time();
	// 更新資料到螢幕
	send_data_to_nextion_hmi_loop();
	updata_pto_status();
	debug_tools();
#endif // UBRR3H
	
}

void debug_tools() {
	if (Serial.available()) {
		char cmd[20] = { '\0' };
		int index = 0;
		while (Serial.available()) {
			cmd[index++] = Serial.read();
			delay(5);
		}
		if (strcmp(cmd, "help") == 0) {
			Serial.println("mag_data\t\t顯示流量計資料");
		}
		else if (strcmp(cmd, "mag_data") == 0) {
			Serial.println("mag_data");
#ifdef UBRR3H
			int index = 0;
			while (mags[index] != NULL)
			{
				mags[index++]->print_data();
			}
#endif // UBRR3H
		}
		else if (strcmp(cmd, "modbus") == 0) {
			for (int i = 0; i < 16; i++) {
				Serial.print(modbus_data_array[i]);
				Serial.print("  ");
			}
			Serial.println();
		}
		else if (strcmp(cmd, "modbus") == 0) {
			//mag_0.data.total_positive += 13;
		}
		else if (strcmp(cmd, "pto") == 0) {
			modbus_data_array[0] = 1;
		}
	}
}


#ifdef UBRR3H
void read_plc_parameter() {
	read_mags_amplification_from_modbus();
	// 暫時手動更新全流量
	total_water_value_left = modbus_data_array[9];
	int tmp_total_water_value = total_water_value_left;
	va_tank_total.data_link->send_val_to_hmi = total_water_value_left;
}
void read_mags_amplification_from_modbus() {
	mag_0.amplification = revert_to_type_double(modbus_data_array[3], 4);
	mag_1.amplification = revert_to_type_double(modbus_data_array[4], 4);
	mag_2.amplification = revert_to_type_double(modbus_data_array[5], 4);
	mag_3.amplification = revert_to_type_double(modbus_data_array[6], 4);
}
void read_data_from_mag_model() {
	// 更新及時流量到data link
	int tiny_val_cut = 50;
	va_wout_nL1.data_link->send_val_to_hmi = mag_0.data.instant_flow_value * 60 > tiny_val_cut ? mag_0.data.instant_flow_value * 60 : 0;
	va_wout_nL2.data_link->send_val_to_hmi = mag_1.data.instant_flow_value *60 > tiny_val_cut ? mag_1.data.instant_flow_value * 60 : 0;
	va_wout_nR2.data_link->send_val_to_hmi = mag_2.data.instant_flow_value *60 > tiny_val_cut ? mag_2.data.instant_flow_value * 60 : 0;
	va_wout_nR1.data_link->send_val_to_hmi = mag_3.data.instant_flow_value *60 > tiny_val_cut ? mag_3.data.instant_flow_value * 60 : 0;
	
	// 更新累計流量到data link
	va_wtotal_L1.data_link->send_val_to_hmi = mag_0.data.total_positive*10/1000;	// *10 因為螢幕用了虛擬小數點, /1000 換算 L to m^3
	va_wtotal_L2.data_link->send_val_to_hmi = mag_1.data.total_positive*10/1000;
	va_wtotal_R2.data_link->send_val_to_hmi = mag_2.data.total_positive*10/1000;
	va_wtotal_R1.data_link->send_val_to_hmi = mag_3.data.total_positive*10/1000;
}
double get_damping_flow_value_with_all_mag_flow_meter() {
	double flow_value_with_all_mag_flow_meter = 0;
	int mag_flow_meter_index = 0;
	while (mags[mag_flow_meter_index] != NULL)
	{
		flow_value_with_all_mag_flow_meter  += mags[mag_flow_meter_index]->data.damping_flow_value;
		mag_flow_meter_index++;
	}
	return flow_value_with_all_mag_flow_meter;
}
double get_instant_flow_value_with_all_mag_flow_meter() {
	double flow_value_with_all_mag_flow_meter = 0;
	int mag_flow_meter_index = 0;
	while (mags[mag_flow_meter_index] != NULL)
	{
		flow_value_with_all_mag_flow_meter += mags[mag_flow_meter_index]->data.instant_flow_rate;
		mag_flow_meter_index++;
	}
	return flow_value_with_all_mag_flow_meter;
}

int get_left_total_water_value() {
	uint32_t left_total_water_value_persentage = *va_tank_now.data_link->recive_val_from_modbus_ptr;
	uint32_t tmp_total_water_value = total_water_value_left;
	uint32_t res = tmp_total_water_value * left_total_water_value_persentage/100/10;
	return res;
}
void update_estimated_end_time() {
	double damping_flow_value_with_all_mag_flow_meter = get_damping_flow_value_with_all_mag_flow_meter();
	double instant_flow_value_with_all_mag_flow_meter = get_instant_flow_value_with_all_mag_flow_meter();
	int left_total_water_value = get_left_total_water_value();

	if (instant_flow_value_with_all_mag_flow_meter <= 1) { return; }			// 流量太小的時候不更新預估時間
	estimated_end_time.set_flow_value(damping_flow_value_with_all_mag_flow_meter);
	estimated_end_time.set_total_water_value_left(left_total_water_value);
	va_tank_timer.data_link->send_val_to_hmi = va_tank_timer2.data_link->send_val_to_hmi = estimated_end_time.get_blur_estimate_end_time();
	//Serial.println(estimated_end_time.get_blur_estimate_end_time());
}

void updata_pto_status() {
	int index = 0;
	bool pto_status = *va_pto.data_link->recive_val_from_modbus_ptr == 1 ? true: false;
	while (mags[index] != NULL)
	{
		mags[index++]->set_pto_status(pto_status);
	}
}

/*
* 把虛擬的浮點數還原成浮點數型態
* @param simulation_double 虛擬出的浮點數數 ex. 1.234 記錄成 1234
* @param number_of_digits 要還原的位數 上面的例子就是3位數 
*/
double revert_to_type_double(const uint16_t simulation_double, const byte number_of_digits) {
	uint16_t simulation_double_buffer = simulation_double;
	double result = 0;
	for (byte i = 0; i < number_of_digits; i++) {
		result += simulation_double_buffer % 10;
		result /= 10;
		simulation_double_buffer /= 10;
	}
	result += simulation_double_buffer;
	return result;
}

#endif // UBRR3H
