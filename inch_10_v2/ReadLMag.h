﻿//
// Created by elijah on 2019/3/4.
//

#ifndef LIBRARIES_READMODBUS_H
#define LIBRARIES_READMODBUS_H
#include "Arduino.h"
#ifndef MODBUS_RTU_H
#define MODBUS_RTU_H
#include <ModbusRtu.h>
#endif // !MODBUS_RTU_H


#ifndef DAMPING_TIMES
#define DAMPING_TIMES 40		//取平均次數  正常平均次數(2000ms/次
#endif

#ifndef CALIBRATION_LEN
#define CALIBRATION_LEN 5
#endif // CALIBRATION_LEN

#define FLOW_EMPTY_RATE_PARTITION 4500
//#define DEBUG_MESSAGE // 若不需要查看debug訊息 應註解此行


// ---------------------------------------
// -------------輔助函數-----------------
/**
 * 網路上抓的  列印浮點數的函數
 * @param val   浮點數
 * @param precision     要顯示的位數
 */
void printDouble(double val, byte precision) {
	// prints val with number of decimal places determine by precision
	// precision is a number from 0 to 6 indicating the desired decimial places
	// example: lcdPrintDouble( 3.1415, 2); // prints 3.14 (two decimal places)

	if (val < 0.0) {
		Serial.print('-');
		val = -val;
	}

	Serial.print(int(val));  //prints the int part
	if (precision > 0) {
		Serial.print("."); // print the decimal point
		unsigned long frac;
		unsigned long mult = 1;
		byte padding = precision - 1;
		while (precision--)
			mult *= 10;

		if (val >= 0)
			frac = (val - int(val)) * mult;
		else
			frac = (int(val) - val) * mult;
		unsigned long frac1 = frac;
		while (frac1 /= 10)
			padding--;
		while (padding--)
			Serial.print("0");
		Serial.print(frac, DEC);
	}
}
// ---------------------------------------

// ---------------------------------------
// -----------------class-----------------
/**
 * @struct 紀錄每台流量計的資料集合
 */
struct LMagData {
	float instant_flow_value;   // 瞬時流量(公升/s
	float instant_flow_rate;    // 瞬時流速
	float damping_flow_value;		// 阻尼流量(公升/s
	float damping_flow_rate;		// 阻尼流速
	double total_positive; // 正向累積
};

struct Calibration
{
	float flow_rate;
	float calibration_amplification;
};

class ReadLMag {
public:
	double amplification = 1.0;	// 機器係數
	/**
	* @param		slave_id			設備id
	* @param		amplification 機器係數
	* @param		d					口徑 單位mm
	*/
	ReadLMag(int slave_id, double amplification, int d);
	LMagData data;  // 存放解析完畢的資料
	void print_data();
	void init(unsigned long now);
	void send_reset_total_flow_command(Modbus master);   // 發送流量計歸零訊號
	int quick_change_detect_len = 4;		// 劇烈反應次數  修正檢查長度
	float quick_change_detect_diff = 0.5;		// 快速修正差異值
	/**
	* 設置多點校正
	* @param calibration 校正的數值
	* @param index 第幾筆校正設定
	*/
	void set_calibration(Calibration calibration, int index);
	void show_calibration();

public:
	void request_timeout(unsigned long now); // 等待超時
	void test();    // 測試用
	// test func
	void insert_test_data(float data_array[DAMPING_TIMES]) {
		for (int i = 0; i < DAMPING_TIMES; i++) {
			damping_flow_rate_array[i] = data_array[i];
		}
		this->damping_flow_rate_array_write_index = DAMPING_TIMES - 1;
		this->data.instant_flow_rate = damping_flow_rate_array[DAMPING_TIMES - 1];
	}
	void show_test_damping_flow_rate() {
		this->caculate_damping_flow_rate(10000);
	}
	void set_pto_status(bool enable);
	int get_slave_id();
	uint16_t return_buffer[25];     // 回傳值緩存區
	/**
	 * 解析返回的數值
	 */
	void caculate_flow_rate();
private:
	double total = 0;      // 總流量(---------------
	int slave_id;   // 現在詢問的設備id
	const int damping_flow_rate_array_len = DAMPING_TIMES;
	float damping_flow_rate_array[DAMPING_TIMES] = { 0 };		 // 時間阻尼陣列
	unsigned long  damping_flow_rate_array_time_stamp = 0;
	int damping_flow_rate_array_write_index = 0;
	int dramatic_changes_detect_count = 0;
	float dramatic_changes_detect_buff = 0;
	Calibration calibrations[CALIBRATION_LEN] = { {0, 0} };  // 多點校正
	uint16_t request_cmd[8] = { 0x01, 0x04, 0x10, 0x10, 0x00, 0x16, 0x74, 0xC1 };
	unsigned long last_add_time = 0;       // 上次累加流量時間
	/**
	 * 累計總流量
	 * @param now   現在的時間
	 */
	void read_total_flow(unsigned long now);
	/**
	 * 用2個16進制轉換成浮點數
	 * @param index  資料來源起點
	 * @return
	 */
	float get_flow_speed_from_device(int index);
	/**
	 * 用2個16進制轉換成浮點數
	 * @param part_a
	 * @param part_b
	 * @return
	 */
	float decode_ieee(uint16_t part_a, uint16_t part_b);
	int d = 65; // 管道直徑 單位:mm
	/**
	 * 計算驗正碼
	 * @param u8length  資料長度
	 * @return 驗正碼
	 */
	uint16_t calcCRC(uint8_t u8length, const uint16_t data_list[]);
	/**
	* 計算阻尼流速
	* @param now 現在時間
	*/
	void caculate_damping_flow_rate(unsigned long now);
	static int sort_desc(const void *cmp1, const void *cmp2);	// 排序用
	/* 
	* 判斷是否為空管  流速10以上 導電三千以上  流量百分比400以上
	* @return bool true if empty
	*/
	bool pipe_is_empty();
	bool pto_enable = false; // 表示PTO是否開啟中
};

ReadLMag::ReadLMag(int slave_id, double amplification, int d) {
	this->slave_id = slave_id;
	this->amplification = amplification;
	this->d = d;
	// 歸零
	this->data.instant_flow_rate = 0;
	this->data.instant_flow_value = 0;
	this->data.total_positive = 0;
	// 寫入slave id到請求命令
	request_cmd[0] = slave_id;
	// 寫入驗證碼
	uint16_t crc = this->calcCRC(6, this->request_cmd);
	request_cmd[6] = crc >> 8;
	request_cmd[7] = crc & 0x00ff;
}

void ReadLMag::test() {
	Serial.print(total / 1000);
	Serial.print("L  ");
	Serial.println();
}

void ReadLMag::set_pto_status(bool status) {
	this->pto_enable = status;
}

int ReadLMag::get_slave_id() {
	return this->slave_id;
}

float ReadLMag::get_flow_speed_from_device(int index) {
	return decode_ieee(
		return_buffer[index],
		return_buffer[index + 1]
	);
}

void ReadLMag::caculate_flow_rate() {
	float calibration = 1.0;
	float instant_flow_rate = get_flow_speed_from_device(2) * this->amplification;
	float current_calibration_flow_rate = 0;

	if (this->pipe_is_empty()) {
		instant_flow_rate = 0;
	}
	
	// 流量校正
	for (int i = 0; i < CALIBRATION_LEN; i++) {
		if (this->calibrations[i].flow_rate != 0 &&
			this->calibrations[i].flow_rate < instant_flow_rate &&
			this->calibrations[i].flow_rate > current_calibration_flow_rate) {
			current_calibration_flow_rate = this->calibrations[i].flow_rate;
			calibration = this->calibrations[i].calibration_amplification;
		}
	}

	this->data.instant_flow_rate = instant_flow_rate * calibration;
	double value_pre_sec = this->data.instant_flow_rate*d*d*M_PI_4;
	this->data.instant_flow_value = value_pre_sec / 1000;
	read_total_flow(millis());
	caculate_damping_flow_rate(millis());
}

float ReadLMag::decode_ieee(uint16_t part_a, uint16_t part_b) {
	union {
		unsigned long l;
		float f;
	}tmp;
	tmp.l = part_a;
	tmp.l = tmp.l << 16;
	tmp.l += part_b;
	return tmp.f;
}

void ReadLMag::send_reset_total_flow_command(Modbus master) {
	Serial.println("send_reset_total_flow_command to zero!!!");
	uint16_t data[1];
	data[0] = 42330;
	modbus_t telegram_reset;
	telegram_reset.u8id = this->slave_id;
	telegram_reset.u8fct = 6;
	telegram_reset.u16RegAdd = 71;
	telegram_reset.au16reg = data;
	telegram_reset.u16CoilsNo = 1;
	master.query(telegram_reset);
	delay(100);
	master.poll();
}

void ReadLMag::read_total_flow(unsigned long now) {
	/*
	* 這是自己計算總流量的code
	double value_pre_sec = this->data.instant_flow_rate*d*d*M_PI_4;
	double value = value_pre_sec * (now - this->last_add_time) / 1000;
	if (value > 0 && value < 1000000) {
		this->data.total_positive += value;
	};
	last_add_time = now;
	*/

	// 直接讀取流量計寄存器裡的數值
	uint32_t total_water_value_integer_part = return_buffer[8];
	total_water_value_integer_part = total_water_value_integer_part << 16;
	total_water_value_integer_part += return_buffer[9];
	float total_water_value_point_part = this->get_flow_speed_from_device(10);
	this->data.total_positive = total_water_value_integer_part + total_water_value_point_part;
	this->data.total_positive *= this->amplification;
}

void ReadLMag::print_data() {
	Serial.print(" ID:");
	Serial.print(this->slave_id);
	Serial.print(" 流速:");
	printDouble(this->data.instant_flow_rate, 5);
	//Serial.print(" D速:");
	//printDouble(this->data.damping_flow_rate, 5);
	Serial.print(" 流量:");
	printDouble(this->data.instant_flow_value * 60, 5);
	Serial.print(" D量:");
	printDouble(this->data.damping_flow_value * 60, 5);
	Serial.print(" 總量:");
	Serial.print(this->data.total_positive);
	Serial.print(" time: ");
	Serial.print(millis() / 1000);
	Serial.print(" sec");

	Serial.println();
}

void ReadLMag::init(unsigned long now) {
	last_add_time = now;
	this->damping_flow_rate_array_time_stamp = now;
}

uint16_t ReadLMag::calcCRC(uint8_t u8length, const uint16_t data_list[]) {
	unsigned int temp, temp2, flag;
	temp = 0xFFFF;
	for (unsigned char i = 0; i < u8length; i++)
	{
		temp = temp ^ data_list[i];
		for (unsigned char j = 1; j <= 8; j++)
		{
			flag = temp & 0x0001;
			temp >>= 1;
			if (flag)
				temp ^= 0xA001;
		}
	}
	// Reverse byte order.
	temp2 = temp >> 8;
	temp = (temp << 8) | temp2;
	temp &= 0xFFFF;
	// the returned value is already swapped
	// crcLo byte is first & crcHi byte is last
	return temp;
}

void ReadLMag::request_timeout(unsigned long now) {
	this->last_add_time = now;
	this->data.instant_flow_rate = 0;  // 沒聯絡到就當作0
	this->data.instant_flow_value = 0;
}

void ReadLMag::caculate_damping_flow_rate(unsigned long now) {
	const uint16_t sampling_time_delta = 500;
	const int noise_reduction_cut_len = 0;
	// 防呆
	if (now - this->damping_flow_rate_array_time_stamp < sampling_time_delta) { return; }
	if (this->damping_flow_rate_array_write_index >= this->damping_flow_rate_array_len) { this->damping_flow_rate_array_write_index = 0; }

	this->damping_flow_rate_array_time_stamp = now;
	this->damping_flow_rate_array[this->damping_flow_rate_array_write_index] = this->data.instant_flow_rate;


	// 複製然後排序 只取中段值來平均(降噪)
	float damping_flow_rate_array_copy[DAMPING_TIMES] = { 0 };
	for (int i = 0; i < this->damping_flow_rate_array_len; i++) {
		damping_flow_rate_array_copy[i] = this->damping_flow_rate_array[i];
	}

	qsort(damping_flow_rate_array_copy,
		this->damping_flow_rate_array_len,
		sizeof(damping_flow_rate_array_copy[0]),
		this->sort_desc);


	float sum = 0;
	int start_index = noise_reduction_cut_len;
	int end_index = this->damping_flow_rate_array_len - noise_reduction_cut_len;
	start_index = start_index > this->damping_flow_rate_array_len ? 0 : start_index;
	end_index = end_index < 0 ? 0 : end_index;

	for (int i = start_index; i < end_index; i++) {
		sum += damping_flow_rate_array_copy[i];
	}
	this->data.damping_flow_rate = sum / (end_index - start_index);
	double value_pre_sec = this->data.damping_flow_rate*d*d*M_PI_4;
	this->data.damping_flow_value = value_pre_sec / 1000;
	
	// 決定是否要快速改值		 //暫時移除快速改值
	//const int quick_change_detect_len = 6;
	//const float quick_change_detect_diff = 1.0;
	//const int cut_len = 1;
	//float  *quick_change_detect_array = new float[this->quick_change_detect_len];
	//float quick_change_sum = 0;
	//for (int i = 0; i < quick_change_detect_len; i++) {
	//	int quick_change_index = this->damping_flow_rate_array_write_index - i;
	//	quick_change_index = quick_change_index < 0 ? this->damping_flow_rate_array_len + quick_change_index : quick_change_index;
	//	quick_change_detect_array[i] = this->damping_flow_rate_array[quick_change_index];
	//	//Serial.print(quick_change_index);
	//	//Serial.print("\t");
	//}

	//// 排序降噪
	//qsort(quick_change_detect_array,
	//	quick_change_detect_len,
	//	sizeof(damping_flow_rate_array_copy[0]),
	//	this->sort_desc);


	//for (int i = cut_len; i < quick_change_detect_len - cut_len; i++) {
	//	quick_change_sum += quick_change_detect_array[i];
	//}
	//float quick_change_avg = quick_change_sum / (quick_change_detect_len - 2 * cut_len);
	//if (abs(this->data.damping_flow_rate - quick_change_avg) > quick_change_detect_diff) {
	//	this->data.damping_flow_rate = quick_change_avg;
	//	for (int i = 0; i < this->damping_flow_rate_array_len; i++) {
	//		this->damping_flow_rate_array[i] = this->data.instant_flow_rate;	 
	//	}
	//	Serial.println("快速改值");
	//}

	this->damping_flow_rate_array_write_index++;
	//delete quick_change_detect_array;
#ifdef DEBUG_MESSAGE
	this->print_data();
#endif // DEBUG_MESSAGE

}

int ReadLMag::sort_desc(const void *cmp1, const void *cmp2)
{
	float a = *((float *)cmp1);
	float b = *((float *)cmp2);
	return a > b ? -1 : (a < b ? 1 : 0);
}

void ReadLMag::set_calibration(Calibration calibration, int index) {
	if (index >= CALIBRATION_LEN) { return; }
	this->calibrations[index] = calibration;
}

void ReadLMag::show_calibration() {
	for (int i = 0; i < CALIBRATION_LEN; i++) {
		Serial.print("流速");
		Serial.print(this->calibrations[i].flow_rate);
		Serial.print("\t\t校正");
		Serial.println(this->calibrations[i].calibration_amplification);
	}
}

bool ReadLMag::pipe_is_empty() {
	// 判斷是否為空管  流速15以上 導電三千以上  流量百分比400以上
	return !this->pto_enable;
}

// ---------------------------------------
// -------------放在ino裡----------------

void magDecoder(ReadLMag * ReadLMag_list[]) {
	if (ReadLMag_list[0] == NULL) {
		return;
	}
	int index = 0;
	while (ReadLMag_list[index] != NULL) {
		ReadLMag_list[index++]->caculate_flow_rate();
	}
}

#endif //LIBRARIES_READMODBUS_H