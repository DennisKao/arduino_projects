﻿// NextionObjectDefine.h

#ifndef _NEXTIONOBJECTDEFINE_h
#define _NEXTIONOBJECTDEFINE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "Nextion.h"
#include "NextionExtension.h"


// define in .ion
extern uint16_t modbus_data_array[];

// Nexobjects
NexPage page_opening = NexPage(0, 0, "opening");

NexVariableCostom va_pto = NexVariableCostom(0, 1, "va_pto",
	new NexDataLink(&modbus_data_array[0], &modbus_data_array[0]));
NexVariableCostom va_pump_p = NexVariableCostom(0, 19, "va_pump_p",
	new NexDataLink(&modbus_data_array[1], &modbus_data_array[1]));
NexVariableCostom va_temp_alert = NexVariableCostom(0, 27, "va_temp_alert",
	new NexDataLink(&modbus_data_array[2], &modbus_data_array[2]));
NexVariableCostom va_wout_nR1 = NexVariableCostom(0, 3, "va_wout_nR1",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_wout_nR2 = NexVariableCostom(0, 4, "va_wout_nR2",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_wout_nL1 = NexVariableCostom(0, 5, "va_wout_nL1",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_wout_nL2 = NexVariableCostom(0, 6, "va_wout_nL2",
	new NexDataLink(NULL, NULL));

NexVariableCostom va_wtotal_L1 = NexVariableCostom(0, 23, "va_wtotal_L1",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_wtotal_L2 = NexVariableCostom(0, 24, "va_wtotal_L2",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_wtotal_R1 = NexVariableCostom(0, 25, "va_wtotal_R1",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_wtotal_R2 = NexVariableCostom(0, 26, "va_wtotal_R2",
	new NexDataLink(NULL, NULL));

NexVariableCostom va_tank_gate = NexVariableCostom(0, 13, "va_tank_gate",
	new NexDataLink(&modbus_data_array[7], &modbus_data_array[7]));
NexVariableCostom va_tank_total = NexVariableCostom(0, 11, "va_tank_total", // 總水量
	new NexDataLink(NULL, NULL));
NexVariableCostom va_tank_now = NexVariableCostom(0, 12, "va_tank_now", // 目前剩餘水量百分比
	new NexDataLink(&modbus_data_array[8], &modbus_data_array[8]));
NexVariableCostom va_tank_timer = NexVariableCostom(0, 14, "va_tank_timer", // 剩餘時間(分
	new NexDataLink(NULL, NULL));
NexVariableCostom va_tank_timer2 = NexVariableCostom(0, 20, "va_tank_timer2", // 剩餘時間(秒
	new NexDataLink(NULL, NULL));
NexVariableCostom va_pump_rpm = NexVariableCostom(0, 15, "va_pump_rpm",
	new NexDataLink(NULL, NULL));
NexVariableCostom va_pump_deg = NexVariableCostom(0, 16, "va_pump_temp",	//溫度
	new NexDataLink(&modbus_data_array[10], &modbus_data_array[10]));
NexVariableCostom va_pump_empty = NexVariableCostom(0, 17, "va_pump_empty",
	new NexDataLink(&modbus_data_array[11], &modbus_data_array[11]));

NexPage page_main = NexPage(1, 0, "main_re");

NexVariableCostom *nex_display_list[] = {
	&va_pto,
	&va_tank_timer,
	&va_tank_timer2,
	&va_pump_p,
	&va_tank_now,
	&va_temp_alert,
	&va_wout_nR1,
	&va_wout_nR2,
	&va_wout_nL1,
	&va_wout_nL2,
	&va_wtotal_L1,
	&va_wtotal_L2,
	&va_wtotal_R1,
	&va_wtotal_R2,
	&va_tank_gate,
	&va_pump_rpm,
	&va_pump_deg,
	&va_pump_empty,
	&va_tank_total,
	NULL
};

unsigned long last_cmd_send_time = 0;
const unsigned long send_cmd_cool_down = 0;
void control_by_modbus(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == *var_ptr->data_link->recive_val_from_modbus_ptr) { return; }
	var_ptr->data_link->current_data_of_hmi = *var_ptr->data_link->recive_val_from_modbus_ptr;
	char page_name[10] = "opening";
	var_ptr->setGlobalValue(page_name, *var_ptr->data_link->recive_val_from_modbus_ptr);
	last_cmd_send_time = millis();
}
void control_by_self(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return; }
	var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
	char page_name[10] = "opening";
	var_ptr->setGlobalValue(page_name, var_ptr->data_link->send_val_to_hmi);
	last_cmd_send_time = millis();
}
void display_water_flow_value(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return; }
	var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
	char page_name[10] = "opening";
	uint16_t send_val_to_hmi_cut_digit_in_ones = var_ptr->data_link->send_val_to_hmi / 10 * 10;		// 去掉個位數
	var_ptr->setGlobalValue(page_name, send_val_to_hmi_cut_digit_in_ones);
	last_cmd_send_time = millis();
}
void display_water_total_value(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return; }
	var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
	char page_name[10] = "opening";
	uint16_t send_val_to_hmi_cut_digit_in_ones = var_ptr->data_link->send_val_to_hmi;
	var_ptr->setGlobalValue(page_name, send_val_to_hmi_cut_digit_in_ones);
	last_cmd_send_time = millis();
}
void display_left_time_minutes(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return; }
	var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
	char page_name[10] = "opening";
	var_ptr->setGlobalValue(page_name, var_ptr->data_link->send_val_to_hmi /60);
	last_cmd_send_time = millis();
}
void display_left_time_second(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return; }
	var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
	char page_name[10] = "opening";
	var_ptr->setGlobalValue(page_name, var_ptr->data_link->send_val_to_hmi % 60);
	last_cmd_send_time = millis();
}
void va_tank_now_callback(void *var, void *null_ptr) {
	if (millis() - last_cmd_send_time < send_cmd_cool_down) { return; }
	NexVariableCostom *var_ptr = (NexVariableCostom *)var;
	if (var_ptr->data_link->current_data_of_hmi == *var_ptr->data_link->recive_val_from_modbus_ptr) { return; }
	var_ptr->data_link->current_data_of_hmi = *var_ptr->data_link->recive_val_from_modbus_ptr;
	char page_name[10] = "opening";
	var_ptr->setGlobalValue(page_name, var_ptr->data_link->current_data_of_hmi);
	last_cmd_send_time = millis();
}

void attach_all_hmi_user_func() {
	va_pto.data_link->attach_hmi_user_func(control_by_modbus, &va_pto, NULL);
	va_pump_p.data_link->attach_hmi_user_func(control_by_modbus, &va_pump_p, NULL);
	va_temp_alert.data_link->attach_hmi_user_func(control_by_modbus, &va_temp_alert, NULL);
	va_wout_nR1.data_link->attach_hmi_user_func(display_water_flow_value, &va_wout_nR1, NULL);
	va_wout_nR2.data_link->attach_hmi_user_func(display_water_flow_value, &va_wout_nR2, NULL);
	va_wout_nL1.data_link->attach_hmi_user_func(display_water_flow_value, &va_wout_nL1, NULL);
	va_wout_nL2.data_link->attach_hmi_user_func(display_water_flow_value, &va_wout_nL2, NULL);
	va_wtotal_L1.data_link->attach_hmi_user_func(display_water_total_value, &va_wtotal_L1, NULL);
	va_wtotal_L2.data_link->attach_hmi_user_func(display_water_total_value, &va_wtotal_L2, NULL);
	va_wtotal_R1.data_link->attach_hmi_user_func(display_water_total_value, &va_wtotal_R1, NULL);
	va_wtotal_R2.data_link->attach_hmi_user_func(display_water_total_value, &va_wtotal_R2, NULL);
	va_tank_now.data_link->attach_hmi_user_func(va_tank_now_callback, &va_tank_now, NULL);
	va_tank_gate.data_link->attach_hmi_user_func(control_by_modbus, &va_tank_gate, NULL);
	va_tank_timer.data_link->attach_hmi_user_func(display_left_time_minutes, &va_tank_timer, NULL);
	va_tank_timer2.data_link->attach_hmi_user_func(display_left_time_second, &va_tank_timer2, NULL);
	va_pump_rpm.data_link->attach_hmi_user_func(control_by_modbus, &va_pump_rpm, NULL);
	va_pump_deg.data_link->attach_hmi_user_func(control_by_modbus, &va_pump_deg, NULL);
	va_pump_empty.data_link->attach_hmi_user_func(control_by_modbus, &va_pump_empty, NULL);
	va_tank_total.data_link->attach_hmi_user_func(control_by_self, &va_tank_total, NULL);
}
void send_data_to_nextion_hmi_loop() {
	static unsigned long timestamp = 0;
	unsigned long now = millis();
	if (now - timestamp < 50) { return; }
	timestamp = now;
	int  index = 0;
	while (nex_display_list[index] != NULL)
	{
		NexVariableCostom *target = nex_display_list[index++];
		target->data_link->render_if_need();
	}
}

#endif

