﻿// EstimatedEndTime.h

#ifndef _ESTIMATEDENDTIME_h
#define _ESTIMATEDENDTIME_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
//#define Estimated_End_Time_Test_mode  // 估計剩餘時間測試模式 請註解以關閉該模式

/*
	用流量計算預估剩餘時間
*/
class EstimatedEndTime {
public:
	/*
		@param total_water_value_left 總容量(單位: 公升)
		@param blur_spacing  模糊的級距(模糊化後的結果跳動的級距) ex 100 -> 105 級距值就是5
	*/
	EstimatedEndTime(int total_water_value_left, int blur_spacing=10);
	/*
		設置總容量
		@param total_water_value_left 總容量(單位: 公升)
	*/
	void set_total_water_value_left(int total_water_value_left);
	/*
		設置流量
		@param flow_value 流量 (單位: 公升/分鐘)
	*/
	void set_flow_value(double flow_value);
	/*
		取得預估結束時間
		@return 結束時間(單位: 秒)
	*/
	int get_estimate_end_time();
	/*
		取得模糊化後的預估結束時間
	*/
	int get_blur_estimate_end_time();
private:
	/*
		總容量(單位: 公升)
	*/
	int total_water_value_left;
	/*
		流量(公升/分)
	*/
	double flow_value = 0;
	/*
		模糊的級距(模糊化後的結果跳動的級距) ex 100 -> 105 級距值就是5
	*/
	int blur_spacing = 0;
	/*
		計算預估時間
	*/
	int caculate_estimate_end_time();
	/*
		模糊化輸出的預估時間
		@param estimate_end_time  預估時間的原始數據
	*/
	int blur_estimate_end_time(int estimate_end_time);
};

// =====================
// =======Public==========
EstimatedEndTime::EstimatedEndTime(int total_water_value_left, int blur_spacing = 10) {
	this->total_water_value_left = total_water_value_left;
	this->blur_spacing = blur_spacing == 0 ? 1: blur_spacing;
}

void EstimatedEndTime::set_total_water_value_left(int total_water_value_left) {
	this->total_water_value_left = total_water_value_left;
}

void EstimatedEndTime::set_flow_value(double flow_value) {
	this->flow_value = flow_value;
}

int EstimatedEndTime::get_estimate_end_time() {
	return this->caculate_estimate_end_time();
}

int EstimatedEndTime::get_blur_estimate_end_time() {
	int estimate_end_time = this->caculate_estimate_end_time();
	return blur_estimate_end_time(estimate_end_time);
}
// =====================


// =====================
// =======Private==========
int EstimatedEndTime::caculate_estimate_end_time() {
	double flow_value_pre_second = this->flow_value;
	// 防止除0
	flow_value_pre_second = flow_value_pre_second == 0 ? 1 : flow_value_pre_second;
#ifdef Estimated_End_Time_Test_mode
	static int test_total_water = 1000;
	static unsigned long st = 0;
	if (millis() - st >= 1000) {
		//test_total_water -= 12;
		st = millis();
	}
	//Serial.println(flow_value);
	return test_total_water / flow_value_pre_second;
#endif // Estimated_End_Time_Test_mode
	return this->total_water_value_left / flow_value_pre_second;
}

int EstimatedEndTime::blur_estimate_end_time(int estimate_end_time) {
	return  estimate_end_time / this->blur_spacing * this->blur_spacing;
}


#endif

