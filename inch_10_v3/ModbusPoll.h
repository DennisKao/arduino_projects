﻿// ModbusPoll.h

#ifndef _MODBUSPOLL_h
#define _MODBUSPOLL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "ReadLMag.h"
#ifndef MODBUS_RTU_H
#define MODBUS_RTU_H
#include <ModbusRtu.h>
#endif // !MODBUS_RTU_H


#ifndef MODBUS_PIN
#define MODBUS_PIN 23
#endif
Modbus master(0, 1, MODBUS_PIN); // this is master and RS-232 or USB-FTDI
unsigned long u32wait;
// test
modbus_t telegram_test;
uint16_t au16data[25] = { 0 };
// setup
void readLMagInit(unsigned long now, ReadLMag * ReadLMag_list[]) {
	master.begin(38400); // baud-rate at 19200
	if (ReadLMag_list[0] == NULL) {
		return;
	}
	int index = 0;
	while (ReadLMag_list[index] != NULL) {
		ReadLMag_list[index++]->init(now);
	}
	master.setTimeOut(100); // if there is no answer in 2000 ms, roll over
	u32wait = millis() + 30;
}

extern ReadLMag *mags[];
extern ReadLMag mag_1;
#define telegram_array_len 4

// loop
int modbusDevicePoll(unsigned long now) {
	static int current_index = 0;   // 正在處理的物件index
	static uint8_t u8state = 0;
	modbus_t telegram_array[telegram_array_len] = {
		{mags[0]->get_slave_id(), 4, 4112, 22, mags[0]->return_buffer},
		{mags[1]->get_slave_id(), 4, 4112, 22, mags[1]->return_buffer},
		{mags[2]->get_slave_id(), 4, 4112, 22, mags[2]->return_buffer},
		{mags[3]->get_slave_id(), 4, 4112, 22, mags[3]->return_buffer}
	};

	// 一些防呆和overflow
	if (current_index >= telegram_array_len) {
		current_index = 0;
	}

	switch (u8state) {
	case 0:
		if (millis() > u32wait) u8state++; // wait state
		break;
	case 1:
		master.query(telegram_array[current_index]); // send query (only once)
		u8state++;
		break;
	case 2:
		master.poll(); // check incoming messages
		if (master.getState() == COM_IDLE) {
			u8state = 0;
			u32wait = millis() + 30;
			current_index++;
		}
		break;
	}

}

#endif

