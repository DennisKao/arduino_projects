// ModbusModel.h

#ifndef _MODBUSMODEL_h
#define _MODBUSMODEL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "JoyStickReader.h"

class Modbus8In8Out{
public:
	struct Sensor
	{
		ControllerIdentity id;
		bool vertical_top;
		bool vertical_bottom;
		bool horizontal_center;
	};
	struct RelayGroup {
		ControllerIdentity id;
		bool relay_0;
		bool relay_1;
		bool relay_2;
		bool relay_3;
	};
	Sensor get_sensor_status(ControllerIdentity id) {};
	void set_relay(bool RelayGroup) {};
	void append_sensor() {}
	void append_relay_group() {}
	void pool() {
		// modbusRtu
	}
private:
	Sensor sensors[2] = {{None}, {None}};
	RelayGroup relays[2] = { {None}, {None} };;
};

#endif

