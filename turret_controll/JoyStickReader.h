// JoyStickReader.h

#ifndef _CONTROLLERREADER_h
#define _CONTROLLERREADER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

enum class DirectionVertical { None, Up, Down };
enum class Directionhorizontal { None, Left, Right };
struct Directions
{
	DirectionVertical vertical;
	Directionhorizontal horizontal;
};
enum ControllerIdentity
{
	None, A, B
};

class JoyStickReader {
public:
	JoyStickReader() {}
	
	Directions get_joystick_status(ControllerIdentity id) {}
	void poll() {}
private:
	void parse_analog_signal() {}
	Directions controll_A_status;
	Directions controll_B_status;
};

#endif

