// TurrentController.h

#ifndef _TURRENTCONTROLLER_h
#define _TURRENTCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "JoyStickReader.h"

class TurrentController {
public:
	void pool() {};
private:
	ControllerIdentity id;
	void turn(Directions directions) {}
};

#endif

