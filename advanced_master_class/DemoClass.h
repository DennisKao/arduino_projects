// DemoClass.h

#ifndef _DEMOCLASS_h
#define _DEMOCLASS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <ModbusRtu.h>


class DemoClass {
public:	// functions
	DemoClass() {
		this->master = new Modbus(0, 0, 0);
		telegram[0].u8id = 1; // slave address
		telegram[0].u8fct = 3; // function code (this one is registers read)
		telegram[0].u16RegAdd = 0; // start address in slave
		telegram[0].u16CoilsNo = 4; // number of elements (coils or registers) to read
		telegram[0].au16reg = au16data; // pointer to a memory array in the Arduino

		// telegram 1: write a single register
		telegram[1].u8id = 1; // slave address
		telegram[1].u8fct = 6; // function code (this one is write a single register)
		telegram[1].u16RegAdd = 4; // start address in slave
		telegram[1].u16CoilsNo = 1; // number of elements (coils or registers) to read
		telegram[1].au16reg = au16data + 4; // pointer to a memory array in the Arduino

		master->begin(19200); // baud-rate at 19200
		master->setTimeOut(5000); // if there is no answer in 5000 ms, roll over
		u32wait = millis() + 1000;
		u8state = u8query = 0;
	};
	void loop() {
		switch (u8state) {
		case 0:
			if (millis() > u32wait) u8state++; // wait state
			break;
		case 1:
			master->query(telegram[u8query]); // send query (only once)
			u8state++;
			u8query++;
			if (u8query > 2) u8query = 0;
			break;
		case 2:
			master->poll(); // check incoming messages
			if (master->getState() == COM_IDLE) {
				u8state = 0;
				u32wait = millis() + 1000;
			}
			break;
		}

		au16data[4] = analogRead(0);
	};
public:	// property
	Modbus *master;
	uint16_t au16data[16]; //!< data array for modbus network sharing
	uint8_t u8state; //!< machine state
	uint8_t u8query; //!< pointer to message query
	modbus_t telegram[2];
	unsigned long u32wait;
};

#endif

