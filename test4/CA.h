// CA.h

#ifndef _CA_h
#define _CA_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


class B;

class A {
public:
	A();
	void test_a();
	void test_b();
	void test_header() {
		test_a();
	}
};


#endif

