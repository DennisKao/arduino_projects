// CB.h

#ifndef _CB_h
#define _CB_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


class A;

class B {
public:
	B();
	void test_a();
	void test_b();
};

#endif

