// ModbusModel.h

#ifndef _MODBUSMODEL_h
#define _MODBUSMODEL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
class ModbusModel {
public:
	// 一般變數
	uint16_t *binary_type_buffer;
	uint16_t *va_battery;
	uint16_t *va_tank_0;
	uint16_t *va_tank_1;
	uint16_t *va_tank_2;
	uint16_t *b2_audsource_0;
	uint16_t *b_vol_up;
	uint16_t *b_mode_1;
	uint16_t *b_speed_up;
	uint16_t *b_aud_left;
	// 二進位
};

#endif

