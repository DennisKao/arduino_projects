﻿// NextionObjectsDefine.h

#ifndef _NEXTIONOBJECTSDEFINE_h
#define _NEXTIONOBJECTSDEFINE_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Nextion.h"

#define DATA_LEN 100 // mod bus 資料長度
// 在.ino裡
extern uint16_t au16data[];

// 變數 依照modbus資料陣列順序排序
// au16data store value is 0 or 1
NexButtonCostom b_light_side = NexButtonCostom(1, 7, "b07_light_side",
	new NexDataLink(&au16data[0], &au16data[0 + DATA_LEN]));
NexButtonCostom b_light_inside = NexButtonCostom(1, 9, "b09_light_in",
	new NexDataLink(&au16data[1], &au16data[1 + DATA_LEN]));
NexButtonCostom b84_shield = NexButtonCostom(1, 84, "b84_shield",
	new NexDataLink(&au16data[2], &au16data[2 + DATA_LEN]));
NexVariableCostom va_light_main = NexVariableCostom(0, 35, "va_light_main",
	new NexDataLink(&au16data[3], &au16data[3 + DATA_LEN]));
NexButtonCostom b_pto = NexButtonCostom(1, 2, "b02_pto",
	new NexDataLink(&au16data[4], &au16data[4 + DATA_LEN]));
NexButtonCostom b_pto2 = NexButtonCostom(1, 3, "b03_pto2",
	new NexDataLink(&au16data[5], &au16data[5 + DATA_LEN]));
NexButtonCostom b_rolldoor = NexButtonCostom(1, 4, "b04_rolldoor",
	new NexDataLink(&au16data[6], &au16data[6 + DATA_LEN]));
NexButtonCostom b_head_lock = NexButtonCostom(1, 5, "b05_headlock",
	new NexDataLink(&au16data[7], &au16data[7 + DATA_LEN]));
NexButtonCostom b86_tailgate = NexButtonCostom(1, 86, "b86_tailgate",
	new NexDataLink(&au16data[8], &au16data[8 + DATA_LEN]));
NexButtonCostom b_85_passenger = NexButtonCostom(1, 85, "b85_passenger",		// 人員座艙指示燈
	new NexDataLink(&au16data[9], &au16data[9 + DATA_LEN]));
NexButtonCostom b24_square_on = NexButtonCostom(5, 24, "b24_square_on",
	new NexDataLink(&au16data[10], &au16data[10 + DATA_LEN]));
NexVariableCostom va_fan = NexVariableCostom(0, 36, "va_fan",
	new NexDataLink(&au16data[11], &au16data[11 + DATA_LEN]));
NexVariableCostom va_fanstand = NexVariableCostom(0, 37, "va_fanstand",
	new NexDataLink(&au16data[12], &au16data[12 + DATA_LEN]));
NexVariableCostom va_cafs = NexVariableCostom(0, 30, "va_cafs",
	new NexDataLink(&au16data[13], &au16data[13 + DATA_LEN]));
NexButtonCostom b2_CAFSonoff = NexButtonCostom(8, 2, "b2_CAFSonoff",
	va_cafs.data_link);
NexVariableCostom va_cannon = NexVariableCostom(0, 31, "va_cannon",
	new NexDataLink(&au16data[14], &au16data[14 + DATA_LEN]));
NexButtonCostom b3_CANNONonoff = NexButtonCostom(8, 3, "b3_CANNONonoff",
	va_cannon.data_link);
NexButtonCostom b9_engine_up = NexButtonCostom(8, 9, "b9_engine_up",
	new NexDataLink(&au16data[15], &au16data[15 + DATA_LEN]));
NexButtonCostom b8_engine_dn = NexButtonCostom(8, 8, "b8_engine_dn",
	new NexDataLink(&au16data[16], &au16data[16 + DATA_LEN]));
NexButtonCostom b5_foam_up = NexButtonCostom(8, 5, "b5_foam_up",
	new NexDataLink(&au16data[17], &au16data[17 + DATA_LEN]));
NexButtonCostom b4_foam_dn = NexButtonCostom(8, 4, "b4_foam_dn",
	new NexDataLink(&au16data[18], &au16data[18 + DATA_LEN]));
NexButtonCostom b7_air_up = NexButtonCostom(8, 7, "b7_air_up",
	new NexDataLink(&au16data[19], &au16data[19 + DATA_LEN]));
NexButtonCostom b6_air_dn = NexButtonCostom(8, 6, "b6_air_dn",
	new NexDataLink(&au16data[20], &au16data[20 + DATA_LEN]));
NexButtonCostom va_cart = NexButtonCostom(0, 38, "va_cart",
	new NexDataLink(&au16data[21], &au16data[21 + DATA_LEN]));
NexButtonCostom b33_proomlight = NexButtonCostom(1, 33, "b33_proomlight",
	new NexDataLink(&au16data[22], &au16data[22 + DATA_LEN]));
NexButtonCostom b34_lightsideR = NexButtonCostom(1, 34, "b34_lightsideR",
	new NexDataLink(&au16data[23], &au16data[23 + DATA_LEN]));
NexButtonCostom b35_lightsideB = NexButtonCostom(1, 35, "b35_lightsideB",
	new NexDataLink(&au16data[24], &au16data[24 + DATA_LEN]));

// au16data store value more than 1
NexButtonCostom b_mode_1 = NexButtonCostom(5, 14, "b_mode_1",
	new NexDataLink(&au16data[30], &au16data[30 + DATA_LEN]));
NexButtonCostom b_mode_2 = NexButtonCostom(5, 22, "b_mode_2",
	b_mode_1.data_link);
NexButtonCostom b_mode_3 = NexButtonCostom(5, 21, "b_mode_3",
	b_mode_1.data_link);
NexButtonCostom b_mode_4 = NexButtonCostom(5, 20, "b_mode_4",
	b_mode_1.data_link);
NexVariableCostom va_led_mode = NexVariableCostom(0, 20, "va_led_mode",   // led 模式
	b_mode_1.data_link);
NexButtonCostom b_speed_up = NexButtonCostom(5, 16, "b_speed_up",
	new NexDataLink(&au16data[31], &au16data[31 + DATA_LEN]));
NexButtonCostom b_speed_dwn = NexButtonCostom(5, 15, "b_speed_dwn",
	b_speed_up.data_link);
NexVariableCostom va_led_speed = NexVariableCostom(0, 21, "va_led_speed",   // led 閃爍速度
	b_speed_up.data_link);
NexVariableCostom va_battery = NexVariableCostom(0, 2, "va_battery",
	new NexDataLink(&au16data[32], &au16data[32 + DATA_LEN]));
NexVariableCostom va_tank_0 = NexVariableCostom(0, 3, "va_tank_0",
	new NexDataLink(&au16data[33], &au16data[33 + DATA_LEN]));
NexVariableCostom va_tank_1 = NexVariableCostom(0, 4, "va_tank_1", 
	new NexDataLink(&au16data[34], &au16data[34 + DATA_LEN]));
NexVariableCostom va_tank_2 = NexVariableCostom(0, 5, "va_tank_2",
	new NexDataLink(&au16data[35], &au16data[35 + DATA_LEN]));
NexVariableCostom va_cafs_press = NexVariableCostom(0, 32, "va_cafs_press",
	new NexDataLink(&au16data[36], &au16data[36 + DATA_LEN]));
NexVariableCostom va_foam_per = NexVariableCostom(0, 33, "va_foam_per",
	new NexDataLink(&au16data[37], &au16data[37 + DATA_LEN]));
NexVariableCostom va_air_gauge = NexVariableCostom(0, 34, "va_air_gauge",
	new NexDataLink(&au16data[38], &au16data[38 + DATA_LEN]));

// 多數值按鈕物件
// --音源
NexButtonCostom b2_audsource_0 = NexButtonCostom(7, 2, "b2_audsource_1",
	new NexDataLink(&au16data[7], &au16data[7 + DATA_LEN]));
NexButtonCostom b3_audsource_1 = NexButtonCostom(7, 3, "b3_audsource_2",
	b2_audsource_0.data_link);
NexButtonCostom b4_audsource_2 = NexButtonCostom(7, 4, "b4_audsource_3",
	b2_audsource_0.data_link);

NexVariableCostom va_aud_source = NexVariableCostom(0, 16, "va_aud_source",
	b2_audsource_0.data_link);
// --音量
NexButtonCostom b_vol_up = NexButtonCostom(1, 81, "b81_vol_up", NULL);
NexButtonCostom b_vol_down = NexButtonCostom(1, 80, "b80_vol_dowm", NULL);
NexVariableCostom va_volume = NexVariableCostom(0, 24, "va_volume", new NexDataLink(NULL, NULL));

NexButtonCostom b_aud_left = NexButtonCostom(1, 11, "b11_speaker_l", new NexDataLink(NULL, NULL));
NexButtonCostom b_aud_front = NexButtonCostom(1, 12, "b12_speaker_f", b_aud_left.data_link);
NexButtonCostom b_aud_right = NexButtonCostom(1, 13, "b13_speaker_r", b_aud_left.data_link);


NexButtonCostom b_light_h = NexButtonCostom(1, 6, "b06_light_h",
	new NexDataLink(NULL, NULL));
NexButtonCostom b_light_flash = NexButtonCostom(1, 8, "b08_flashlight",
	new NexDataLink(NULL, NULL));
NexButtonCostom b25_square_off = NexButtonCostom(5, 25, "b25_square_off",
	b24_square_on.data_link);
NexButtonCostom b_aud_switch = NexButtonCostom(1, 10, "b10_aud_switch",
	new NexDataLink(NULL, NULL));
NexButtonCostom b_sync_light = NexButtonCostom(1, 19, "b19_sync_light", new NexDataLink(NULL, NULL));


// 不在modbus資料陣列的變數
NexVariableCostom va_pto = NexVariableCostom(0, 7, "va_pto");
NexVariableCostom va_pto2 = NexVariableCostom(0, 8, "va_pto2");
NexVariableCostom va_rolldoor = NexVariableCostom(0, 9, "va_rolldoor");
NexVariableCostom va_light_h = NexVariableCostom(0, 11, "va_light_h");
NexVariableCostom va_light_side = NexVariableCostom(0, 12, "va_light_side");
NexVariableCostom va_light_flash = NexVariableCostom(0, 13, "va_light_flash");
NexVariableCostom va_light_in = NexVariableCostom(0, 14, "va_light_in");
NexVariableCostom va_light_set = NexVariableCostom(0, 15, "va_light_set");
NexVariableCostom va_aud_out = NexVariableCostom(0, 22, "va_aud_out");
NexVariableCostom va_aud_switch = NexVariableCostom(0, 23, "va_aud_switch");
NexVariableCostom va_sync_light = NexVariableCostom(0, 25, "va_sync_light");
NexVariableCostom va_shield = NexVariableCostom(0, 26, "va_shield");
NexVariableCostom va_passenger = NexVariableCostom(0, 27, "va_passenger");
NexVariableCostom va_tailgate = NexVariableCostom(0, 28, "va_tailgate");
NexVariableCostom va_squareflash = NexVariableCostom(0, 29, "va_squareflash", b24_square_on.data_link);
NexVariableCostom va_head_lock = NexVariableCostom(0, 10, "va_head_lock");   // 車頭的訊號
NexVariableCostom va_proomlight = NexVariableCostom(0, 39, "va_proomlight");
NexVariableCostom va_lightsideR = NexVariableCostom(0, 40, "va_lightsideR");
NexVariableCostom va_lightsideB = NexVariableCostom(0, 41, "va_lightsideB");

// mic說明頁面
NexVariable va_mic_b0 = NexVariable(6, 11, "va_mic_b0");
NexVariable va_mic_b1 = NexVariable(6, 12, "va_mic_b1");
NexVariable va_mic_b2 = NexVariable(6, 13, "va_mic_b2");
NexVariable va_mic_b3 = NexVariable(6, 14, "va_mic_b3");
NexVariable va_mic_b4 = NexVariable(6, 15, "va_mic_b4");
NexVariable va_mic_b5 = NexVariable(6, 16, "va_mic_b5");
NexVariable va_mic_b6 = NexVariable(6, 17, "va_mic_b6");



// 頁面
NexPage p_opening = NexPage(0, 0, "opening");
NexPage p_home = NexPage(1, 0, "home");
NexPage p_alert = NexPage(2, 0, "alert");



#endif