﻿// PinDefine.h

#ifndef _PINDEFINE_h
#define _PINDEFINE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#endif
/*
2~11 保留外接
14~19 rxtx1 ~ rxtx3
21~21 IIC
50~52 SPI
*/


// BY8301
#define IO1 31		// by8310
#define IO2 33		// by8310
#define IO3 35		// by8310
#define BUSY 37						// by8310
#define AUD_SOURCE_0 23		// 音源控制板 6pin接頭(aud 3 in 1 out)
#define AUD_SOURCE_1 25		// 音源控制板 6pin接頭(aud 3 in 1 out)
#define AUD_SOURCE_2 27		// 音源控制板 6pin接頭(aud 3 in 1 out)
#define ALERT 39    // 硬控警報因
#define MCP41010_CS_PIN 53		// 數位電阻
#define LIGHT_LINE_PIN 40	// 硬控排燈
#define LIGHT_FLASH_PIN 42	// 硬控暴閃


#ifndef HAND_HELD_MIC_OUT_PIN
#define HAND_HELD_MIC_OUT_PIN 28		// 麥克風音源控制板 6pin接頭(mic 1 in 3 out)
#endif // !HAND_HELD_MIC_OUT_PIN

#ifndef ALERT_OUT_PIN
#define ALERT_OUT_PIN 30		// 麥克風音源控制板 6pin接頭(mic 1 in 3 out)
#endif // !ALERT_OUT_PIN

#ifndef COCKPIT_OUT_PIN
#define COCKPIT_OUT_PIN 32		// 麥克風音源控制板 6pin接頭(mic 1 in 3 out)
#endif // !COCKPIT_OUT_PIN

#ifndef ALERT_POWER_PIN
#define ALERT_POWER 44		// 揚聲器主機音源
#endif // !ALERT_POWER

#ifndef ALERT_MIC_POWER_PIN
#define ALERT_MIC_POWER_PIN 48		//無線電ptt乾接點
#endif // !ALERT_MIC_POWER_PIN

#ifndef SECEND_RADIO_POWER_PIN
#define SECEND_RADIO_POWER_PIN 46		//揚聲器主機電源
#endif // !SECEND_RADIO_POWER_PIN