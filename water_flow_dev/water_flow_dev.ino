#include "LedNumberDisplay.h"
#include "ReadLMag.h"

ReadLMag mag_1 = ReadLMag(1, 3.1203, 65); 
ReadLMag mag_2 = ReadLMag(2, 3.1203, 65);
ReadLMag mag_3 = ReadLMag(3, 1, 65); 
ReadLMag mag_4 = ReadLMag(4, 1, 65);

ReadLMag *read_lmag_list[] = {
	//&mag_1,
	&mag_2,
	//&mag_3,
	//&mag_4,
	NULL
};

LedNumberDisplay led_1 = LedNumberDisplay(11);

void setup() {
	Serial.begin(9600);
	unsigned long now = millis();
	readLMagInit(now, read_lmag_list);
	LedNumberDisplayInit();
	led_1.set_decimal_point(0);
	Serial.println("init water flow dev");
	mag_2.set_calibration({ 0.1, 0.99 }, 0);
}

void loop() {
	unsigned long now = millis();
	readLMagPoll(now, read_lmag_list);
	led_1.set_value(mag_1.data.damping_flow_value*60);
	delay(50);
	debug_cmd();
}

void debug_cmd() {
	if (Serial.available() == 0) { return; }
	String cmd = "";
	while (Serial.available())
	{
		cmd += (char)Serial.read();
		delay(1);
	}

	if(cmd == "help"){
		Serial.println("data\t\t查看流量計統計資料");
		Serial.println("amp [float]\t\t流量係數校正");
		Serial.println("val [int]\t\t變更led數值");
		Serial.println("cal [流速, 倍率, 紀錄位置]\t\t變更led數值");
		Serial.println("pcal \t\t設定狀態");
	}
	if (cmd == "data") {
		int index = 0;
		while (read_lmag_list[index] != NULL)
		{
			read_lmag_list[index++]->print_data();
		}
	}
	if (cmd.substring(0, 4) == "amp ") {
		int len = cmd.length();
		String float_str = cmd.substring(4, len);
		float temp = ::atof(float_str.c_str());
		int index = 0;
		while (read_lmag_list[index] != NULL)
		{
			read_lmag_list[index++]->amplification = temp;
		}
	}
	if (cmd.substring(0, 4) == "val ") {
		int len = cmd.length();
		String int_str = cmd.substring(4, len);
		int temp = ::atoi(int_str.c_str());
		led_1.set_value(temp);
		Serial.println(temp);
	}
	if (cmd.substring(0, 4) == "cal ") {
		int len = cmd.length();
		String float_str = cmd.substring(4, len);
		int read_index = 0;
		float param[3] = { 0 };
		for (int i = 0; i < 3; i++) {
			String buff;
			if (read_index >= len - 4) { break; }
			while (float_str[read_index] != ' ')
			{
				buff += float_str[read_index++];
			}
			read_index++;
			param[i] = ::atof(buff.c_str());
		}
		int index = param[2];
		mag_2.set_calibration({param[0], param[1]}, index);
	}
	if (cmd.substring(0, 4) == "pcal") {
		mag_2.show_calibration();
	}
	if (cmd.substring(0, 5) == "reset") {
		mag_2.data.total_positive = 0; 
	}
}
