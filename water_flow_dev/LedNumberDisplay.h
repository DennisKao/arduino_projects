﻿// LedNumberDisplay.h

#ifndef _LEDNUMBERDISPLAY_h
#define _LEDNUMBERDISPLAY_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef MODBUS_PIN_LED
#define MODBUS_PIN_LED 30
#endif

#ifndef MODBUS_SERIAL_LED
#define MODBUS_SERIAL_LED Serial2
#endif

HardwareSerial *modbus_port_for_led = &MODBUS_SERIAL_LED;

class LedNumberDisplay{
public:
	LedNumberDisplay(int slave_id);
	void calcCRC(uint8_t u8length, uint16_t data_list[]);
	/**
	*	設定要顯示的小數點位數
	* @param digits 小數點後面的位數
	*/
	void set_decimal_point(int digits);
	void set_value(int val);
private:
	int slave_id;
	/**
	* 發送命令
	*/
	void send_cmd(int len, uint16_t cmd[]);
};

LedNumberDisplay::LedNumberDisplay(int slave_id) {
	this->slave_id = slave_id;
}

void LedNumberDisplay::calcCRC(uint8_t u8length, uint16_t data_list[]) {
	unsigned int temp, temp2, flag;
	temp = 0xFFFF;
	for (unsigned char i = 0; i < u8length-2; i++)
	{
		temp = temp ^ data_list[i];
		for (unsigned char j = 1; j <= 8; j++)
		{
			flag = temp & 0x0001;
			temp >>= 1;
			if (flag)
				temp ^= 0xA001;
		}
	}
	// Reverse byte order.
	temp2 = temp >> 8;
	temp = (temp << 8) | temp2;
	temp &= 0xFFFF;
	// the returned value is already swapped
	// crcLo byte is first & crcHi byte is last
	data_list[u8length - 2] = temp >> 8;
	data_list[u8length - 1] = temp & 0x00ff;
}

void LedNumberDisplay::set_decimal_point(int digits) {
	uint16_t cmd[8] = {this->slave_id, 0x06, 0x00, 0x25, 0x00, digits, 0x00, 0x00};
	calcCRC((uint8_t)8, cmd);
	send_cmd(8, cmd);
}

void LedNumberDisplay::send_cmd(int len, uint16_t cmd[]) {
	digitalWrite(MODBUS_PIN_LED, HIGH);
	for (int i = 0; i < len; i++) {
		uint16_t t = *(cmd + i);
		modbus_port_for_led->write(t);
	}
	modbus_port_for_led->flush();
	//delay(3);
	//delayMicroseconds(50);
	digitalWrite(MODBUS_PIN_LED, LOW);
}

void LedNumberDisplay::set_value(int val) {
	uint16_t val_16 = val;
	uint16_t cmd[8] = { this->slave_id, 0x06, 0x00, 0x88, val_16>>8, val_16 & 0x00ff, 0x00, 0x00 };
	calcCRC((uint8_t)8, cmd);
	send_cmd(8, cmd);
}

void LedNumberDisplayInit() {
	modbus_port_for_led->begin(9600);
}
#endif




