﻿/*
 Name:		HMIElementTest.ino
 Created:	2019/3/26 下午 12:02:07
 Author:	elijah
*/
#include "Nextion.h"
NexButton b_home = NexButton(1, 1, "b01_home");

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(9600);
	delay(100);
	nexInit();
	Serial.println("init HHI Element Test");
	uint32_t number;
	b_home.obj_test(number);
}

// the loop function runs over and over again until power down or reset
void loop() {
  
}
