#include <Wire.h>

// modbus
#include <ModbusRtu.h>
#define DATA_LEN 5
uint16_t modbus_data_array[DATA_LEN] = {0};
Modbus slave(1, 0, 10);

struct ModbusData {
	byte led_mode_setting;
	byte led_speed;
	byte led_flash_light_display_mode;
	byte water_level;
};

//give a name to the group of data
ModbusData modbus_data;

//define slave i2c address
#define I2C_SLAVE_ADDRESS 8

void setup() {
	Wire.begin();
	// modbus
	slave.begin(9600);
}

void loop() {
	unsigned long now = millis();
	// modbus
	slave.poll(modbus_data_array, DATA_LEN);

	// ��s
	write_modbus_data_to_struct();

	static unsigned long send_time_stamp = 0;
	if (now - send_time_stamp > 100) {
		send_time_stamp = now;
		Wire.beginTransmission(I2C_SLAVE_ADDRESS); // transmit to device #8
		Wire.write(modbus_data.led_mode_setting);
		Wire.write(modbus_data.led_speed);
		Wire.write(modbus_data.led_flash_light_display_mode);
		Wire.write(modbus_data.water_level);
		Wire.endTransmission();
	}
}

void write_modbus_data_to_struct() {
	modbus_data.led_mode_setting = modbus_data_array[0];
	modbus_data.led_speed = modbus_data_array[1];
	modbus_data.led_flash_light_display_mode = modbus_data_array[2];
	modbus_data.water_level = modbus_data_array[3];
}
