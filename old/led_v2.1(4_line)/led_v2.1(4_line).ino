// 遠東號 pin 10 故障 改成 12
//#define DATA_PIN_BACK_LEFT 12   // 後左

// LED
#include "SLM.h"
#include "FastLED.h"
#pragma once
#include <Wire.h>

struct ModbusData {
	uint8_t led_mode_setting;
	uint8_t led_speed;
	uint8_t led_flash_light_display_mode;
	uint8_t water_level;
};

ModbusData modbus_data;


CRGB leds_0[NUM_LED]; 
LedGroup group_left = LedGroup(0, SIDE_LED_NUMBER_EACH_LINE *4, leds_0, NULL, SIDE_LED_NUMBER_EACH_LINE);
LedGroup group_right = LedGroup(SIDE_LED_NUMBER_EACH_LINE *4, SIDE_LED_NUMBER_EACH_LINE *8, leds_0, NULL, SIDE_LED_NUMBER_EACH_LINE);
LedGroup group_back = LedGroup(SIDE_LED_NUMBER_EACH_LINE *8, SIDE_LED_NUMBER_EACH_LINE *12, leds_0, NULL, SIDE_LED_NUMBER_EACH_LINE);
LedGroup group_left_top = LedGroup(SIDE_LED_NUMBER_EACH_LINE *12, SIDE_LED_NUMBER_EACH_LINE *12 + 256, leds_0, NULL, LED_NUMBER_EACH_LINE);

LedGroup *groups[] = {
  &group_left_top,
  &group_left,
  &group_right,
  &group_back,
  NULL
};


// command
// 對應狀態碼   0000  0010     0110         0011      0100  0001   0101   0111
enum FLASH_MODE{OFF, BLINK=8, BREAK, RIGHT, BREAK_RIGHT, LEFT, BREAK_LEFT, WATER_LEVEL=15};

uint8_t black = 0; // 為了用水位功能設置全黑
//int mode_setting_code[4] = {0,0,0,0};  //讀取自電位 會轉換成設定模式 FLASH_MODE
FLASH_MODE mode_setting = FLASH_MODE(OFF);    // 設定的模式
FLASH_MODE mode_now = FLASH_MODE(BLINK);    // 正在閃的模式


void setup() {
	// LED
	Serial.begin(9600);
	// for test
	//modbus_data.led_mode_setting = 4;

	Wire.begin(8);                // join i2c bus with address #8
	Wire.onReceive(receiveEvent); // register event



	LEDS.addLeds<WS2812, DATA_PIN_LEFT_FRONT, RGB>(leds_0, 0, LED_NUM_EACH_PIN);    // 左前
	LEDS.addLeds<WS2812, DATA_PIN_LEFT_BACK, RGB>(leds_0, LED_NUM_EACH_PIN, LED_NUM_EACH_PIN);   // 左後

	LEDS.addLeds<WS2812, DATA_PIN_RIGHT_FRONT, RGB>(leds_0, LED_NUM_EACH_PIN*2, LED_NUM_EACH_PIN);   // 右前
	LEDS.addLeds<WS2812, DATA_PIN_RIGHT_BACK, RGB>(leds_0, LED_NUM_EACH_PIN*3, LED_NUM_EACH_PIN);   // 右後

	LEDS.addLeds<WS2812, DATA_PIN_BACK_LEFT, RGB>(leds_0, LED_NUM_EACH_PIN*4, LED_NUM_EACH_PIN);    // 後左
	LEDS.addLeds<WS2812, DATA_PIN_BACK_RIGHT, RGB>(leds_0, LED_NUM_EACH_PIN*5, LED_NUM_EACH_PIN);    // 後右
	LEDS.addLeds<WS2812, DATA_PIN_TOP, RGB>(leds_0, LED_NUM_EACH_PIN*6, LED_NUM_TOP);    // 上方
	LEDS.setBrightness(100);
	LEDS.clear();
	FastLED.show();
	Serial.println("led v2.11 init");
}

void loop() {
	// 調整模式
	unsigned long now = millis();
	static unsigned long st = 0;

	load_mode_setting();
	update_setting();

	// led
	if (mode_now == OFF) {
		LEDS.clear();
		FastLED.show();
	}
	else {
		int i = 0;
		bool need_display = false;
		while (groups[i] != NULL) {
			if (groups[i]->do_animate()) {
				need_display = true;
			}
			i++;
		}
		if (need_display) {
			FastLED.show();
		}
	}
  
  if (Serial.available()) {
	  char cmd = 'x';
	  while (Serial.available())
	  {
		  cmd = Serial.read();
		  delay(1);
	  }
	  if (cmd == 'b') {
		  modbus_data.led_flash_light_display_mode = 2;
		  mode_setting = FLASH_MODE(BLINK);
		  Serial.println("b");
	  }
	  if (cmd == 'w') {
		  mode_setting = FLASH_MODE(WATER_LEVEL);
		  Serial.println("w");
	  }
  }
}
unsigned long _time_stamp = 0;
// 從電位讀取狀態 並轉換為設定

void load_mode_setting(){
	unsigned long now = millis();
	// 顯示模式
	static int last_mode = -1;
	static int last_falsh_light_display_mode = -1;
	static int last_led_speed = -1;

	int mode_code = modbus_data.led_mode_setting;
	//mode_code = mode_code < 0 ? 0 : mode_code > 7 ? 7 : mode_code;
	//Serial.println(mode_code);
	static unsigned long st = 0;
	if (last_mode != mode_code && now - st > 100) {
		last_mode = mode_code;
		st = now;
		mode_setting = FLASH_MODE(mode_code);
		last_falsh_light_display_mode = -1;
		last_led_speed = -1;
		return;		// 一次只做一件事
	}

	// 報閃模式
	
	int display_mode = modbus_data.led_flash_light_display_mode;
	display_mode = display_mode < 0 ? 0 : display_mode > 3 ? 3 : display_mode;
	if (last_falsh_light_display_mode != display_mode) {
		last_falsh_light_display_mode = display_mode;
		mode_now = FLASH_MODE(OFF);	// 強迫重繪led
		last_led_speed = -1;
		return;
	}

	// 報閃速度
	int led_speed = modbus_data.led_speed;
	led_speed = led_speed < 0 ? 0 : led_speed > 100 ? 100 : led_speed;
	if (last_led_speed != led_speed) {
		last_led_speed = led_speed;
		float coefficient_of_correction = 100 - led_speed;
		group_left_top.mode->coefficient_of_correction = coefficient_of_correction;
		group_left.mode->coefficient_of_correction = coefficient_of_correction;
		group_right.mode->coefficient_of_correction = coefficient_of_correction;
		group_back.mode->coefficient_of_correction = coefficient_of_correction;
		return;
	}
}

ModeProtocol* get_flash_mode_class() {
	switch (modbus_data.led_flash_light_display_mode)
	{
	case 1:
		return new ModeBlinkRB4Line5Section(BLINK_SPEED, 24, script_blink_br_7, true);
	case 2:
		return new ModeBlinkRB4Line3Section(BLINK_SPEED, 24, script_blink_br, true);
	case 3:
		return new ModeBlinkRB4Line5Section(BLINK_SPEED, 24, script_blink_br_6, true);
	default:
		return new ModeBlack();
	}
}

void update_setting(){
    if(mode_setting == mode_now){return;}
    mode_now = mode_setting;
    Serial.print("mode: ");
    Serial.println(mode_now);

    del_all_mode();
    LEDS.clear();
    if(mode_now == OFF){
        Serial.println("off mode");
    }
    else if (mode_now == BREAK){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED_256);
        group_left.mode = get_flash_mode_class();
        group_right.mode = get_flash_mode_class();
        group_back.mode = new ModeBlink(BLINK_SPEED, BLINK_SPEED);
    }
    else if (mode_now == BREAK_LEFT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED_256);
        group_left.mode = new ModeDirection(true, DIR_SPEED, FADE_OUT_SCALE);
        group_right.mode = get_flash_mode_class();
        group_back.mode = new ModeBlink(BLINK_SPEED, BLINK_SPEED);
    }
    else if (mode_now == BREAK_RIGHT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED_256);
        group_left.mode = get_flash_mode_class();
        group_right.mode = new ModeDirection(true, DIR_SPEED, FADE_OUT_SCALE);
        group_back.mode = new ModeBlink(BLINK_SPEED, BLINK_SPEED);
    }
    else if (mode_now == LEFT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED_256);
        group_left.mode = new ModeDirection(true, DIR_SPEED, FADE_OUT_SCALE);
		group_right.mode = get_flash_mode_class();
        group_back.mode = new ModeDirection(true, DIR_SPEED, FADE_OUT_SCALE);
    }
    else if (mode_now == RIGHT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED_256);
        group_left.mode = new ModeBlinkRB4Line3Section(BLINK_SPEED, 24, script_blink_br, true);
        group_right.mode = new ModeDirection(true, DIR_SPEED, FADE_OUT_SCALE);
        group_back.mode = new ModeDirection(false, DIR_SPEED, FADE_OUT_SCALE);
    }
    else if (mode_now == BLINK){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED_256);
        group_left.mode = get_flash_mode_class();
        group_right.mode = get_flash_mode_class();
        group_back.mode = get_flash_mode_class();
    }
    else if (mode_now == WATER_LEVEL){
        group_left_top.mode = new ModeWaterLevel(&modbus_data.water_level, 1, 50);
        group_left.mode = new ModePersentage(&modbus_data.water_level);
        group_right.mode = new ModePersentage(&modbus_data.water_level);
        group_back.mode = new ModePersentage(&modbus_data.water_level);
    }
}

// 刪除所有指針
void del_all_mode(){
  delete group_left_top.mode;
  delete group_left.mode;
  delete group_right.mode;
  delete group_back.mode;
  group_left_top.mode = NULL;
  group_left.mode = NULL;
  group_right.mode = NULL;
  group_back.mode = NULL;
}

void receiveEvent(int howMany) {
	while (4 < Wire.available()) {
		char c = Wire.read();
	}
	modbus_data.led_mode_setting = Wire.read();
	modbus_data.led_speed = Wire.read();
	modbus_data.led_flash_light_display_mode = Wire.read();
	modbus_data.water_level = Wire.read();
}

