// 讀取流量計
#include "ReadLMag.h"
#define MODBUS_PIN 8
ReadLMag reader = ReadLMag(MODBUS_PIN);

// modbus slave
#include <ModbusRtu.h>
#include <SoftwareSerial.h>
#define MODBUS_DATA_LEN 10
#define MODBUS_SERIAL Serial2
SoftwareSerial mySerial(10, 11);		// rx, tx
uint16_t modbus_datas[MODBUS_DATA_LEN] = { 0 };
/*
	資料定義: 設備0流量/累加流量 (0, 1)
					設備1流量/累加流量 (2, 3)
					設備2流量/累加流量 (4, 5)
					設備3流量/累加流量 (6, 7)
					合計流量/合計累加流量(8, 9)
//*/
Modbus slave(1);

void setup() {
    Serial.begin(9600);
	slave.set_pin(9);
	slave.begin(&mySerial, 9600);
    unsigned long now = millis();   

    reader.init(now);
	slave.begin(9600);
	
    Serial.println("init2");
}

unsigned long st = 0;

void loop() {
    unsigned long now = millis();
    reader.poll(now);
	//copy_data();
	slave.poll(modbus_datas, MODBUS_DATA_LEN);
	desplay_led_485();
	
    // debug
    if (now - st >= 1000){
        st = now;
        reader.print_data();
    }
    

}

/**
複製資料讓modbus讀取
*/
void copy_data() {
	int all_water_flow_val = 0;
	int all_water_flow_total = 0;
	for (int i = 0; i < 4; i++) {
		int modbus_index = i * 2;
		modbus_datas[modbus_index] = reader.data_list[i].instant_flow_value;
		all_water_flow_val += reader.data_list[i].instant_flow_value;
		modbus_datas[modbus_index + 1] = reader.total[i] / 1000;
		all_water_flow_total += reader.total[i] / 1000;
	}
	modbus_datas[8] = all_water_flow_val;
	modbus_datas[9] = all_water_flow_total;
}


/**
 * @param u8length      要計算的資料長度
 * @param au8Buffer     資料存放位置指針  要再多兩個欄位存放crc
 * ex. 要計算的資料長度是6  au8Buffer 長度必須至少八位
 * @return 新的資料長度
 */
uint8_t calcCRC(uint8_t u8length, uint16_t *au8Buffer)
{
	unsigned int temp, temp2, flag;
	temp = 0xFFFF;
	for (unsigned char i = 0; i < u8length; i++)
	{
		temp = temp ^ au8Buffer[i];
		for (unsigned char j = 1; j <= 8; j++)
		{
			flag = temp & 0x0001;
			temp >>= 1;
			if (flag)
				temp ^= 0xA001;
		}
	}
	// Reverse byte order.
	temp2 = temp >> 8;
	temp = (temp << 8) | temp2;
	temp &= 0xFFFF;
	*(au8Buffer + u8length) = temp >> 8;
	*(au8Buffer + u8length + 1) = temp & 0x00ff;
	return u8length + 2;
}

/**
 * @param device_id  顯示管id
 * @param val  顯示數值
 * @param buffer    文字緩衝
 * @return  產生的命令長度
 */
int led_485_cmd_generator(int device_id, int val, char*buffer) {
	buffer[0] = '$';
	sprintf(buffer + 1, "%03d", device_id % 1000);
	buffer[4] = ',';
	sprintf(buffer + 5, "%03d", val % 1000);
	buffer[8] = '#';
	buffer[9] = '\0';
	return 9;
}

void desplay_led_485() {
	for (int i = 0; i < 4; i++) {
		int flow_value_id = i * 2;
		int device_id = i + 5;
		digitalWrite(MODBUS_PIN, HIGH);
		char cmd_buff[10];
		int len = led_485_cmd_generator(device_id, modbus_datas[flow_value_id], cmd_buff);
		MODBUS_SERIAL.write(cmd_buff, len);
		MODBUS_SERIAL.flush();
	}
}