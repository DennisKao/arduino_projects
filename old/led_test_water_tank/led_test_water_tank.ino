#include "SLM.h"
CRGB leds_0[NUM_LED];
LedGroup group_left = LedGroup(0, 17, leds_0, NULL, 17);
LedGroup group_right = LedGroup(17, 17*2, leds_0, NULL, 17);

LedGroup *groups[] = {
  &group_left,
  &group_right,
  NULL
};
void setup()
{
	// LED
	Serial.begin(9600);
	LEDS.addLeds<WS2812, DATA_PIN_LEFT_FRONT, RGB>(leds_0, 0, LED_NUM_EACH_PIN);    // ���e
	LEDS.addLeds<WS2812, DATA_PIN_RIGHT_FRONT, RGB>(leds_0, LED_NUM_EACH_PIN, LED_NUM_EACH_PIN);    // �k�e
	LEDS.setBrightness(100);
	group_left.mode = new ModeBlinkRB4Line5Section(BLINK_SPEED, 24, script_blink_br_6, true);
	group_left.mode->coefficient_of_correction = 0;
	group_right.mode = new ModeBlinkRB4Line5Section(BLINK_SPEED, 24, script_blink_br_6, true);
	group_right.mode->coefficient_of_correction = 0;
}


void loop()
{
	// led
	int i = 0;
	bool need_display = false;
	while (groups[i] != NULL) {
		if (groups[i]->do_animate()) {
			need_display = true;
		}
		i++;
	}
	if (need_display) {
		FastLED.show();
	}

}
