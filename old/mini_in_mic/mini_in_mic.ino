/*
    Name:       mini_in_mic.ino
    Created:	2019/3/26 下午 04:44:09
    Author:     DESKTOP-CKE735S\elijah
*/
#define AVOID_LEFT_PIN 4
#define AVOID_FRONT_PIN 5
#define AVOID_RIGHT_PIN 6
#define HAND_HELD_MIC_IN_PIN 7
#define ALERT_IN_PIN 8
#define COCKPIT_IN_PIN 9
#define MUTE_PIN 10
#define BTN_7_PIN 11

#define PIN_ARRAY_LEN 8

int pin_array[PIN_ARRAY_LEN] = {
	AVOID_LEFT_PIN,
	AVOID_FRONT_PIN,
	AVOID_RIGHT_PIN,
	HAND_HELD_MIC_IN_PIN,
	ALERT_IN_PIN,
	COCKPIT_IN_PIN,
	MUTE_PIN,
	BTN_7_PIN
};
void setup()
{
	Serial.begin(9600);
	int index = 0;
	while (index < PIN_ARRAY_LEN) {
		pinMode(pin_array[index++], INPUT_PULLUP);
	}
	Serial.println("mini in mic init");
}

int last_sig = -1;
int last_sig_count = 0;
void loop()
{
	int index = 0;
	int sig = -1;
	while (index < PIN_ARRAY_LEN)
	{
		if (digitalRead(pin_array[index]) == LOW) {
			sig = index;
		}
		index++;
	}

	if (sig != -1 || true) {
		last_sig_count = last_sig == sig ? last_sig_count + 1 : 0;
		last_sig = sig;
		if (last_sig_count < 3) {
			Serial.println(sig);
		}
	}
	else {
		Serial.println(sig);
		last_sig = sig;
		last_sig_count = 0;
	}
	//Serial.flush();
	delay(100);
}
