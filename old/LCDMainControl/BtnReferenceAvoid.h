// BtnReferenceAvoid.h

#ifndef _BTNREFERENCEAVOID_h
#define _BTNREFERENCEAVOID_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "BtnReference.h"

class BtnReferenceAvoid : public BtnReference {
public:
	BtnReferenceAvoid(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn, NexObject *ref,
		int avoid_code);
	int avoid_code;     // 避讓音效代號
};


BtnReferenceAvoid::BtnReferenceAvoid(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn,
	NexObject *ref, int avoid_code)
	: BtnReference(modbus_data_index, type, btn, ref) {
	this->avoid_code = avoid_code;
}

#endif

