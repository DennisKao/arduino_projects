//
// Created by elijah on 2019/1/23.
//

#ifndef LIBRARIES_HMIDATAMANAGER_H
#define LIBRARIES_HMIDATAMANAGER_H

#include "BtnReference.h"
#include <EEPROM.h>


struct LedBlinkModeSave{
    int mode_code;    // led 模式代碼
    int bright;     // 亮度
    int speed;      // 速度
	int volume;	// 音量
};

const LedBlinkModeSave default_mode = {0, 100, 100, 100};

/**
 * @param need  是否需要重設記憶體
 */
void init_save_mode(){
    LedBlinkModeSave new_obj = default_mode;
    EEPROM.put(0, new_obj);
    Serial.println("led 存檔初始化完成");
}
/**
 * @param index index of data usually equal to  LedBlinkModeSave.mode_id
 * @return LedBlinkModeSave object
 */
LedBlinkModeSave get_save_data(){
    LedBlinkModeSave res;
    EEPROM.get(0, res);
    return res;
}

/**
 * @param new_data  新的要存的資料
 * @param index  要存的位置
 */
void put_save_data(LedBlinkModeSave new_data){
    EEPROM.put(0, new_data);
}

/**
 *  列印出來看
 */
void show_save_data(LedBlinkModeSave data){
    Serial.println("--------------------");
    Serial.print("mode_code: ");
    Serial.println(data.mode_code);
    Serial.print("bright: ");
    Serial.println(data.bright);
    Serial.print("speed: ");
    Serial.println(data.speed);
	Serial.print("volume: ");
	Serial.println(data.volume);
    Serial.println("--------------------");
}
#endif //LIBRARIES_HMIDATAMANAGER_H
