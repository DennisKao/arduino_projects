//
// Created by elijah on 2019/1/23.
//

#ifndef LIBRARIES_BTNREFERNCE_H
#define LIBRARIES_BTNREFERNCE_H
/**
 *      VAR: 按鈕連結到變數
 *      BTN: 按鈕去讀取按鈕本身的數值
 *      APH: 控制0~127 ( 0是透明 )
 *      BAR: 進度條 控制.val
 *      NUM: 數字物件
 */
enum BTN_REF_TYPE{
	VAR,	// local var
	NUM,	// 數字
    BTN,    // 按鈕
    VAR_GLO, // 全域變數
    HEAD_LOCK,  // 車頭鎖定
	READ,  // 循環數值
	UNDEFINED, // 未定義
};    // 連結的關係類型

class NexObject;

class BtnReference {
public:
    BtnReference();
    BtnReference(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn, NexObject *ref);
    NexObject *btn;      // 觸發動作的物件
    NexObject *ref;      // 被觸發後，應該去讀取的物件
    int modbus_data_index;      // 存檔位置
    BTN_REF_TYPE type;      // 被連結資料的類型
};

BtnReference::BtnReference() {}
BtnReference::BtnReference(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn, NexObject *ref)
{
	this->modbus_data_index = modbus_data_index;
	this->type = type;
	this->btn = btn;
	this->ref = ref;
}

#endif //LIBRARIES_BTNREFERNCE_H
