// BtnReferenceActiveControl.h

#ifndef _BTNREFERENCEACTIVECONTROL_h
#define _BTNREFERENCEACTIVECONTROL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "BtnReference.h"
class BtnReferenceActiveControl:public BtnReference
{
public:
	BtnReferenceActiveControl(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn, NexObject *ref, int control_pin);
	int control_pin;
private:

};

BtnReferenceActiveControl::BtnReferenceActiveControl(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn, NexObject *ref, int control_pin)
:BtnReference(modbus_data_index, type, btn, ref){
	this->control_pin = control_pin;
}


#endif

