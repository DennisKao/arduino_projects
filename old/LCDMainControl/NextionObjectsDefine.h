// NextionObjectsDefine.h

#ifndef _NEXTIONOBJECTSDEFINE_h
#define _NEXTIONOBJECTSDEFINE_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "LibPath.h"

// 一般按鈕物件
NexButton b_home = NexButton(1, 1, "b01_home");
NexButton b_pto = NexButton(1, 2, "b02_pto");
NexButton b_pto2 = NexButton(1, 3, "b03_pto2");
NexButton b_rolldoor = NexButton(1, 4, "b04_rolldoor");
NexButton b_head_lock = NexButton(1, 5, "b05_headlock");
NexButton b_light_h = NexButton(1, 6, "b06_light_h");
NexButton b_light_side = NexButton(1, 7, "b07_light_side");
NexButton b_light_flash = NexButton(1, 8, "b08_light_f");
NexButton b_light_inside = NexButton(1, 9, "b09_light_in");
NexButton b_aud_switch = NexButton(1, 10, "b10_aud_switch");
NexButton b_aud_left = NexButton(1, 11, "b11_speaker_l");
NexButton b_aud_front = NexButton(1, 12, "b12_speaker_f");
NexButton b_aud_right = NexButton(1, 13, "b13_speaker_r");
NexButton b_sync_light = NexButton(1, 19, "b19_sync_light");

NexButton b_speed_dwn = NexButton(5, 1, "b_speed_dwn");
NexButton b_speed_up = NexButton(5, 2, "b_speed_up");
NexButton b_mode_next = NexButton(5, 4, "b_mode_next");
NexButton b_mode_pre = NexButton(5, 5, "b_mode_pre");


// 變數
NexVariableCostom va_battery = NexVariableCostom(0, 2, "va_battery");
NexVariableCostom va_b_voltage = NexVariableCostom(0, 6, "va_b_voltage");    // 電壓 val
NexVariableCostom va_head_lock = NexVariableCostom(0, 10, "va_head_lock");   // 車頭的訊號
NexVariableCostom va_tank_0 = NexVariableCostom(0, 3, "va_tank_0");
NexVariableCostom va_tank_1 = NexVariableCostom(0, 4, "va_tank_1");
NexVariableCostom va_tank_2 = NexVariableCostom(0, 5, "va_tank_2");

NexVariableCostom va_pto = NexVariableCostom(0, 7, "va_pto");
NexVariableCostom va_pto2 = NexVariableCostom(0, 8, "va_pto2");
NexVariableCostom va_rolldoor = NexVariableCostom(0, 9, "va_rolldoor");
NexVariableCostom va_light_h = NexVariableCostom(0, 11, "va_light_h");
NexVariableCostom va_light_side = NexVariableCostom(0, 12, "va_light_side");
NexVariableCostom va_light_flash = NexVariableCostom(0, 13, "va_light_flash");
NexVariableCostom va_light_in = NexVariableCostom(0, 14, "va_light_in");
NexVariableCostom va_light_set = NexVariableCostom(0, 15, "va_light_set");
NexVariableCostom va_aud_out = NexVariableCostom(0, 22, "va_aud_out");
NexVariableCostom va_aud_switch = NexVariableCostom(0, 23, "va_aud_switch");
NexVariableCostom va_sync_light = NexVariableCostom(0, 24, "va_sync_light");
//NexVariable va_aud_front = NexVariable(0, 18, "va_aud_front");
//NexVariable va_aud_right = NexVariable(0, 19, "va_aud_right");

NexVariableCostom va_led_mode = NexVariableCostom(0, 20, "va_led_mode");   // led 模式
NexVariableCostom va_led_speed = NexVariableCostom(0, 21, "va_led_speed");   // led 閃爍速度
// 多數值按鈕物件
// --音源
NexButton b_aud_source = NexButton(1, 30, "cb30_audsource");
NexVariableCostom va_aud_source = NexVariableCostom(0, 16, "va_aud_source");
// --音量
NexButton b_vol_up = NexButton(1, 81, "b81_vol_up");
NexButton b_vol_down = NexButton(1, 80, "b80_vol_dowm");

NexVariableCostom vol_set = NexVariableCostom(1, 22, "vol_set");

// mic說明頁面
NexVariable va_mic_b0 = NexVariable(6, 11, "va_mic_b0");
NexVariable va_mic_b1 = NexVariable(6, 12, "va_mic_b1");
NexVariable va_mic_b2 = NexVariable(6, 13, "va_mic_b2");
NexVariable va_mic_b3 = NexVariable(6, 14, "va_mic_b3");
NexVariable va_mic_b4 = NexVariable(6, 15, "va_mic_b4");
NexVariable va_mic_b5 = NexVariable(6, 16, "va_mic_b5");



// 頁面
NexPage p_home = NexPage(1, 0, "home");
NexPage p_alert = NexPage(2, 0, "alert");
// nextion 監聽用
NexTouch *nex_listen_list[] =
{
	// 一般按鈕物件
	&b_light_h,
	&b_light_side,
	&b_light_flash,
	&b_light_inside,
	&b_aud_left,
	&b_aud_front,
	&b_aud_right,
	&b_aud_switch,
	// pages
	&p_home,
	// 多數值按鈕物件
	// --聲音
	&b_aud_source,
	&b_vol_up,
	&b_vol_down,
	&b_speed_dwn,
	&b_speed_up,
	&b_mode_next,
	&b_mode_pre,
	NULL
};

// ----------------------------
// --------dennis-------------
// write your code here
NexVariableCostom va_volume = NexVariableCostom(0, 24, "va_volume");
// ----------------------------

#endif