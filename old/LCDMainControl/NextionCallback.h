// NextionCallback.h

#ifndef _NEXTIONCALLBACK_h
#define _NEXTIONCALLBACK_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#endif
#include "NextionObjectsDefine.h"
#include "BY8301.h"
#include "HmiDataManager.h"
#include "RadioControlButton.h"

#define BINARY_DATA_LEN 15
#define DATA_LEN 16 // mod bus 資料長度
// 在.ino裡
extern uint16_t hmi_data[];
extern uint16_t hmi_binary_data[];
extern uint16_t au16data[];
extern uint16_t binary_data[];
extern RadioControlButton radio_control_button;
extern MCP4101Controller mcp41010;
extern void check_data_array();
extern bool find_ref(void *ptr, int *result_index, BtnReference *ref_list[]);
extern bool find_ref(void *ptr, int *result_index);

// 紀錄物件之間的關係
BtnReference *ref_list[] = {
		new BtnReference(2, VAR_GLO, &va_battery, &va_battery),
		new BtnReference(3, VAR_GLO, &va_b_voltage, &va_b_voltage),
		new BtnReference(4, VAR_GLO, &va_tank_0 , &va_tank_0),    // 水位計
		new BtnReference(5, VAR_GLO, &va_tank_1 , &va_tank_1),
		new BtnReference(6, VAR_GLO, &va_tank_2 , &va_tank_2),
		new BtnReference(7, VAR_GLO, &b_aud_source, &va_aud_source),     // 音源鈕

		new BtnReference(8, READ, &b_vol_up, &va_volume),     // 聲音鈕
		new BtnReference(8, READ, &b_vol_down, &va_volume),   // 聲音鈕     
		new BtnReference(9, READ, &b_mode_next, &va_led_mode),   // led 模式+
		new BtnReference(9, READ, &b_mode_pre, &va_led_mode),   // led 模式-
		new BtnReference(10, READ, &b_speed_up, &va_led_speed),   // 閃爍速度+
		new BtnReference(10, READ, &b_speed_dwn, &va_led_speed),   // 閃爍速度-

		new BtnReferenceAvoid(11, UNDEFINED, &b_aud_left, &va_aud_out, 1),       // 避讓
		new BtnReferenceAvoid(11, UNDEFINED, &b_aud_front, &va_aud_out, 2),
		new BtnReferenceAvoid(11, UNDEFINED, &b_aud_right, &va_aud_out, 3),
		NULL
};

// 二進位欄位關係表
BtnReference *ref_binary_list[] = {
		new BtnReference(0, BTN, &b_home, &b_home),   // 首頁
		new BtnReference(1, BTN, &b_pto, &va_pto),
		new BtnReference(2, BTN, &b_pto2, &va_pto2),
		new BtnReference(3, BTN, &b_rolldoor, &va_rolldoor),
		new BtnReferenceHeadLock(4, HEAD_LOCK, &b_head_lock, &va_head_lock, 0),    // 車頭定位
		new BtnReferenceActiveControl(5, UNDEFINED, &b_light_h, &va_light_h, LIGHT_HEAD_PIN),
		new BtnReference(6, BTN, &b_light_side, &va_light_side),
		new BtnReferenceActiveControl(7, UNDEFINED, &b_light_flash, &va_light_flash, LIGHT_FLASH_PIN),
		new BtnReference(8, BTN, &b_light_inside, &va_light_in),
		new BtnReference(9, BTN, &b_aud_switch , &va_aud_switch),
		new BtnReference(10, BTN, &b_sync_light , &va_sync_light),
		NULL
};

// 已切換頁面
void page_callback(void *ptr) {
	Serial.println("頁面載入完成");

	// 把hmi資料歸零
	for (int i = 0; i < DATA_LEN; i++) {
		hmi_data[i] = 0;
	}
	for (int i = 0; i < BINARY_DATA_LEN; i++) {
		hmi_binary_data[i] = 0;
	}
	// 比對資料
	check_data_array();
}

// 更改modbus預備送出的值(au16data 前半段) 切換 1 & 0
void btn_type_callback(void *ptr) {
	int ref_index = 0;
	if (!find_ref(ptr, &ref_index, ref_binary_list)) {
		Serial.println("find index error");
		return;
	}
	Serial.print("ref_index: ");
	Serial.println(ref_index);
	BtnReference *ref_obj = ref_binary_list[ref_index];

	// 放上新資料
	int data_index = ref_obj->modbus_data_index;
	int new_data = binary_data[data_index + BINARY_DATA_LEN] == 0 ? 1 : 0;    // 變更後半段的值送到前面
	binary_data[data_index] = new_data;
}

// 循環數值回呼
void read_value_callback(void *ptr) {
	Serial.println("read_value_callback");
	int ref_index;
	if (!find_ref(ptr, &ref_index)) {
		Serial.println("find index error");
		return;
	}
	BtnReference *ref_obj = ref_list[ref_index];
	NexVariableCostom *var = (NexVariableCostom*)ref_obj->ref;
	// 放上新資料
	int data_index = ref_obj->modbus_data_index;
	uint32_t new_data;
	char page_name[10] = "opening";
	int data_len = var->getGlobalValue(page_name, &new_data);
	if (data_len == 0) {
		Serial.println("get value fail");
		read_value_callback(ptr);
		return;
	}

	au16data[data_index] = new_data;
	hmi_data[data_index] = new_data;
	Serial.print("new data: ");
	Serial.println(new_data);
}

// 聲音源
void aud_callback(void *ptr) {
	read_value_callback(ptr);

	int ref_index;
	if (!find_ref(ptr, &ref_index)) {
		Serial.println("find index error");
		return;
	}
	BtnReference *ref_obj = ref_list[ref_index];

	// 放上新資料
	int data_index = ref_obj->modbus_data_index;
	int new_data = au16data[data_index];

	Serial.print("aud_callback");
	Serial.println(new_data);

	// 溢位檢查
	if (new_data > 2 || new_data < 0) {
		Serial.print("音源數值異常: ");
		Serial.println(new_data);
		return;
	}
	output_aud_source_status(new_data);
}

// 設定模式回呼
void set_mod_callback(void *ptr) {
	read_value_callback(ptr);

	// 紀錄模式
	int mod_ref_index;
	find_ref(&b_mode_next, &mod_ref_index);
	BtnReference *mod_ref_obj = (BtnReference*)ref_list[mod_ref_index];

	int speed_ref_index;
	find_ref(&b_speed_up, &speed_ref_index);
	BtnReference *speed_ref_obj = (BtnReference*)ref_list[speed_ref_index];
	LedBlinkModeSave mode_data = get_save_data();
	mode_data.mode_code = au16data[mod_ref_obj->modbus_data_index];
	mode_data.speed = au16data[speed_ref_obj->modbus_data_index];
	put_save_data(mode_data);
}

// 避讓回呼
void avoid_callback(void *ptr) {
	Serial.println("avoid_callback: ");
	// 找關聯
	int mod_ref_index = 0;
	find_ref(ptr, &mod_ref_index);
	BtnReferenceAvoid *mod_ref_obj = (BtnReferenceAvoid*)ref_list[mod_ref_index];
	// 寫入數值
	binary_data[mod_ref_obj->modbus_data_index] = mod_ref_obj->avoid_code;

	// 螢幕變色
	NexVariableCostom *var = (NexVariableCostom *)mod_ref_obj->ref;
	char page_name[10] = "opening";
	var->setGlobalValue(page_name, mod_ref_obj->avoid_code);
	// 播放音效
	Audio cmd = (Audio)mod_ref_obj->avoid_code;
	by8301_play_audio(cmd);
}

// 揚聲器主機電源
void speaker_host_power_callback(void *ptr) {
	btn_type_callback(ptr);

	int mod_ref_index = 0;
	find_ref(ptr, &mod_ref_index, ref_binary_list);
	BtnReference *mod_ref_obj = (BtnReference*)ref_list[mod_ref_index];
	radio_control_button.sperker_host_power(binary_data[mod_ref_obj->modbus_data_index] == 1 ? true:false);
}

// 音量回呼
void vol_callback(void *ptr) {
	read_value_callback(ptr);

	int ref_index = 0;
	find_ref(ptr, &ref_index);
	BtnReference *ref_obj = (BtnReference*)ref_list[ref_index];
	int vol_value = au16data[ref_obj->modbus_data_index]*10;
	
	// 檢查範圍
	if (vol_value > 100 || vol_value < 0){
		Serial.flush();
		Serial.print("vol_value out of range:");
		Serial.println(vol_value);
	}
	// 存入記憶體
	LedBlinkModeSave mode_data = get_save_data();
	mode_data.volume = vol_value;
	put_save_data(mode_data);
	// 變更電阻值
	mcp41010.change_value(vol_value);
}

// 螢幕直接控制硬控
void active_control_callback(void *ptr) {
	int ref_index = 0;
	if (!find_ref(ptr, &ref_index, ref_binary_list)) {
		Serial.println("find index error");
		return;
	}
	BtnReferenceActiveControl *ref_obj = (BtnReferenceActiveControl*)ref_binary_list[ref_index];
	int data_index = ref_obj->modbus_data_index;
	int new_data = hmi_binary_data[data_index] == 0 ? 1 : 0;
	hmi_binary_data[data_index] = new_data;
	binary_data[data_index] = new_data;
	digitalWrite(ref_obj->control_pin, new_data == 0 ? LOW:HIGH);
	// 螢幕變色
	NexVariableCostom *var = (NexVariableCostom *)ref_obj->ref;
	char page_name[10] = "opening";
	var->setGlobalValue(page_name, new_data);
}

// 加載按鈕回呼函數
void attach_callback_func() {
	b_light_h.attachPush(active_control_callback, &b_light_h);
	b_light_side.attachPush(btn_type_callback, &b_light_side);
	b_light_flash.attachPush(active_control_callback, &b_light_flash);
	b_light_inside.attachPush(btn_type_callback, &b_light_inside);
	b_aud_left.attachPush(avoid_callback, &b_aud_left);
	b_aud_front.attachPush(avoid_callback, &b_aud_front);
	b_aud_right.attachPush(avoid_callback, &b_aud_right);
	b_aud_switch.attachPush(speaker_host_power_callback, &b_aud_switch);
	// 多數值按鈕物件
	// --聲音
	//b_aud_source.attachPush(aud_callback, &b_aud_source);
	b_aud_source.attachPop(aud_callback, &b_aud_source);
	b_vol_up.attachPush(vol_callback, &b_vol_up);
	b_vol_down.attachPush(vol_callback, &b_vol_down);

	b_speed_dwn.attachPush(set_mod_callback, &b_speed_dwn);
	b_speed_up.attachPush(set_mod_callback, &b_speed_up);
	b_mode_next.attachPush(set_mod_callback, &b_mode_next);
	b_mode_pre.attachPush(set_mod_callback, &b_mode_pre);

}
