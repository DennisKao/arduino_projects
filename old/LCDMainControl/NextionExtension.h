// NextionExtension.h

#ifndef _NEXTIONEXTENSION_h
#define _NEXTIONEXTENSION_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "LibPath.h"

/**
	耎办跑计
*/

class NexVariableCostom: public NexVariable
{
public:
	NexVariableCostom(uint8_t pid, uint8_t cid, const char *name);
	/**
	 * Get val attribute of component
	 * @param global_name 办跑计纗嘿
	 * @param number - buffer storing data retur
	 * @return the length of the data
	 */
	uint32_t getGlobalValue(char global_name[10], uint32_t *number);

	/**
	 * @param global_name 办跑计纗嘿
	 * @param number
	 * @return
	 */
	bool setGlobalValue(char global_name[], uint32_t number);
};

// 猽ノ NexVariable ﹍てよ猭
NexVariableCostom::NexVariableCostom(uint8_t pid, uint8_t cid, const char *name): NexVariable(pid, cid, name){}

bool NexVariableCostom::setGlobalValue(char global_name[10], uint32_t number)
{
	char buf[10] = { 0 };
	String cmd;
	utoa(number, buf, 10);
	cmd += global_name;
	cmd += ".";
	cmd += getObjName();
	cmd += ".val=";
	cmd += buf;
	sendCommand(cmd.c_str());
	return recvRetCommandFinished();
}

uint32_t NexVariableCostom::getGlobalValue(char global_name[10], uint32_t *number)
{
	String cmd = String("get ");
	cmd += global_name;
	cmd += ".";
	cmd += getObjName();
	cmd += ".val";
	sendCommand(cmd.c_str());
	return recvRetNumber(number);
}






#endif

