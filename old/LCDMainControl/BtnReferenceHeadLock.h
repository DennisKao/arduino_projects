// BtnReferenceHeadLock.h

#ifndef _BTNREFERENCEHEADLOCK_h
#define _BTNREFERENCEHEADLOCK_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "BtnReference.h"

class BtnReferenceHeadLock : public BtnReference {
public:
	BtnReferenceHeadLock(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn, NexObject *ref,
		int lock_status);
	int lock_status;     // 車頭鎖定狀態 0是鎖定 1是打開
};


BtnReferenceHeadLock::BtnReferenceHeadLock(int modbus_data_index, BTN_REF_TYPE type, NexObject *btn,
	NexObject *ref, int lock_status)
	: BtnReference(modbus_data_index, type, btn, ref) {
	this->lock_status = lock_status;
}


#endif

