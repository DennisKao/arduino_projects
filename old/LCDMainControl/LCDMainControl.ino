
#include "BtnReferenceActiveControl.h"
#include "LibPath.h"
#include "NextionExtension.h"
#include "BtnReferenceHeadLock.h"
#include "BtnReferenceAvoid.h"
#include "MCP4101Controller.h"
#include "NextionCallback.h"
#include "BY8301.h"
#include "PinDefine.h"
#include "AlertSoundEffect.h"
#include "NextionObjectsDefine.h"
#include "RadioControlButton.h"
#include "HmiDataManager.h"
#define VIRTUAL_MODBUS true   // 是否開啟模擬modbus功能

/* TODO:
	
	微調警報音音調
*/

// ---- Modbus ----
#include <ModbusRtu.h>


uint16_t au16data[DATA_LEN*2] = {0};  // 前半段是send 後半段是receive
uint16_t binary_data[BINARY_DATA_LEN*2] = {0};
uint8_t u8state;
Modbus slave(1, 2, 34);	

// ---- HMI ----
uint16_t hmi_data[DATA_LEN] = {0};  // 比對mod bus 資料用
uint16_t hmi_binary_data[BINARY_DATA_LEN] = {0};  // 比對mod bus 資料用
unsigned long read_hmi_time_stamp = 0;
const unsigned long read_hmi_delay = 200;
unsigned long mod_bus_data_stamp = 0;
const unsigned long mod_bus_data_delay = 200;

RadioControlButton radio_control_button = RadioControlButton();
MCP4101Controller mcp41010 = MCP4101Controller();
void setup(void) {
    slave.begin(9600);
    // ---- Modbus ----
	radio_control_button.init();
	mcp41010.setup();
	pinMode(LIGHT_HEAD_PIN, OUTPUT);
	pinMode(LIGHT_FLASH_PIN, OUTPUT);

    // ---- HMI ----
    // 一般按鈕物件
    nexInit();
	attach_callback_func();

	binary_data[9 + BINARY_DATA_LEN] = 1;	// 預設電源狀態=開
    // page
    p_home.attachPop(page_callback, &p_home);

    // ---- 存檔功能 ----
    read_mode();

    // ---- 音效 ----
    init_BY8301Controller();
    init_aud_source();
    
    Serial.begin(9600);
    Serial.println("init");
	Serial.println(nexSerial);
    // debug
}

void loop(void) {
	unsigned long now = millis();
	// 讀取手持mic按鈕狀態
	radio_control_button.read_status(now);
	// 模擬modnus
    virtual_modbus2();
    // 監聽螢幕按鈕
    nexLoop(nex_listen_list);
    
    // 把二進制資料寫入modbus陣列
    int data_buffer;
    data_buffer = encode_binary(BINARY_DATA_LEN, (int*)binary_data);
    au16data[0] = data_buffer;
    
    // 讀取 ModBus
    slave.poll(au16data, DATA_LEN*2);
    virtual_modbus();
    
    // 還原modbus陣列資料 到二進制資料陣列
    data_buffer = au16data[DATA_LEN];
    decode_binary(data_buffer, (int*)binary_data);
    
    // 更新資料到螢幕
    check_data_array();

    // 音效播放完畢
    bool busy_now = digitalRead(BUSY);
    if (busy_now != busy_status){
        if (busy_now){
            Serial.println("BY8301 busy!!");
			digitalWrite(IO1, HIGH);
			digitalWrite(IO2, HIGH);
			digitalWrite(IO3, HIGH);
        }else{
            Serial.println("BY8301 not busy!!");
            by8301_play_audio(AVOID_RESET);
            char page_name[10] = "opening";
            va_aud_out.setGlobalValue(page_name, 0);
        }
        busy_status = busy_now;
    }
    // 警報音
    playSirenSound();
    
    // for playBeep1
    unsigned long t_now = micros(); //us
	
    // debug
    if (Serial.available()) {
		char cmd[20] = { '\0' };
		int index = 0;
		while (Serial.available()) {
			cmd[index++] = Serial.read();
			delay(5);
		}
		if (strcmp(cmd, "help") == 0) {
			Serial.println("b_data\t看二進位資料詳細內容");
			Serial.println("data\t看資料詳細內容");
			Serial.println("rom -i\t查看記錄模式在記憶體裡的數值");
			Serial.println("rom -r\t初始化記憶體裡的數值");
			Serial.println("aud\t看音源狀態");
			delay(100);
			Serial.println("relay off\t切斷所有relay");
			Serial.println("relay on\t打開所有relay");
			Serial.println("vol -v [0-9]\t設置音量");
			Serial.println("vol -n\t查看現在設定的音量");
			delay(100);
			Serial.println("light_test\t發送排燈訊號");
			Serial.println("flash_test\t發送暴閃燈訊號");
		}
		if (strcmp(cmd, "b_data") == 0) {
			show_binary_data();
		}
		if (strcmp(cmd, "data") == 0) {
			show_data();
		}
		if (strcmp(cmd, "rom -r") == 0) {
			LedBlinkModeSave mode_data = get_save_data();
			show_save_data(mode_data);
		}
		if (strcmp(cmd, "rom -i") == 0) {
			init_save_mode();
		}
		if (strcmp(cmd, "aud") == 0) {
			Serial.print("aud source status: ");
			Serial.print(digitalRead(8));
			Serial.print(" ");
			Serial.print(digitalRead(9));
			Serial.print(" ");
			Serial.println(digitalRead(10));
		}
		if (strcmp(cmd, "relay off") == 0) {
			radio_control_button.switch_alert_power(false);
			radio_control_button.radio_host_power(false);
			radio_control_button.sperker_host_power(false);
			Serial.println("relay_off");
		}
		if (strcmp(cmd, "relay on") == 0) {
			radio_control_button.switch_alert_power(true);
			radio_control_button.radio_host_power(true);
			radio_control_button.sperker_host_power(true);
			Serial.println("relay_on");
		}
		if (strncmp(cmd, "vol -v ", 7) == 0) {
			mcp41010.change_value((cmd[7] - '0') * 10);
			Serial.print("變更音量數值: ");
			Serial.println((cmd[7] - '0') * 10);
		}
		if (strcmp(cmd, "vol -n") == 0) {
			Serial.print("音量數值: ");
			Serial.println(mcp41010.level_now);
		}
		if (strcmp(cmd, "light_test") == 0) {
			Serial.println("light_test");
			active_control_callback(&b_light_h);
		}
		if (strcmp(cmd, "flash_test") == 0) {
			active_control_callback(&b_light_flash);
			Serial.println("flash_test");
		}
		
	}
}

// 檢查plc資料一致性
void check_data_array() {
    unsigned long now = millis();
    if ((now - mod_bus_data_stamp) < mod_bus_data_delay){
        return;
    }
    mod_bus_data_stamp = now;
    for (int index = 0; index < DATA_LEN; index++) {
        if (au16data[index+DATA_LEN] != hmi_data[index]) {  // 資料不同需要更新
            if(index == 0){   // 按鈕的2進位數值
                check_binary_data_array();
            }else{
                update_to_hmi(index);            
            }
        }
    }
}

void check_binary_data_array() {
    for (int index = 0; index < BINARY_DATA_LEN; index++) {
        if (binary_data[index+BINARY_DATA_LEN] != hmi_binary_data[index]) {  // 資料不同需要更新
            update_to_hmi_btn(index);
            //Serial.print("update_to_hmi_btn index: ");
            //Serial.println(index);
        }
    }
    hmi_data[0] = au16data[DATA_LEN];
}

// 更新plc資料到螢幕
void update_to_hmi(int index) {
    // 找尋關聯
    int ref_index=0;
    bool result_final = true;
    while(ref_list[ref_index] != NULL){
        BtnReference *ref_obj = ref_list[ref_index];
        if (ref_obj->modbus_data_index == index) {
            // 分辨模式
            bool result = false;
            if (ref_obj->type == VAR) {
                NexVariableCostom *var = (NexVariableCostom *)ref_obj->ref;
                result = var->setValue(au16data[index+DATA_LEN]);
            }
            else if (ref_obj->type == VAR_GLO){
                NexVariableCostom *var = (NexVariableCostom *)ref_obj->ref;
                char page_name[10] = "opening";
                result = var->setGlobalValue(page_name, au16data[index+DATA_LEN]);
            }
            else if (ref_obj->type == NUM){
                NexNumber *var = (NexNumber *)ref_obj->ref;
                result = var->setValue(au16data[index+DATA_LEN]);
            }
			else if (ref_obj->type == READ) {
				// 不主動更新到螢幕
				result = true;
			}
            else{
                Serial.print("資料格式未定: ");
                Serial.println(ref_obj->type);
                result = true;
            }
            if (!result){
                result_final = false;
                Serial.print("錯誤 欄位: ");
                Serial.print(index);
                Serial.print(" type: ");
                Serial.print(ref_obj->type);
                Serial.print(" val: ");
                Serial.println(au16data[index]);
                NexObject *obj = (NexObject *)ref_obj->ref;
                obj->printObjInfo();
            }
            break;
        }
        ref_index++;
    }
    
    if (result_final || true){
        hmi_data[index] = au16data[index+DATA_LEN];
        au16data[index] = au16data[index+DATA_LEN];
    }
}

// 更新plc資料到螢幕 2進位專用
void update_to_hmi_btn(int index){
    // 找尋關聯
    int ref_index=0;
    bool result_final = true;
    while(ref_binary_list[ref_index] != NULL){
        BtnReference *ref_obj = ref_binary_list[ref_index];
        bool result=true;
        if (ref_binary_list[ref_index]->modbus_data_index == index && ref_obj->type == BTN) {      
            NexVariableCostom *var = (NexVariableCostom *)ref_obj->ref;
            char page_name[10] = "opening";
            result = var->setGlobalValue(page_name, binary_data[index+BINARY_DATA_LEN]);
        }
        else if (ref_binary_list[ref_index]->modbus_data_index == index && ref_obj->type == HEAD_LOCK){
            NexVariableCostom *var = (NexVariableCostom *)ref_obj->ref;
            char page_name[10] = "opening";
            result = var->setGlobalValue(page_name, binary_data[index+BINARY_DATA_LEN]);

			BtnReferenceHeadLock* mode_ref_obj = (BtnReferenceHeadLock*)ref_obj;
            if (binary_data[index+BINARY_DATA_LEN] == 0){
                mode_ref_obj->lock_status=0;
                p_home.show();
            }else if(mode_ref_obj->lock_status == 0){
                mode_ref_obj->lock_status =1;
                p_alert.show();
            }
         }
         
		if (!result){
              result_final = false;
              Serial.print("-錯誤 欄位: ");
              Serial.print(index);
              Serial.print(" type: ");
              Serial.print(ref_obj->type);
              Serial.print(" val: ");
              Serial.println(au16data[index]);
              NexObject *obj = (NexObject *)ref_obj->ref;
              obj->printObjInfo();
         }
         ref_index++;
    }
    hmi_binary_data[index] = binary_data[index+BINARY_DATA_LEN];
}


// 找尋關聯檔案
bool find_ref(void *ptr, int *result_index, BtnReference *ref_list[]){
    int ref_index = 0;
    while(ref_list[ref_index] != NULL){
        if(ref_list[ref_index]->btn == ptr){
            *result_index = ref_index;
            return true;
        }
        ref_index++;
    }
    return false;
}
bool find_ref(void *ptr, int *result_index){
    return find_ref(ptr, result_index, ref_list);
}


// 避讓硬控按鈕
unsigned long avoid_hardware_controll_time_stamp = 0;

// 讀取EEPROM裡的資料
void read_mode(){
    // 紀錄模式
    LedBlinkModeSave mode_data = get_save_data();
	if (mode_data.mode_code > 5 || mode_data.mode_code<0) {
		Serial.println("led 存檔可能尚未初始化!!!");
		delay(100);
		Serial.println("led 存檔可能尚未初始化!!!");
		delay(100);
		Serial.println("led 存檔可能尚未初始化!!!");
		delay(100);
		Serial.println("輸入 i 來進行記憶體初始化");
	}
    int mod_ref_index;
    find_ref(&b_mode_next, &mod_ref_index);
	BtnReference *mod_ref_obj = (BtnReference*)ref_list[mod_ref_index];
	
    int speed_ref_index;
    find_ref(&b_speed_up, &speed_ref_index);
	BtnReference *speed_ref_obj = (BtnReference*)ref_list[speed_ref_index];

	int volume_ref_index;
	find_ref(&b_vol_up, &volume_ref_index);
	BtnReference *volume_ref_obj = (BtnReference*)ref_list[volume_ref_index];
	
	// 寫入modbus
    au16data[mod_ref_obj->modbus_data_index] = mode_data.mode_code;
    au16data[speed_ref_obj->modbus_data_index] = mode_data.speed;
	au16data[volume_ref_obj->modbus_data_index] = mode_data.volume;
	
	// 寫入螢幕
	char page_name[10] = "opening";
	NexVariableCostom *var_mode = (NexVariableCostom*)mod_ref_obj->ref;
	var_mode->setGlobalValue(page_name, mode_data.mode_code);
	NexVariableCostom *var_speed = (NexVariableCostom*)speed_ref_obj->ref;
	var_speed->setGlobalValue(page_name, mode_data.speed);
	NexVariableCostom *var_volume = (NexVariableCostom*)volume_ref_obj->ref;
	var_volume->setGlobalValue(page_name, mode_data.volume/10);
}

/**
 * @param data_len  資料長度
 * @param data  2進位資料陣列
 * @return  10進制數字
 */
int encode_binary(int data_len, const int data[]){
    int res = 0;
    int int_buff;
    for(int i = 0; i<data_len; i++){
        int_buff = data[i]%2;
        int_buff = int_buff << i;
        res += int_buff;
    }
    return res;
}

/**
 * @param data   輸入10進制數字
 * @param res_len  存放結果的緩衝區大小
 * @param start_index  放置結果的起點
 * @param res   存結果的陣列
 * @return  總結果長度
 */
void decode_binary(int data, int res[BINARY_DATA_LEN*2]){
    for(int index = BINARY_DATA_LEN; index<BINARY_DATA_LEN*2; index++){
        res[index] = data%2;
        data /= 2;
    }
}

// 模擬modbus 功能
void virtual_modbus(){
    if(!VIRTUAL_MODBUS){return;}
    for(int i = 0; i<DATA_LEN; i++){
        au16data[DATA_LEN+i] = au16data[i];
    }
}
void virtual_modbus2(){
    if(!VIRTUAL_MODBUS){return;}
    for(int i = 0; i<BINARY_DATA_LEN; i++){
        binary_data[i] = binary_data[i+BINARY_DATA_LEN];
    }
}
// 列印按鈕們的狀態
void show_binary_data(){
    Serial.println();
    Serial.println("-----------------------");
    Serial.println("send:");
    for(int i=0;i<BINARY_DATA_LEN;i++){
        Serial.print(binary_data[i]);
        Serial.print("  "); 
    }
    Serial.println();
    Serial.println("recive:");
    for(int i=BINARY_DATA_LEN;i<BINARY_DATA_LEN*2;i++){
        Serial.print(binary_data[i]);
        Serial.print("  ");
    }
    Serial.println();
    Serial.println("hmi:");
    for(int i=0;i<BINARY_DATA_LEN;i++){
        Serial.print(hmi_binary_data[i]);
        Serial.print("  "); 
    }
    Serial.println();
    Serial.println("-----------------------");
}
// 列印modbus 資料
void show_data() {
	Serial.println();
	Serial.println("-----------------------");
	Serial.println("send:");
	for (int i = 0; i < DATA_LEN; i++) {
		Serial.print(au16data[i]);
		Serial.print("  ");
	}
	Serial.println();
	Serial.println("recive:");
	for (int i = DATA_LEN; i < DATA_LEN * 2; i++) {
		Serial.print(au16data[i]);
		Serial.print("  ");
	}
	Serial.println();
	Serial.println("hmi:");
	for (int i = 0; i < DATA_LEN; i++) {
		Serial.print(au16data[i]);
		Serial.print("  ");
	}
	Serial.println();
	Serial.println("-----------------------");
}

