// AlertSoundEffect.h

#ifndef _ALERTSOUNDEFFECT_h
#define _ALERTSOUNDEFFECT_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#endif

#include <avr/pgmspace.h> //http://www.nongnu.org/avr-libc/user-manual/group__avr__pgmspace.html 
#include <toneAC.h> //plays on pins 9 & 10 for ATmega328 for mega 11 12
#include "Pindefine.h"

//API
void playSirenSound(byte volume, unsigned int period_us);
void playBeep1();

// ToneAC
extern bool busy_status;
byte sirenVolume = 10; //for ToneAC; from 0-10
const unsigned int SIREN_SLOW = 1000; //us; desired delta time between siren freq updates
const unsigned int SIREN_FAST = 5000; //us
unsigned int sirenPeriod = SIREN_SLOW; //us

void playSirenSound(byte volume = sirenVolume, unsigned int period_us = SIREN_SLOW);
//sine wave lookup table to produce the siren sound
static const unsigned int sineLookupTable[] PROGMEM =
{
	//Sine wave lookup table generator: 
	// - https://daycounter.com/Calculators/Sine-Generator-Calculator.phtml
	//Number of points: 256
	//Max Amplitude: 255
	//Numbers per row: 8
	//Decimal
  250,253,256,259,262,265,268,271,
  275,278,281,284,287,290,293,296,
  299,302,305,308,311,314,317,320,
  323,326,328,331,334,337,340,343,
  346,348,351,354,357,360,362,365,
  368,371,373,376,379,381,384,386,
  389,391,394,396,399,401,404,406,
  409,411,413,416,418,420,422,425,
  427,429,431,433,435,437,439,441,
  443,445,447,449,451,453,454,456,
  458,460,461,463,464,466,468,469,
  470,472,473,475,476,477,479,480,
  481,482,483,484,485,486,487,488,
  489,490,491,492,493,493,494,495,
  495,496,496,497,497,498,498,498,
  499,499,499,500,500,500,500,500,499,499,
  499,498,498,498,497,497,496,496,
  495,495,494,493,493,492,491,490,
  489,488,487,486,485,484,483,482,
  481,480,479,477,476,475,473,472,
  470,469,468,466,464,463,461,460,
  458,456,454,453,451,449,447,445,
  443,441,439,437,435,433,431,429,
  427,425,422,420,418,416,413,411,
  409,406,404,401,399,396,394,391,
  389,386,384,381,379,376,373,371,
  368,365,362,360,357,354,351,348,
  346,343,340,337,334,331,328,326,
  323,320,317,314,311,308,305,302,
  299,296,293,290,287,284,281,278,
  275,271,268,265,262,259,256,253,
  250,247,244,241,238,235,232,229,
  225,222,219,216,213,210,207,204,
  201,198,195,192,189,186,183,180,
  177,174,172,169,166,163,160,157,
  154,152,149,146,143,140,138,135,
  132,129,127,124,121,119,116,114,
  111,109,106,104,101,99,96,94,
  91,89,87,84,82,80,78,75,
  73,71,69,67,65,63,61,59,
  57,55,53,51,49,47,46,44,
  42,40,39,37,36,34,32,31,
  30,28,27,25,24,23,21,20,
  19,18,17,16,15,14,13,12,
  11,10,9,8,7,7,6,5,
  5,4,4,3,3,2,2,2,
  1,1,1,0,1,1,
  1,2,3,4,
  5,5,6,7,8,9,10,
  11,12,13,14,15,16,17,18,
  19,20,21,23,24,25,27,28,
  30,31,32,34,36,37,39,40,
  42,44,46,47,49,51,53,55,
  57,59,61,63,65,67,69,71,
  73,75,78,80,82,84,87,89,
  91,94,96,99,101,104,106,109,
  111,114,116,119,121,124,127,129,
  132,135,138,140,143,146,149,152,
  154,157,160,163,166,169,172,174,
  177,180,183,186,189,192,195,198,
  201,204,207,210,213,216,219,222,
  225,229,232,235,238,241,244,247,
};
const unsigned int NUM_SINEWAVE_ELEMENTS = sizeof(sineLookupTable) / sizeof(unsigned int);
unsigned long t_now = 0;
int speed1 = 0, step1 = 0;

void playSirenSound(byte volume, unsigned int period_us) {
	// �Ұʱ���
	// (digitalRead(ALERT) == LOW )
	if (busy_status ) {
		noToneAC();
		return;
	}

	static unsigned long t_start = micros(); //us
	unsigned long t_now = micros(); //us
	if (t_now - t_start >= period_us)
	{
		//Serial.println("x");
		t_start = t_now; //us; update
		static unsigned int sirenIndex = 0;
		unsigned int freq = pgm_read_word(sineLookupTable + sirenIndex);
		sirenIndex++;
		if (sirenIndex >= NUM_SINEWAVE_ELEMENTS)
			sirenIndex = 0; //start back at beginning of sine wave 
		toneAC(freq + 635, volume);
	}
}

void playBeep1() {
	//local vars
	static unsigned long t_start1 = micros(); //us
	if (t_now - t_start1 >= 5000)
	{
		t_start1 = t_now; //us; update
		speed1 = speed1 + step1;
		if ((speed1 == 0) || (speed1 == 200)) {
			step1 = -step1;
		}
		toneAC(550 + speed1);
	}
}