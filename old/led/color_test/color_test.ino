#define CLK_PIN 2 // 定義連接腳位
#define DT_PIN 3
#define SW_PIN 4
#define MAX_PAGE 255

volatile long count = 0;
volatile bool increasing = true;
unsigned long t = 0;

// led
#include <FastLED.h>
CRGB leds_0[16];


void setup() {
  Serial.begin(9600);
  // 當狀態下降時，代表旋轉編碼器被轉動了
  attachInterrupt(0, rotaryEncoderChanged, FALLING);
  pinMode(CLK_PIN, INPUT_PULLUP); // 輸入模式並啟用內建上拉電阻
  pinMode(DT_PIN, INPUT_PULLUP);
  pinMode(SW_PIN, INPUT_PULLUP);
  
  // led
  LEDS.addLeds<WS2812, 7, RGB>(leds_0, 0, 16);
  LEDS.setBrightness(100);
  for (int i = 0; i < 16; i++) {
	  leds_0[i] = CRGB(0, 255, 10);
  }
  
  FastLED.show();
  Serial.println("init");
}

void loop() {
  if (digitalRead(SW_PIN) == LOW) { // 按下開關，歸零
    //count = 0;
    //Serial.println("count reset to 0");
  }
}

void rotaryEncoderChanged() { // when CLK_PIN is FALLING
  //教學
  unsigned long temp = millis();
  if (temp - t < 10) // 靈敏度
    return;
  t = temp;

  // DT_PIN的狀態代表正轉或逆轉
  count += digitalRead(DT_PIN) == HIGH ? 1 : -1;

  // 限制輸出範圍
  if (count > MAX_PAGE) {
    count = 0;
  }
  else if (count < 0) {
    count = MAX_PAGE;
  }

  Serial.println(count);
  
  leds_0[0] = CHSV(count, 255, 255);
  
  FastLED.show();
}
