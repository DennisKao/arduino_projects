#include <FastLED.h>
#define PIN_17 2
#define PIN_256 11
#define LED_LEN 17*2

CRGB leds[LED_LEN + 256];

unsigned long time_stamp = 0;   // 時間戳
const unsigned long time_interval = 200;   // 時間間隔
int led_count = 0;   // 燈index

void setup() {
    Serial.begin(9600);  
    LEDS.addLeds<WS2812, PIN_17, RGB>(leds, 0, LED_LEN);
    LEDS.addLeds<WS2812, PIN_256, RGB>(leds, LED_LEN, 256);
    LEDS.setBrightness(100);
    
    Serial.print("init v01");
}

void loop() {
    for(int i=0; i<LED_LEN; i++){
        leds[i] = CRGB(255, 255, 255);
    }
    for(int i=LED_LEN; i<LED_LEN+256; i++){
        leds[i] = CRGB(255, 255, 255);
    }
    FastLED.show();
}
