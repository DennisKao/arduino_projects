 // LED
#include <SLM.h>
#include "FastLED.h"
#pragma once
#define NUM_LED 17*2*6+256
#define DATA_PIN_0 5   // 左上

CRGB leds_0[NUM_LED];
LedGroup group_left_top = LedGroup(0, 17*1, leds_0, NULL, 17);
int black = 60;

void setup() {
  Serial.begin(9600);
  LEDS.addLeds<WS2812, 6, RGB>(leds_0, 0, 17*2);    // 左前
  LEDS.addLeds<WS2812, 7, RGB>(leds_0, 17*2, 17*2);   // 左後
  LEDS.addLeds<WS2812, 8, RGB>(leds_0, 17*2*2, 17*2);   // 右前
  LEDS.addLeds<WS2812, 9, RGB>(leds_0, 17*2*3, 17*2);   // 右後
  LEDS.addLeds<WS2812, 10, RGB>(leds_0, 17*2*4, 17*2);    // 後左
  LEDS.addLeds<WS2812, 11, RGB>(leds_0, 17*2*5, 17*2);    // 後右
  LEDS.addLeds<WS2812, 5, RGB>(leds_0, 17*2*6, 256);    // 上方
  LEDS.setBrightness(30);
  group_left_top.mode = new ModeBlinkRB_4Line_5_section(BLINK_SPEED, 56, script_blink_br_8, true);
  group_left_top.do_animate();
  FastLED.show();
  Serial.println("init");
}

void loop() {
  // led
    int i = 0;
    bool need_display = false;
    if(group_left_top.do_animate()){
       need_display = true;
    }
    if (need_display){
      FastLED.show();
    }
  
}
