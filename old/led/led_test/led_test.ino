 // LED
#include <SLM.h>
#include "FastLED.h"
#pragma once
CRGB leds_0[NUM_LED];
LedGroup group_left = LedGroup(0, 17*4, leds_0, NULL, 17);
LedGroup group_right = LedGroup(17*2*2, 17*2*4, leds_0, NULL, 17);
LedGroup group_back = LedGroup(17*2*4, 17*2*6, leds_0, NULL, 17);

LedGroup group_left_top = LedGroup(17*2*6, 17*2*6 + 256, leds_0, NULL, 16);

LedGroup *groups[] = {
  &group_left_top,
  &group_left,
  &group_right,
  &group_back,
  NULL
};

// modbus
#include <ModbusRtu.h>
#define DATA_LEN 5
uint16_t au16data[DATA_LEN] = {40};
uint16_t led_data[DATA_LEN] = {40};
Modbus slave(1, 2, 40);

// command
// 對應狀態碼   0000  0010     0110         0011      0100  0001   0101   0111
enum FLASH_MODE{OFF, BREAK, BREAK_LEFT, BREAK_RIGHT, LEFT, RIGHT, BLINK, WATER_LEVEL};
#define MODE_PIN_0 2
#define MODE_PIN_1 3
#define MODE_PIN_2 4
#define MODE_PIN_3 12
const int black = 0; // 為了用水位功能設置全黑
int mode_setting_code[4] = {0,0,0,0};  //讀取自電位 會轉換成設定模式 FLASH_MODE
FLASH_MODE mode_setting = OFF;    // 設定的模式
FLASH_MODE mode_now = OFF;    // 正在閃的模式


void setup() {
  // LED
  Serial.begin(9600);
  LEDS.clear();
  LEDS.addLeds<WS2812, 6, RGB>(leds_0, 0, 17*2);    // 左前
  LEDS.addLeds<WS2812, 7, RGB>(leds_0, 17*2*1, 17*2);   // 左後
  LEDS.addLeds<WS2812, 8, RGB>(leds_0, 17*2*2, 17*2);   // 右前
  LEDS.addLeds<WS2812, 9, RGB>(leds_0, 17*2*3, 17*2);   // 右後
  LEDS.addLeds<WS2812, 10, RGB>(leds_0, 17*2*4, 17*2);    // 後左
  LEDS.addLeds<WS2812, 11, RGB>(leds_0, 17*2*5, 17*2);    // 後右
  LEDS.addLeds<WS2812, 5, RGB>(leds_0, 17*2*6, 256);    // 上方
  LEDS.setBrightness(10);
  
  group_left_top.mode = new ModePersentage(&black);
  group_left.mode = new ModePersentage(&black);
  group_right.mode = new ModePersentage(&black);
  group_back.mode =  new ModePersentage(&black);
  
  // command
  pinMode(MODE_PIN_0, INPUT);
  pinMode(MODE_PIN_1, INPUT);
  pinMode(MODE_PIN_2, INPUT);
  pinMode(MODE_PIN_3, INPUT);
  
  // modbus
  slave.begin(9600);
  Serial.println("init 005");
}

void loop() {
    // led
    int i = 0;
    bool need_display = false;
    while (groups[i] != NULL) {
      if(groups[i]->do_animate()){
          need_display = true;
      }
      i++;
    }
    if (need_display){
      FastLED.show();
    }
  

  // modbus
  slave.poll(au16data, DATA_LEN);

  // 調整模式
  load_mode_setting();
  update_setting();
}
unsigned long _time_stamp = 0;
// 從電位讀取狀態 並轉換為設定

void load_mode_setting(){
    unsigned long now = millis();
    if(now - _time_stamp < 300){
        return false;
    }
    _time_stamp = now;
    char mod_str[5];
    // 讀取電位
    int pins[4] = {MODE_PIN_0, MODE_PIN_1, MODE_PIN_2, MODE_PIN_3};
    for (int i = 0; i < 4; i++){
      mode_setting_code[i] = digitalRead(pins[i]);
    }
    // 轉寫成文字
    int code = 1000*mode_setting_code[0] + 100*mode_setting_code[1] + 10*mode_setting_code[2] + mode_setting_code[3];
    sprintf(mod_str, "%04d", code);
    // 開始分析
    if(strcmp(mod_str, "0000") == 0){
        mode_setting = OFF;
    }
    else if(strcmp(mod_str, "0010") == 0){
        mode_setting = BREAK;
    }
    else if(strcmp(mod_str, "0110") == 0){
        mode_setting = BREAK_LEFT;
    }
    else if(strcmp(mod_str, "0011") == 0){
        mode_setting = BREAK_RIGHT;
    }
    else if(strcmp(mod_str, "0100") == 0){
        mode_setting = LEFT;
    }
    else if(strcmp(mod_str, "0001") == 0){
        mode_setting = RIGHT;
    }
    else if(strcmp(mod_str, "0101") == 0){
        mode_setting = BLINK;
    }
    else if(strcmp(mod_str, "0111") == 0){
        mode_setting = WATER_LEVEL;
    }
}

void update_setting(){
    if(mode_setting == mode_now){return;}
    mode_now = mode_setting;
    Serial.print("mode: ");
    Serial.println(mode_now);
    
    del_all_mode();
    LEDS.clear();
    if(mode_now == OFF){
        Serial.println("off mode");
        group_left_top.mode = new ModePersentage(&black);
        group_left.mode = new ModePersentage(&black);
        group_right.mode = new ModePersentage(&black);
        group_back.mode = new ModePersentage(&black);
    }
    else if (mode_now == BREAK){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED);
        group_left.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_right.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_back.mode = new ModeBlink(BLINK_SPEED, BLINK_SPEED);
    }
    else if (mode_now == BREAK_LEFT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED);
        group_left.mode = new ModeDirection(true, BLINK_SPEED, 80);
        group_right.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_back.mode = new ModeBlink(BLINK_SPEED, BLINK_SPEED);
    }
    else if (mode_now == BREAK_RIGHT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED);
        group_left.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_right.mode = new ModeDirection(true, BLINK_SPEED, 80);
        group_back.mode = new ModeBlink(BLINK_SPEED, BLINK_SPEED);
    }
    else if (mode_now == LEFT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED);
        group_left.mode = new ModeDirection(true, BLINK_SPEED, 80);
        group_right.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_back.mode = new ModeDirection(false, BLINK_SPEED, 100);
    }
    else if (mode_now == RIGHT){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED);
        group_left.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_right.mode = new ModeDirection(true, BLINK_SPEED, 80);
        group_back.mode =new ModeDirection(true, BLINK_SPEED, 80);
    }
    else if (mode_now == BLINK){
        group_left_top.mode = new ModeBlinkBR_16Line(BLINK_SPEED);
        group_left.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_right.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
        group_back.mode = new ModeBlinkRB_4Line(BLINK_SPEED*20, script_blink_br, true);
    }
    else if (mode_now == WATER_LEVEL){
        group_left_top.mode = new ModeWaterLevel(&au16data[0], 1, 1000);
        group_left.mode = new ModePersentage(&au16data[0]);
        group_right.mode = new ModePersentage(&au16data[0]);
        group_back.mode = new ModePersentage(&au16data[0]);
    }
}

// 刪除所有指針
void del_all_mode(){
  delete group_left_top.mode;
  delete group_left.mode;
  delete group_right.mode;
  delete group_back.mode;
}
