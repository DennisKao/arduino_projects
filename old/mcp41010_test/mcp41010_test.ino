#include <SPI.h>
byte address = 0x11;
int CS = 53;
int i = 0;
#include <lib_test.h>

void setup()
{
	Serial.begin(9600);
	pinMode(CS, OUTPUT);
	SPI.begin();
	digitalPotWrite(0xff, CS);
	delay(500);
}

void loop()
{
	if (Serial.available()) {
		char cmd = Serial.read();
		if (cmd == '1') {
			digitalPotWrite(0x01, CS);
		}
		else if (cmd == '2') {
			digitalPotWrite(0x2b, CS);
		}
		else if (cmd == '3') {
			digitalPotWrite(0x6e, CS);
		}
		Serial.println(cmd);
	}
}

int digitalPotWrite(int value, int pin)
{
	digitalWrite(pin, LOW);
	SPI.transfer(address);
	SPI.transfer(value);
	digitalWrite(pin, HIGH);
}

