#include "LedModbusTranslation.h"
#ifdef UBRR3H	// 判斷是mega還是nano
#include "NextionObjectDefine.h"
#include "NextionExtension.h"
#include "ReadLMag.h"
#endif // UBRR3H



// ---- Modbus ----
#include <ModbusRtu.h>
#define DATA_LEN 16
uint16_t modbus_data_array[DATA_LEN] = {0}; // 13 ~ 16 要給led指令用


#ifdef UBRR2H
#define MODBUS_SERIAL_PORT 2
#define MODBUS_CONTROL_PIN 31
#else
#define MODBUS_SERIAL_PORT 0
#define MODBUS_CONTROL_PIN 10
#endif // UBRR2H

Modbus slave(3, MODBUS_SERIAL_PORT, MODBUS_CONTROL_PIN);

#ifdef UBRR3H
ReadLMag mag_0 = ReadLMag(1, 1.0, 65);
ReadLMag mag_1 = ReadLMag(2, 1.0, 65);
ReadLMag mag_2 = ReadLMag(3, 1.0, 65);
ReadLMag mag_3 = ReadLMag(4, 1.0, 65);
// ---- Mags ----
ReadLMag *mags[] = {
	&mag_0,
	&mag_1,
	&mag_2,
	&mag_3,
	NULL
};
#endif // UBRR3H

void setup() {
	// modbus
	slave.begin(57600);
	//Led
	Wire.begin();
	// hmi
#ifdef UBRR3H
	Serial.begin(9600);
    nexInit();
	attach_all_hmi_user_func();
	
	// mag
	readLMagInit(millis(), mags);
	/*mag_0.send_reset();
	mag_1.send_reset();
	mag_2.send_reset();
	mag_3.send_reset();*/
	Serial.println("10 inch v1.2 init");
#endif // UBRR3H
}


void loop() {
    // ---- Modbus ----
    slave.poll(modbus_data_array, DATA_LEN);
	write_modbus_data_to_struct();
	send_data_to_led_contriller(millis());
	

#ifdef UBRR3H
    // 讀取流量計
	readLMagPoll(millis(), mags);
	// 更新
	update_mag_flow_value_data_to_hmi();
	// 更新資料到螢幕
	send_data_to_nextion_hmi_loop();
	updata_pto_status();
	debug_tools();
#endif // UBRR3H
	
}

void debug_tools() {
	if (Serial.available()) {
		char cmd[20] = { '\0' };
		int index = 0;
		while (Serial.available()) {
			cmd[index++] = Serial.read();
			delay(5);
		}
		if (strcmp(cmd, "help") == 0) {
			Serial.println("mag_data\t\t顯示流量計資料");
		}
		else if (strcmp(cmd, "mag_data") == 0) {
			Serial.println("mag_data");
#ifdef UBRR3H
			int index = 0;
			while (mags[index] != NULL)
			{
				mags[index++]->print_data();
			}
#endif // UBRR3H
		}
		else if (strcmp(cmd, "modbus") == 0) {
			for (int i = 0; i < 16; i++) {
				Serial.print(modbus_data_array[i]);
				Serial.print("  ");
			}
			Serial.println();
		}
		else if (strcmp(cmd, "modbus") == 0) {
			//mag_0.data.total_positive += 13;
		}
	}
}


#ifdef UBRR3H
void update_mag_flow_value_data_to_hmi() {
	// 更新及時流量到data link
	int tiny_val_cut = 50;
	va_wout_nL1.data_link->send_val_to_hmi = mag_0.data.instant_flow_value * 60 > tiny_val_cut ? mag_0.data.instant_flow_value * 60 : 0;
	va_wout_nL2.data_link->send_val_to_hmi = mag_1.data.instant_flow_value *60 > tiny_val_cut ? mag_1.data.instant_flow_value * 60 : 0;
	va_wout_nR2.data_link->send_val_to_hmi = mag_2.data.instant_flow_value *60 > tiny_val_cut ? mag_2.data.instant_flow_value * 60 : 0;
	va_wout_nR1.data_link->send_val_to_hmi = mag_3.data.instant_flow_value *60 > tiny_val_cut ? mag_3.data.instant_flow_value * 60 : 0;
	
	// 更新累計流量到data link
	va_wtotal_L1.data_link->send_val_to_hmi = mag_0.data.total_positive*10;
	va_wtotal_L2.data_link->send_val_to_hmi = mag_1.data.total_positive*10;
	va_wtotal_R2.data_link->send_val_to_hmi = mag_2.data.total_positive*10;
	va_wtotal_R1.data_link->send_val_to_hmi = mag_3.data.total_positive*10;
}
void caculate_water_tank_expect_empty_time() {
	uint16_t water_left_in_persentage = *va_tank_now.data_link->send_val_to_modbus_ptr;
	va_tank_timer.data_link->send_val_to_hmi = 0;
}
void updata_pto_status() {
	int index = 0;
	bool pto_status = modbus_data_array[0] == 1 ? true: false;
	while (mags[index] != NULL)
	{
		mags[index++]->set_pto_status(pto_status);
	}
}
#endif // UBRR3H
