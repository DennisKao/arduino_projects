﻿//
// Created by elijah on 2019/3/4.
//

#ifndef LIBRARIES_READMODBUS_H
#define LIBRARIES_READMODBUS_H
#endif //LIBRARIES_READMODBUS_H
#include "Arduino.h"

#ifndef MODBUS_SERIAL
#define MODBUS_SERIAL Serial1
#endif

#ifndef MODBUS_PIN
#define MODBUS_PIN 23
#endif

#ifndef DAMPING_TIMES
#define DAMPING_TIMES 10		//取平均次數  正常平均次數(2000ms/次
#endif

#ifndef CALIBRATION_LEN
#define CALIBRATION_LEN 5
#endif // CALIBRATION_LEN

#define FLOW_EMPTY_RATE_PARTITION 4500
#define DEBUG_MESSAGE // 若不需要查看debug訊息 應註解此行

HardwareSerial *modbus_port = &MODBUS_SERIAL;
// ---------------------------------------
// -------------輔助函數-----------------
/**
 * 網路上抓的  列印浮點數的函數
 * @param val   浮點數
 * @param precision     要顯示的位數
 */
void printDouble(double val, byte precision) {
	// prints val with number of decimal places determine by precision
	// precision is a number from 0 to 6 indicating the desired decimial places
	// example: lcdPrintDouble( 3.1415, 2); // prints 3.14 (two decimal places)

	if (val < 0.0) {
		Serial.print('-');
		val = -val;
	}

	Serial.print(int(val));  //prints the int part
	if (precision > 0) {
		Serial.print("."); // print the decimal point
		unsigned long frac;
		unsigned long mult = 1;
		byte padding = precision - 1;
		while (precision--)
			mult *= 10;

		if (val >= 0)
			frac = (val - int(val)) * mult;
		else
			frac = (int(val) - val) * mult;
		unsigned long frac1 = frac;
		while (frac1 /= 10)
			padding--;
		while (padding--)
			Serial.print("0");
		Serial.print(frac, DEC);
	}
}
// ---------------------------------------

// ---------------------------------------
// -----------------class-----------------
/**
 * @struct 紀錄每台流量計的資料集合
 */
struct LMagData {
	float instant_flow_value;   // 瞬時流量(公升/s
	float instant_flow_rate;    // 瞬時流速
	float damping_flow_value;		// 阻尼流量(公升/s
	float damping_flow_rate;		// 阻尼流速
	double total_positive; // 正向累積
};

struct Calibration
{
	float flow_rate;
	float calibration_amplification;
};

class ReadLMag {
public:
	float amplification = 1.0;	// 機器係數
	/**
	* @param		slave_id			設備id
	* @param		amplification 機器係數
	* @param		d					口徑 單位mm
	*/
	ReadLMag(int slave_id, float amplification, int d);
	LMagData data;  // 存放解析完畢的資料
	void print_data();
	void init(unsigned long now);
	void send_reset();   // 發送流量計歸零訊號
	int quick_change_detect_len = 4;		// 劇烈反應次數  修正檢查長度
	float quick_change_detect_diff = 0.5;		// 快速修正差異值
	/**
	* 設置多點校正
	* @param calibration 校正的數值
	* @param index 第幾筆校正設定
	*/
	void set_calibration(Calibration calibration, int index);
	void show_calibration();

public:
	void send_request();    // 發送詢問
	void request_timeout(unsigned long now); // 等待超時
	void receive_data(unsigned long now, uint16_t data_input[50]); // 接收訊號
	void test();    // 測試用
	// test func
	void insert_test_data(float data_array[DAMPING_TIMES]) {
		for (int i = 0; i < DAMPING_TIMES; i++) {
			damping_flow_rate_array[i] = data_array[i];
		}
		this->damping_flow_rate_array_write_index = DAMPING_TIMES - 1;
		this->data.instant_flow_rate = damping_flow_rate_array[DAMPING_TIMES - 1];
	}
	void show_test_damping_flow_rate() {
		this->caculate_damping_flow_rate(10000);
	}
	void set_pto_status(bool enable);
private:
	double total = 0;      // 總流量(---------------
	int pin;    // rs-485 to ttl 通訊pin
	int slave_id;   // 現在詢問的設備id
	const int damping_flow_rate_array_len = DAMPING_TIMES;
	float damping_flow_rate_array[DAMPING_TIMES] = { 0 };		 // 時間阻尼陣列
	unsigned long  damping_flow_rate_array_time_stamp = 0;
	int damping_flow_rate_array_write_index = 0;
	int dramatic_changes_detect_count = 0;
	float dramatic_changes_detect_buff = 0;
	Calibration calibrations[CALIBRATION_LEN] = { {0, 0} };  // 多點校正
	uint16_t return_buffer[50];     // 回傳值緩存區
	uint16_t request_cmd[8] = { 0x01, 0x04, 0x10, 0x10, 0x00, 0x0a, 0x74, 0xC1 };
	unsigned long last_add_time = 0;       // 上次累加流量時間
	/**
	 * 解析返回的數值
	 */
	void decode_data();
	/**
	 * 累計總流量
	 * @param now   現在的時間
	 */
	void updata_total_water_value(unsigned long now);
	/**
	 * 用4個16進制轉換成浮點數
	 * @param index  資料來源起點
	 * @return
	 */
	float get_float_from(int index);
	/**
	 * 用4個16進制轉換成浮點數
	 * @param part_a
	 * @param part_b
	 * @param part_c
	 * @param part_d
	 * @return
	 */
	float get_float(uint16_t part_a, uint16_t part_b, uint16_t part_c, uint16_t part_d);
	int d = 65; // 管道直徑 單位:mm
	/**
	 * 計算驗正碼
	 * @param u8length  資料長度
	 * @return 驗正碼
	 */
	uint16_t calcCRC(uint8_t u8length, const uint16_t data_list[]);
	/**
	* 計算阻尼流速
	* @param now 現在時間
	*/
	void caculate_damping_flow_rate(unsigned long now);
	static int sort_desc(const void *cmp1, const void *cmp2);	// 排序用
	/* 
	* 判斷是否為空管  流速10以上 導電三千以上  流量百分比400以上
	* @return bool true if empty
	*/
	bool pipe_is_empty();
	bool pto_enable = false; // 表示PTO是否開啟中
};

ReadLMag::ReadLMag(int slave_id, float amplification, int d) {
	this->pin = MODBUS_PIN;
	this->slave_id = slave_id;
	this->amplification = amplification;
	this->d = d;
	// 歸零
	this->data.instant_flow_rate = 0;
	this->data.instant_flow_value = 0;
	this->data.total_positive = 0;
	// 寫入slave id到請求命令
	request_cmd[0] = slave_id;
	// 寫入驗證碼
	uint16_t crc = this->calcCRC(6, this->request_cmd);
	request_cmd[6] = crc >> 8;
	request_cmd[7] = crc & 0x00ff;
}

void ReadLMag::test() {
	Serial.print(total / 1000);
	Serial.print("L  ");
	Serial.println();
}

void ReadLMag::set_pto_status(bool status) {
	this->pto_enable = status;
}

void ReadLMag::send_request() {
	//Serial.println("send_request");
	digitalWrite(this->pin, HIGH);
	for (int i = 0; i < 8; i++) {
		uint16_t t = *(this->request_cmd + i);
		modbus_port->write(t);
	}
	modbus_port->flush();
	delay(4);
	delayMicroseconds(200);
	digitalWrite(this->pin, LOW);
}

float ReadLMag::get_float_from(int index) {
	return get_float(
		return_buffer[index],
		return_buffer[index + 1],
		return_buffer[index + 2],
		return_buffer[index + 3]
	);
}

void ReadLMag::decode_data() {
	float calibration = 1.0;
	float instant_flow_rate = get_float_from(7) * this->amplification;
	float current_calibration_flow_rate = 0;

	if (this->pipe_is_empty()) {
		instant_flow_rate = 0;
#ifdef DEBUG_MESSAGE
		Serial.print("Empty: ");
		Serial.println(return_buffer[0]);
#endif //DEBUG_MESSAGE
	}
	
	// 流量校正
	for (int i = 0; i < CALIBRATION_LEN; i++) {
		if (this->calibrations[i].flow_rate != 0 &&
			this->calibrations[i].flow_rate < instant_flow_rate &&
			this->calibrations[i].flow_rate > current_calibration_flow_rate) {
			current_calibration_flow_rate = this->calibrations[i].flow_rate;
			calibration = this->calibrations[i].calibration_amplification;
		}
	}

	this->data.instant_flow_rate = instant_flow_rate * calibration;
	double value_pre_sec = this->data.instant_flow_rate*d*d*M_PI_4;
	this->data.instant_flow_value = value_pre_sec / 1000;
}

float ReadLMag::get_float(uint16_t part_a, uint16_t part_b, uint16_t part_c, uint16_t part_d) {
	union {
		unsigned long l;
		float f;
	}tmp;
	tmp.l = part_a;
	tmp.l = tmp.l << 8;
	tmp.l += part_b;
	tmp.l = tmp.l << 8;
	tmp.l += part_c;
	tmp.l = tmp.l << 8;
	tmp.l += part_d;
	return tmp.f;
}

void ReadLMag::send_reset() {
	Serial.println("send_reset to zero!!!");
	uint16_t reset_cmd[8] = { 0x01, 0x06, 0x00, 0x47, 0xA5, 0x5A, 0xC2, 0xB4 };
	reset_cmd[0] = this->slave_id;

	uint16_t crc = this->calcCRC(6, reset_cmd);
	reset_cmd[6] = crc >> 8;
	reset_cmd[7] = crc & 0x00ff;

	digitalWrite(this->pin, HIGH);

	for (int i = 0; i < 8; i++) {
		modbus_port->write(*(reset_cmd + i));
	}
	modbus_port->flush();
	digitalWrite(this->pin, LOW);
}

void ReadLMag::updata_total_water_value(unsigned long now) {
	/*
	* 這是自己計算總流量的code
	double value_pre_sec = this->data.instant_flow_rate*d*d*M_PI_4;
	double value = value_pre_sec * (now - this->last_add_time) / 1000;
	if (value > 0 && value < 1000000) {
		this->data.total_positive += value;
	};
	last_add_time = now;
	*/

	// 直接讀取流量計寄存器裡的數值
	uint32_t total_water_value_integer_part = return_buffer[19];
	total_water_value_integer_part = total_water_value_integer_part << 8;
	total_water_value_integer_part += return_buffer[20];
	total_water_value_integer_part = total_water_value_integer_part << 8;
	total_water_value_integer_part += return_buffer[21];
	total_water_value_integer_part = total_water_value_integer_part << 8;
	total_water_value_integer_part += return_buffer[22];
	float total_water_value_point_part = this->get_float_from(23);
	this->data.total_positive = total_water_value_integer_part + total_water_value_point_part;
}

void ReadLMag::print_data() {
	if (this->slave_id != 4) {
		return;
	}
	Serial.print(" ID:");
	Serial.print(this->slave_id);
	Serial.print(" 流速:");
	printDouble(this->data.instant_flow_rate, 5);
	//Serial.print(" D速:");
	//printDouble(this->data.damping_flow_rate, 5);
	Serial.print(" 流量:");
	printDouble(this->data.instant_flow_value * 60, 5);
	Serial.print(" D量:");
	printDouble(this->data.damping_flow_value * 60, 5);
	Serial.print(" 總量:");
	Serial.print(this->data.total_positive / 1000);
	Serial.print(" time: ");
	Serial.print(millis() / 1000);
	Serial.print(" sec");

	Serial.println();
}

void ReadLMag::init(unsigned long now) {
	last_add_time = now;
	this->damping_flow_rate_array_time_stamp = now;
}

uint16_t ReadLMag::calcCRC(uint8_t u8length, const uint16_t data_list[]) {
	unsigned int temp, temp2, flag;
	temp = 0xFFFF;
	for (unsigned char i = 0; i < u8length; i++)
	{
		temp = temp ^ data_list[i];
		for (unsigned char j = 1; j <= 8; j++)
		{
			flag = temp & 0x0001;
			temp >>= 1;
			if (flag)
				temp ^= 0xA001;
		}
	}
	// Reverse byte order.
	temp2 = temp >> 8;
	temp = (temp << 8) | temp2;
	temp &= 0xFFFF;
	// the returned value is already swapped
	// crcLo byte is first & crcHi byte is last
	return temp;
}

void ReadLMag::request_timeout(unsigned long now) {
	this->last_add_time = now;
	this->data.instant_flow_rate = 0;  // 沒聯絡到就當作0
	this->data.instant_flow_value = 0;
}

void ReadLMag::receive_data(unsigned long now, uint16_t data_input[50]) {
	if (data_input[0] != this->slave_id) { return; }
	for (int i = 0; i < 50; i++) {
		this->return_buffer[i] = data_input[i];
	}
	decode_data();
	updata_total_water_value(now);
	caculate_damping_flow_rate(now);
}

void ReadLMag::caculate_damping_flow_rate(unsigned long now) {
	const uint16_t sampling_time_delta = 500;
	const int noise_reduction_cut_len = 0;
	// 防呆
	if (now - this->damping_flow_rate_array_time_stamp < sampling_time_delta) { return; }
	if (this->damping_flow_rate_array_write_index >= this->damping_flow_rate_array_len) { this->damping_flow_rate_array_write_index = 0; }

	this->damping_flow_rate_array_time_stamp = now;
	this->damping_flow_rate_array[this->damping_flow_rate_array_write_index] = this->data.instant_flow_rate;


	// 複製然後排序 只取中段值來平均(降噪)
	float damping_flow_rate_array_copy[DAMPING_TIMES] = { 0 };
	for (int i = 0; i < this->damping_flow_rate_array_len; i++) {
		damping_flow_rate_array_copy[i] = this->damping_flow_rate_array[i];
	}

	qsort(damping_flow_rate_array_copy,
		this->damping_flow_rate_array_len,
		sizeof(damping_flow_rate_array_copy[0]),
		this->sort_desc);


	float sum = 0;
	int start_index = noise_reduction_cut_len;
	int end_index = this->damping_flow_rate_array_len - noise_reduction_cut_len;
	start_index = start_index > this->damping_flow_rate_array_len ? 0 : start_index;
	end_index = end_index < 0 ? 0 : end_index;

	for (int i = start_index; i < end_index; i++) {
		sum += damping_flow_rate_array_copy[i];
	}
	this->data.damping_flow_rate = sum / (end_index - start_index);
	double value_pre_sec = this->data.damping_flow_rate*d*d*M_PI_4;
	this->data.damping_flow_value = value_pre_sec / 1000;

	// 決定是否要快速改值
	//const int quick_change_detect_len = 6;
	//const float quick_change_detect_diff = 1.0;
	const int cut_len = 1;
	float  *quick_change_detect_array = new float[this->quick_change_detect_len];
	float quick_change_sum = 0;
	for (int i = 0; i < quick_change_detect_len; i++) {
		int quick_change_index = this->damping_flow_rate_array_write_index - i;
		quick_change_index = quick_change_index < 0 ? this->damping_flow_rate_array_len + quick_change_index : quick_change_index;
		quick_change_detect_array[i] = this->damping_flow_rate_array[quick_change_index];
		//Serial.print(quick_change_index);
		//Serial.print("\t");
	}

	// 排序降噪
	qsort(quick_change_detect_array,
		quick_change_detect_len,
		sizeof(damping_flow_rate_array_copy[0]),
		this->sort_desc);


	for (int i = cut_len; i < quick_change_detect_len - cut_len; i++) {
		quick_change_sum += quick_change_detect_array[i];
	}
	float quick_change_avg = quick_change_sum / (quick_change_detect_len - 2 * cut_len);
	if (abs(this->data.damping_flow_rate - quick_change_avg) > quick_change_detect_diff) {
		this->data.damping_flow_rate = quick_change_avg;
		for (int i = 0; i < this->damping_flow_rate_array_len; i++) {
			//this->damping_flow_rate_array[i] = this->data.instant_flow_rate;	  //暫時移除快速改值
		}
		Serial.println("快速改值");
	}

	this->damping_flow_rate_array_write_index++;
	delete quick_change_detect_array;
#ifdef DEBUG_MESSAGE
	this->print_data();
#endif // DEBUG_MESSAGE

	
}

int ReadLMag::sort_desc(const void *cmp1, const void *cmp2)
{
	float a = *((float *)cmp1);
	float b = *((float *)cmp2);
	return a > b ? -1 : (a < b ? 1 : 0);
}

void ReadLMag::set_calibration(Calibration calibration, int index) {
	if (index >= CALIBRATION_LEN) { return; }
	this->calibrations[index] = calibration;
}
void ReadLMag::show_calibration() {
	for (int i = 0; i < CALIBRATION_LEN; i++) {
		Serial.print("流速");
		Serial.print(this->calibrations[i].flow_rate);
		Serial.print("\t\t校正");
		Serial.println(this->calibrations[i].calibration_amplification);
	}
}

bool ReadLMag::pipe_is_empty() {
	// 判斷是否為空管  流速15以上 導電三千以上  流量百分比400以上
	return !this->pto_enable;
}


// ---------------------------------------
// -------------放在ino裡----------------
// setup
void readLMagInit(unsigned long now, ReadLMag * ReadLMag_list[]) {
	modbus_port->begin(9600);
	pinMode(MODBUS_PIN, OUTPUT);
	if (ReadLMag_list[0] == NULL) {
		return;
	}
	int index = 0;
	while (ReadLMag_list[index] != NULL) {
		ReadLMag_list[index++]->init(now);
	}
}
// loop
int readLMagPoll(unsigned long now, ReadLMag * ReadLMag_list[]) {
	const unsigned long timeout = 300;
	static int current_index = 0;   // 正在處理的物件index
	enum STEP { SEND, WAIT, RECEIVE };
	static STEP step = SEND;    // 正在第幾個步驟
	static unsigned long time_stamp = 0;
	static unsigned long timeout_time_stamp;

	uint16_t return_buffer[50];

	// 一些防呆和overflow
	if (ReadLMag_list[current_index] == NULL) {
		current_index = 0;
	}
	if (ReadLMag_list[0] == NULL) {
		return;
	}


	if (now - time_stamp < 50) { return; }
	time_stamp = now;


	if (step == SEND) {  // 發送模式
		timeout_time_stamp = now;
		ReadLMag_list[current_index]->send_request();
		step = WAIT;
	}
	else if (step == WAIT) {     // 等待接收模式
		if (now - timeout_time_stamp > timeout) {     // 逾時
			step = SEND;
			ReadLMag_list[current_index]->request_timeout(now);
			current_index += 1;
		}
		else if (modbus_port->available()) {     // 開始有訊號傳入
			step = RECEIVE;
		}
	}
	else {
		int index = 0;
		while (modbus_port->available()) {
			return_buffer[index++] = modbus_port->read();
			delay(1);
		}
		//Serial.println(index);
		step = SEND;
		ReadLMag_list[current_index]->receive_data(now, return_buffer);
		current_index += 1;

		// debug
		if (index != 29) {
			Serial.print("data len wrong\t\t");
			Serial.print("id:");
			Serial.print(current_index);
			Serial.print("\t\t");
			Serial.print("data len:");
			Serial.println(index);
		}
		return (int)step;
	}
}

