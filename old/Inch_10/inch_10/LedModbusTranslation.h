// LedModbusTranslation.h

#ifndef _LEDMODBUSTRANSLATION_h
#define _LEDMODBUSTRANSLATION_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Wire.h>
struct ModbusData {
	byte led_mode_setting;
	byte led_speed;
	byte led_flash_light_display_mode;
	byte water_level;
};

//give a name to the group of data
ModbusData modbus_data;

//define slave i2c address
#define I2C_SLAVE_ADDRESS 8

void send_data_to_led_contriller(unsigned long now) {
	static unsigned long send_time_stamp = 0;
	if (now - send_time_stamp > 100) {
		send_time_stamp = now;
		Wire.beginTransmission(I2C_SLAVE_ADDRESS); // transmit to device #8
		Wire.write(modbus_data.led_mode_setting);
		Wire.write(modbus_data.led_speed);
		Wire.write(modbus_data.led_flash_light_display_mode);
		Wire.write(modbus_data.water_level);
		Wire.endTransmission();
	}
}

extern uint16_t modbus_data_array[];

void write_modbus_data_to_struct() {
	modbus_data.led_mode_setting = modbus_data_array[12];
	modbus_data.led_speed = modbus_data_array[13];
	modbus_data.led_flash_light_display_mode = modbus_data_array[14];
	modbus_data.water_level = modbus_data_array[6];
}


#endif

