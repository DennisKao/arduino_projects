#include "Nextion.h"
SoftwareSerial HMISerial(6, 7);
// 紀錄pin跟page的關係
typedef struct{
  int ping;
  uint8_t page_index;
}pin_page_pair;

// --------------------
// -----命名變數們-----
// 為了 bar demo
uint32_t v = 0;   
bool inc = false;
// 其他
const int buttonPin_3 = 3;     // 按鈕輸入針腳
const int buttonPin_4 = 4;
uint8_t page_now = 0; // 現在顯示頁面
uint8_t page_going = 0;  // 預計顯示頁面
char page_name_buffer[7]; // 頁面名字緩衝區
const int pin_page_pair_list_length = 2;
pin_page_pair pin_page_pair_list[pin_page_pair_list_length] = {{3,1}, {4,2}};
// --------------------

// -------------
// -----API-----
void bar_demo();    // 三種bar 依序補滿
void detect_page_button(); // 偵測換頁按鈕狀態
void change_page_if_need(); // 如果需要就下換頁指令
NexPage get_page_obj(uint8_t page_index); // 取得頁面物件
// -------------

// ------------------
// -----生命週期-----
void setup() {
  nexInit();
  NexPage p1 = get_page_obj(1);
  p1.show();
  pinMode(buttonPin_3, INPUT);
  pinMode(buttonPin_4, INPUT);
}

void loop() {
  detect_page_button();
  change_page_if_need();
}
// ------------------

// ------------------
// -----API 實作-----
void bar_demo() {
  NexProgressBar blue_bar = NexProgressBar(1, 1, "j0");
  NexProgressBar yellow_bar = NexProgressBar(1, 2, "j1");
  NexProgressBar green_bar = NexProgressBar(1, 3, "j2");
  if (v == 0 || v == 100) {
    inc = !inc;
  }

  if (inc == true) {
    v += 10;
  }
  else {
    v -= 10;
  }

  blue_bar.setValue(v);
  delay(50);
  yellow_bar.setValue(v);
  delay(50);
  green_bar.setValue(v);
  delay(100);
}

NexPage get_page_obj(uint8_t page_index){
  sprintf(page_name_buffer, "page%d", page_index);
  return NexPage(page_index, 0, page_name_buffer);
}

void detect_page_button(){
  for (int index = 0; index < pin_page_pair_list_length; index += 1){
    int state = digitalRead(pin_page_pair_list[index].ping);
    if (state == HIGH){
      page_going = pin_page_pair_list[index].page_index;
    }
  }
}

void change_page_if_need(){
  if (page_now != page_going){
    page_now = page_going;
    NexPage page_obj = get_page_obj(page_going);
    page_obj.show();
  }
}
// ------------------
