#define CLK_PIN 2 // 定義連接腳位
#define DT_PIN 24
#define SW_PIN 26
#define MAX_PAGE 255

volatile long count = 0;
volatile bool increasing = true;
unsigned long t = 0;

void setup() {
  Serial.begin(9600);
  // 當狀態下降時，代表旋轉編碼器被轉動了
  attachInterrupt(0, rotaryEncoderChanged, FALLING);
  pinMode(CLK_PIN, INPUT_PULLUP); // 輸入模式並啟用內建上拉電阻
  pinMode(DT_PIN, INPUT_PULLUP);
  pinMode(SW_PIN, INPUT_PULLUP);
  Serial.println("init");
}

void loop() {
  if (digitalRead(SW_PIN) == LOW) { // 按下開關，歸零
    count = 0;
    Serial.println("count reset to 0");
  }
}

void rotaryEncoderChanged() { // when CLK_PIN is FALLING
  //教學
  unsigned long temp = millis();
  if (temp - t < 10) // 靈敏度
    return;
  t = temp;

  // DT_PIN的狀態代表正轉或逆轉
  count += digitalRead(DT_PIN) == HIGH ? 1 : -1;

  // 限制輸出範圍
  if (count > MAX_PAGE) {
    count = 0;
  }
  else if (count < 0) {
    count = MAX_PAGE;
  }

  Serial.println(count);
}
