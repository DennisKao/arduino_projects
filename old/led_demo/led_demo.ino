#include <FastLED.h>

// How many leds in your strip?
#define NUM_LEDS 20
#define DATA_PIN 18

// Define the array of leds
CRGB leds[NUM_LEDS];

void setup() { 
	Serial.begin(9600);
    FastLED.addLeds<WS2812, DATA_PIN, RGB>(leds, 0 , NUM_LEDS);
	LEDS.setBrightness(100);
	FastLED.show();
	Serial.println("led test init");
}


int color_count = 0; // 0 = red, 1 = blue, 2=green
unsigned long time_stamp = 0;
int led_index_count = 0;
void loop() { 
	CRGB color;
	switch (color_count)
	{
	case 0:
		color = CRGB::Red;
		break;
	case 1:
		color = CRGB::Blue;
		break;
	case 2:
		color = CRGB::Green;
		break;
	default:
		color = CRGB::White;
	}
	//LEDS.clear();
	leds[led_index_count++] = CRGB::White;
	FastLED.show();

	led_index_count = led_index_count > NUM_LEDS ? 0 : led_index_count;
	delay(50);
}
