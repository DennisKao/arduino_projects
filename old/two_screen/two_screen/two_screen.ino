#include "Nextion.h"
NewSoftwareSerial HMISerial(6, 7);

NexPicture p0 = NexPicture(0, 1, "p0");
NexButton b0 = NexButton(0, 2, "b0");
uint32_t p0_pic_id = 0;

NexPicture pn0 = NexPicture(0, 1, "pn0");
NexButton bn0 = NexButton(0, 2, "pn0");
uint32_t pn0_pic_id = 0;

NexTouch *nex_listen_list[] = 
{
    &b0,
    NULL
};
NexTouch *nex_listen_list2[] = 
{
    &bn0,
    NULL
};

void setup() {
  nexInit();
  b0.attachPop(b0PopCallback, &b0);
  bn0.attachPop(bn0PopCallback, &bn0);
  //digitalWrite(13, HIGH);
}
int flag = 0;
int lis_times = 0;
int lis_times_max = 3000;
void loop() {
  if (flag == 0){
    if (lis_times == 0){
      digitalWrite(13, HIGH);
      HMISerial.end();
      HMISerial.Change_pins(8, 9);
      HMISerial.begin(9600);
      lis_times += 1;
    }
    else if (lis_times < lis_times_max){
      nexLoop(nex_listen_list2);
      lis_times += 1;
    }
    else{
      flag = 1;
      lis_times = 0;
    }
  }
  else {
    if (lis_times == 0){
      digitalWrite(13, LOW);
      HMISerial.end();
      HMISerial.Change_pins(6, 7);
      HMISerial.begin(9600);
      lis_times += 1;
    }
    else if (lis_times < lis_times_max){
      nexLoop(nex_listen_list);
      lis_times += 1;
    }
    else{
      flag = 0;
      lis_times = 0;
    }
  }
  
  //p0.setPic(1);
  //pn0.setPic(1);
}


void b0PopCallback(void *ptr){
  digitalWrite(13, HIGH);
  if (p0_pic_id == 0){
    p0_pic_id = 1;
  }
  else{
    p0_pic_id = 0;
  }
  p0.setPic(p0_pic_id);  
}

void bn0PopCallback(void *ptr){
  digitalWrite(13, HIGH);
  if (pn0_pic_id == 0){
    pn0_pic_id = 1;
  }
  else{
    pn0_pic_id = 0;
  }
  pn0.setPic(pn0_pic_id);  
}
