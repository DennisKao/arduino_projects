#include "HmiDataManager.h"
#define VIRTUAL_MODBUS true   // 是否開啟模擬modbus功能
// 存檔功能
#define NEED_INIT_SAVE false // 是否需要初始化記憶體 只有第一次需要執行

// ---- Modbus ----
#include <ModbusRtu.h>
#define DATA_LEN 16 // mod bus 資料長度
#define BINARY_DATA_LEN 15
uint16_t au16data[DATA_LEN*2] = {0};  // 前半段是send 後半段是receive
uint16_t binary_data[BINARY_DATA_LEN*2] = {0};
uint8_t u8state;
Modbus slave(1, 2, 40);

// ---- HMI ----
#include "Nextion.h"
uint16_t hmi_data[DATA_LEN] = {0};  // 比對mod bus 資料用
uint16_t hmi_binary_data[BINARY_DATA_LEN] = {0};  // 比對mod bus 資料用
unsigned long read_hmi_time_stamp = 0;
const unsigned long read_hmi_delay = 200;
unsigned long mod_bus_data_stamp = 0;
const unsigned long mod_bus_data_delay = 200;

// 一般按鈕物件
NexButton b_home = NexButton(1, 2, "b_home");
NexButton b_pto = NexButton(1, 3, "b_pto");
NexButton b_pto2 = NexButton(1, 4, "b_pto2");
NexButton b_rolldoor = NexButton(1, 15, "b_rolldoor");
NexButton b_head_lock = NexButton(1, 5, "b_head_lock");
NexButton b_light_h = NexButton(1, 6, "b_light_h");
NexButton b_light_side = NexButton(1, 7, "b_light_side");
NexButton b_light_flash = NexButton(1, 8, "b_light_flash");
NexButton b_light_inside = NexButton(1, 9, "b_light_inside");
NexButton b_aud_left = NexButton(1, 1, "b_aud_left");
NexButton b_aud_front = NexButton(1, 16, "b_aud_front");
NexButton b_aud_right = NexButton(1, 17, "b_aud_right");
NexButton b_speed_dwn = NexButton(5, 1, "b_speed_dwn");
NexButton b_speed_up = NexButton(5, 2, "b_speed_up");
NexButton b_mode_next = NexButton(5, 4, "b_mode_next");
NexButton b_mode_pre = NexButton(5, 5, "b_mode_pre");
// 變數
NexVariable va_battery = NexVariable(0, 2, "va_battery");
NexVariable va_b_voltage = NexVariable(0, 6, "va_b_voltage");    // 電壓 val
NexVariable va_head_lock = NexVariable(0, 10, "va_head_lock");   // 車頭的訊號
NexVariable va_tank_0 = NexVariable(0, 3, "va_tank_0");
NexVariable va_tank_1 = NexVariable(0, 4, "va_tank_1");
NexVariable va_tank_2 = NexVariable(0, 5, "va_tank_2");

NexVariable va_pto = NexVariable(0, 7, "va_pto");
NexVariable va_pto2 = NexVariable(0, 8, "va_pto2");
NexVariable va_rolldoor = NexVariable(0, 9, "va_rolldoor");
NexVariable va_light_h = NexVariable(0, 11, "va_light_h");
NexVariable va_light_side = NexVariable(0, 12, "va_light_side");
NexVariable va_light_flash = NexVariable(0, 13, "va_light_flash");
NexVariable va_light_in = NexVariable(0, 14, "va_light_in");
NexVariable va_light_set = NexVariable(0, 15, "va_light_set");
NexVariable va_aud_out = NexVariable(0, 22, "va_aud_out");
//NexVariable va_aud_front = NexVariable(0, 18, "va_aud_front");
//NexVariable va_aud_right = NexVariable(0, 19, "va_aud_right");

NexVariable va_led_mode = NexVariable(0, 20, "va_led_mode");   // led 模式
NexVariable va_led_speed = NexVariable(0, 21, "va_led_speed");   // led 閃爍速度
// 多數值按鈕物件
// --音源
NexButton b_aud_source = NexButton(1, 11, "b_aud_source");
NexVariable aud_set = NexVariable(1, 20 , "aud_set");
// --音量
NexButton b_vol_up = NexButton(1, 34, "b_vol_up");
NexButton b_vol_down = NexButton(1, 19, "b_vol_down");
NexVariable vol_set = NexVariable(1, 22, "vol_set");

// 頁面
NexPage p_home = NexPage(1, 0, "home");
NexPage p_alert = NexPage(2, 0, "alert");
// nextion 監聽用
NexTouch *nex_listen_list[DATA_LEN] =
        {
                // 一般按鈕物件
                &b_light_h,
                &b_light_side,
                &b_light_flash,
                &b_light_inside,
                &b_aud_left,
                &b_aud_front,
                &b_aud_right,
                // pages
                &p_home,
                // 多數值按鈕物件
                // --聲音
                &b_aud_source,
                &b_vol_up,
                &b_vol_down,
                &b_speed_dwn,
                &b_speed_up,
                &b_mode_next,
                &b_mode_pre,
                NULL
        };

// 紀錄物件之間的關係

BtnReference *ref_list[] = {
        new BtnReference(2, VAR_GLO, &va_battery, &va_battery),
        new BtnReference(3, VAR_GLO, &va_b_voltage, &va_b_voltage),
        new BtnReference(4, VAR_GLO, &va_tank_0 , &va_tank_0 ),    // 水位計
        new BtnReference(5, VAR_GLO, &va_tank_1 , &va_tank_1 ),
        new BtnReference(6, VAR_GLO, &va_tank_2 , &va_tank_2 ),
        new BtnReference(7, VAR, &b_aud_source, &aud_set),     // 音源鈕
        new BtnReferenceCycleMode(8, VAR, &b_vol_up, &vol_set, 1, 100, 20),     // 聲音鈕
        new BtnReferenceCycleMode(8, VAR, &b_vol_down, &vol_set, 0, 100, 20),   // 聲音鈕     
        new BtnReferenceCycleMode(9, VAR_GLO, &b_mode_next, &va_led_mode, 1, 3, 1),   // led 模式+
        new BtnReferenceCycleMode(9, VAR_GLO, &b_mode_pre, &va_led_mode, 0, 3, 1),   // led 模式-
        new BtnReferenceCycleMode(10, VAR_GLO, &b_speed_up, &va_led_speed, 1, 100, 20),   // 閃爍速度+
        new BtnReferenceCycleMode(10, VAR_GLO, &b_speed_dwn, &va_led_speed, 0, 100, 20),   // 閃爍速度-
        new BtnReferenceCycleMode(11, AVOID, &b_aud_left, &va_aud_out, 1, 0, 0),       // 避讓
        new BtnReferenceCycleMode(11, AVOID, &b_aud_front, &va_aud_out, 2, 0, 0),
        new BtnReferenceCycleMode(11, AVOID, &b_aud_right, &va_aud_out, 3, 0, 0),
        NULL
};

// 二進位欄位關係表
BtnReference *ref_binary_list[] = {
        new BtnReference(0, BTN, &b_home, &b_home),   // 首頁
        new BtnReference(1, BTN, &b_pto, &va_pto),
        new BtnReference(2, BTN, &b_pto2, &va_pto2),
        new BtnReference(3, BTN, &b_rolldoor, &va_rolldoor),
        new BtnReferenceCycleMode(4, HEAD_LOCK, &b_head_lock, &va_head_lock, 0, 0, 0),    // 車頭定位
        new BtnReference(5, BTN, &b_light_h, &va_light_h),  
        new BtnReference(6, BTN, &b_light_side, &va_light_side),
        new BtnReference(7, BTN, &b_light_flash, &va_light_flash),
        new BtnReference(8, BTN, &b_light_inside, &va_light_in),
        NULL
};
void check_data_attay();    // 比對陣列資料是否有不同

// BY8301
#define IO1 31
#define IO2 33
#define IO3 35
#define IO4 37
#define IO5 39
#define BUSY 41
#define ALERT 43    // 硬控警報因
#define AVOID_LEFT_PIN 45
#define AVOID_FRONT_PIN 47
#define AVOID_RIGHT_PIN 49
#define AUD_SOURCE_0 48
#define AUD_SOURCE_1 50
#define AUD_SOURCE_2 52

// 音效順序
enum Audio{AVOID_LEFT = 1, AVOID_FRONT, AVOID_RIGHT};
bool busy_status = false;

#include <avr/pgmspace.h> //http://www.nongnu.org/avr-libc/user-manual/group__avr__pgmspace.html 
//#include <toneAC.h> //plays on pins 9 & 10 for ATmega328 for mega 11 12
// ToneAC
byte sirenVolume = 1; //for ToneAC; from 0-10
const unsigned int SIREN_SLOW = 1000; //us; desired delta time between siren freq updates
const unsigned int SIREN_FAST = 5000; //us
unsigned int sirenPeriod = SIREN_SLOW; //us

void playSirenSound(byte volume = sirenVolume, unsigned int period_us = SIREN_SLOW);
//sine wave lookup table to produce the siren sound
static const unsigned int sineLookupTable[] PROGMEM = 
{
  //Sine wave lookup table generator: 
  // - https://daycounter.com/Calculators/Sine-Generator-Calculator.phtml
  //Number of points: 256
  //Max Amplitude: 255
  //Numbers per row: 8
  //Decimal
250,253,256,259,262,265,268,271,
275,278,281,284,287,290,293,296,
299,302,305,308,311,314,317,320,
323,326,328,331,334,337,340,343,
346,348,351,354,357,360,362,365,
368,371,373,376,379,381,384,386,
389,391,394,396,399,401,404,406,
409,411,413,416,418,420,422,425,
427,429,431,433,435,437,439,441,
443,445,447,449,451,453,454,456,
458,460,461,463,464,466,468,469,
470,472,473,475,476,477,479,480,
481,482,483,484,485,486,487,488,
489,490,491,492,493,493,494,495,
495,496,496,497,497,498,498,498,
499,499,499,500,500,500,500,500,499,499,
499,498,498,498,497,497,496,496,
495,495,494,493,493,492,491,490,
489,488,487,486,485,484,483,482,
481,480,479,477,476,475,473,472,
470,469,468,466,464,463,461,460,
458,456,454,453,451,449,447,445,
443,441,439,437,435,433,431,429,
427,425,422,420,418,416,413,411,
409,406,404,401,399,396,394,391,
389,386,384,381,379,376,373,371,
368,365,362,360,357,354,351,348,
346,343,340,337,334,331,328,326,
323,320,317,314,311,308,305,302,
299,296,293,290,287,284,281,278,
275,271,268,265,262,259,256,253,
250,247,244,241,238,235,232,229,
225,222,219,216,213,210,207,204,
201,198,195,192,189,186,183,180,
177,174,172,169,166,163,160,157,
154,152,149,146,143,140,138,135,
132,129,127,124,121,119,116,114,
111,109,106,104,101,99,96,94,
91,89,87,84,82,80,78,75,
73,71,69,67,65,63,61,59,
57,55,53,51,49,47,46,44,
42,40,39,37,36,34,32,31,
30,28,27,25,24,23,21,20,
19,18,17,16,15,14,13,12,
11,10,9,8,7,7,6,5,
5,4,4,3,3,2,2,2,
1,1,1,0,1,1,
1,2,3,4,
5,5,6,7,8,9,10,
11,12,13,14,15,16,17,18,
19,20,21,23,24,25,27,28,
30,31,32,34,36,37,39,40,
42,44,46,47,49,51,53,55,
57,59,61,63,65,67,69,71,
73,75,78,80,82,84,87,89,
91,94,96,99,101,104,106,109,
111,114,116,119,121,124,127,129,
132,135,138,140,143,146,149,152,
154,157,160,163,166,169,172,174,
177,180,183,186,189,192,195,198,
201,204,207,210,213,216,219,222,
225,229,232,235,238,241,244,247,
};
const unsigned int NUM_SINEWAVE_ELEMENTS = sizeof(sineLookupTable)/sizeof(unsigned int); 
unsigned long t_now= 0;
int speed1 = 0, step1 = 0;

void setup(void) {
    slave.begin(9600);
    // ---- Modbus ----
    
    // ---- HMI ----
    // 一般按鈕物件
    nexInit();
    b_light_h.attachPush(btn_type_callback, &b_light_h);
    b_light_side.attachPush(btn_type_callback, &b_light_side);
    b_light_flash.attachPush(btn_type_callback, &b_light_flash);
    b_light_inside.attachPush(btn_type_callback, &b_light_inside);
    b_aud_left.attachPush(avoid_callback, &b_aud_left);
    b_aud_front.attachPush(avoid_callback, &b_aud_front);
    b_aud_right.attachPush(avoid_callback, &b_aud_right);
    // 多數值按鈕物件
    // --聲音
    b_aud_source.attachPush(aud_callback, &b_aud_source);
    b_vol_up.attachPush(cycle_value_callback, &b_vol_up);
    b_vol_down.attachPush(cycle_value_callback, &b_vol_down);
    
    b_speed_dwn.attachPush(set_mod_callback, &b_speed_dwn);
    b_speed_up.attachPush(set_mod_callback, &b_speed_up);
    b_mode_next.attachPush(set_mod_callback, &b_mode_next);
    b_mode_pre.attachPush(set_mod_callback, &b_mode_pre);

    au16data[8] = 80;    // 預設音量
    // page
    p_home.attachPop(page_callback, &p_home);

    // ---- 存檔功能 ----
    init_save_mode(NEED_INIT_SAVE);
    read_mode();

    // ---- 音效 ----
    init_BY8301Controller();
    avoid_hardware_controll_init();
    Serial.begin(9600);
    Serial.println("init");

    // debug
    /*
    pinMode(8, INPUT);
    pinMode(9, INPUT);
    pinMode(10, INPUT);
    */
}

void loop(void) {
    /*
    virtual_modbus2();
    // 監聽螢幕按鈕
    nexLoop(nex_listen_list);
    
    // 把二進制資料寫入modbus陣列
    int data_buffer;
    data_buffer = encode_binary(BINARY_DATA_LEN, binary_data);
    au16data[0] = data_buffer;
    
    // 讀取 ModBus
    slave.poll(au16data, DATA_LEN*2);
    virtual_modbus();
    
    // 還原modbus陣列資料 到二進制資料陣列
    data_buffer = au16data[DATA_LEN];
    decode_binary(data_buffer, binary_data);
    
    // 更新資料到螢幕
    check_data_array();

    // 音效播放完畢
    bool busy_now = digitalRead(BUSY);
    if (busy_now != busy_status){
        if (busy_now){
            Serial.println("BY8301 busy!!");
        }else{
            Serial.println("BY8301 not busy!!");
            by8301_play_audio(0);
            char page_name[10] = "opening";
            va_aud_out.setGlobalValue(page_name, 0);
        }
        busy_status = busy_now;
    }
    // 警報音
    playSirenSound();
    
    // for playBeep1
    unsigned long t_now = micros(); //us

    avoid_hardware_controll();
    */
    // debug
    if (Serial.available()) {
        char cmd = Serial.read();
        if (cmd == 'p'){    // 看二進位資料詳細內容
           show_binary_data();
        }else if(cmd == 'b'){   // 看二進位資料內容 在10進位狀態的數值
            Serial.print("data_buffer: ");
            //Serial.println(data_buffer); 
        }else if(cmd == 'l'){   // 查看記錄模式在記憶體裡的數值
            LedBlinkModeSave mode_data = get_save_data(0);
            show_save_data(mode_data);
        }else if(cmd == 's'){
            Serial.print("aud source status: ");
            Serial.print(digitalRead(8));
            Serial.print(" ");
            Serial.print(digitalRead(9));
            Serial.print(" ");
            Serial.println(digitalRead(10));
        }else if (cmd == 'r'){
            output_aud_source_status(au16data[7]);
        }
        if (cmd == '1'){
            output_aud_source_status(1);
        }
        if (cmd == '2'){
            output_aud_source_status(2);
        }
        if (cmd == '3'){
            output_aud_source_status(3);
        }
        if (cmd == '0'){
            output_aud_source_status(0);
        }
    }

    //output_aud_source_status(au16data[7]);
}

// 檢查plc資料一致性
void check_data_array() {
    unsigned long now = millis();
    if ((now - mod_bus_data_stamp) < mod_bus_data_delay){
        return;
    }
    mod_bus_data_stamp = now;
    for (int index = 0; index < DATA_LEN; index++) {
        if (au16data[index+DATA_LEN] != hmi_data[index]) {  // 資料不同需要更新
            if(index == 0){   // 按鈕的2進位數值
                check_binary_data_array();
            }else{
                update_to_hmi(index);            
            }
        }
    }
}

void check_binary_data_array() {
    for (int index = 0; index < BINARY_DATA_LEN; index++) {
        if (binary_data[index+BINARY_DATA_LEN] != hmi_binary_data[index]) {  // 資料不同需要更新
            update_to_hmi_btn(index);
            //Serial.print("update_to_hmi_btn index: ");
            //Serial.println(index);
        }
    }
    hmi_data[0] = au16data[DATA_LEN];
}


// 更新plc資料到螢幕
void update_to_hmi(int index) {
    // 找尋關聯
    int ref_index=0;
    bool result_final = true;
    while(ref_list[ref_index] != NULL){
        BtnReference *ref_obj = ref_list[ref_index];
        if (ref_list[ref_index]->modbus_data_index == index) {
            // 分辨模式
            bool result = false;
            if (ref_obj->type == VAR) {
                NexVariable *var = (NexVariable *)ref_obj->ref;
                result = var->setValue(au16data[index+DATA_LEN]);
            }
            else if (ref_obj->type == VAR_GLO){
                NexVariable *var = (NexVariable *)ref_obj->ref;
                char page_name[10] = "opening";
                result = var->setGlobalValue(page_name, au16data[index+DATA_LEN]);
            }
            else if (ref_obj->type == NUM){
                NexNumber *var = (NexNumber *)ref_obj->ref;
                result = var->setValue(au16data[index+DATA_LEN]);
            }
            else{
                Serial.print("資料格式未定: ");
                Serial.println(ref_obj->type);
                result = true;
            }
            if (!result){
                result_final = false;
                Serial.print("錯誤 欄位: ");
                Serial.print(index);
                Serial.print(" type: ");
                Serial.print(ref_obj->type);
                Serial.print(" val: ");
                Serial.println(au16data[index]);
                NexObject *obj = (NexObject *)ref_obj->ref;
                obj->printObjInfo();
            }
            break;
        }
        ref_index++;
    }
    
    if (result_final || true){
        hmi_data[index] = au16data[index+DATA_LEN];
        au16data[index] = au16data[index+DATA_LEN];
    }
}


// 更新plc資料到螢幕 按鈕專用
void update_to_hmi_btn(int index){
    // 找尋關聯
    int ref_index=0;
    bool result_final = true;
    while(ref_binary_list[ref_index] != NULL){
        BtnReference *ref_obj = ref_binary_list[ref_index];
        bool result=true;
        if (ref_binary_list[ref_index]->modbus_data_index == index && ref_obj->type == BTN) {      
            NexVariable *var = (NexVariable *)ref_obj->ref;
            char page_name[10] = "opening";
            result = var->setGlobalValue(page_name, binary_data[index+BINARY_DATA_LEN]);
        }
        else if (ref_binary_list[ref_index]->modbus_data_index == index && ref_obj->type == HEAD_LOCK){
            NexVariable *var = (NexVariable *)ref_obj->ref;
            char page_name[10] = "opening";
            result = var->setGlobalValue(page_name, binary_data[index+BINARY_DATA_LEN]);

            BtnReferenceCycleMode* mode_ref_obj = (BtnReferenceCycleMode*)ref_obj;
            if (binary_data[index+BINARY_DATA_LEN] == 0){
                mode_ref_obj->status=0;
                p_home.show();
            }else if(mode_ref_obj->status == 0){
                mode_ref_obj->status=1;
                p_alert.show();
            }
         }
         if (!result){
              result_final = false;
              Serial.print("-錯誤 欄位: ");
              Serial.print(index);
              Serial.print(" type: ");
              Serial.print(ref_obj->type);
              Serial.print(" val: ");
              Serial.println(au16data[index]);
              NexObject *obj = (NexObject *)ref_obj->ref;
              obj->printObjInfo();
         }
         ref_index++;
    }
    hmi_binary_data[index] = binary_data[index+BINARY_DATA_LEN];
}

// 設定為需要更新
void set_need_update(void *ptr){}

// 找尋關聯檔案
bool find_ref(void *ptr, int *result_index, BtnReference *ref_list[]){
    int ref_index = 0;
    while(ref_list[ref_index] != NULL){
        if(ref_list[ref_index]->btn == ptr){
            *result_index = ref_index;
            return true;
        }
        ref_index++;
    }
    return false;
}
bool find_ref(void *ptr, int *result_index){
    return find_ref(ptr, result_index, ref_list);
}
// 已切換頁面
void page_callback(void *ptr){
    Serial.println("頁面載入完成");
    // 把hmi資料歸零
    for(int i=0; i<DATA_LEN; i++){
        hmi_data[i] = 0;
    }
    for(int i=0; i<BINARY_DATA_LEN; i++){
        hmi_binary_data[i] = 0;
    }
    // 比對資料
    check_data_array();
}

// 更改modbus預備送出的值(au16data 前半段) 切換 1 & 0
void btn_type_callback(void *ptr){
    int ref_index = 0;
    if(!find_ref(ptr, &ref_index, ref_binary_list)){
        Serial.println("find index error");
        return;
    }
    Serial.print("ref_index: ");
    Serial.println(ref_index);
    BtnReference *ref_obj = ref_binary_list[ref_index];
    
    // 放上新資料
    int data_index = ref_obj->modbus_data_index;
    int new_data = binary_data[data_index+BINARY_DATA_LEN] == 0 ? 1:0;    // 變更後半段的值送到前面
    binary_data[data_index] = new_data;

    //Serial.print("new data: ");
    //Serial.println(new_data);
}

// 聲音源
void aud_callback(void *ptr){
    int ref_index;
    if(!find_ref(ptr, &ref_index)){
        Serial.println("find index error");
        return;
    }
    BtnReference *ref_obj = ref_list[ref_index];

    // 放上新資料
    int data_index = ref_obj->modbus_data_index;
    int new_data = au16data[data_index+DATA_LEN] + 1;
    // 溢位檢查
    if (new_data > 3){
        new_data = 0;
    }
    output_aud_source_status(new_data);
    au16data[data_index] = new_data;
}

// 循環數值回呼
void cycle_value_callback(void *ptr){
    Serial.println("cycle_value_callback");
    int ref_index;
    if(!find_ref(ptr, &ref_index)){
        Serial.println("find index error");
        return;
    }
    BtnReferenceCycleMode *ref_obj = ref_list[ref_index];
    
    // 放上新資料
    int data_index = ref_obj->modbus_data_index;
    int new_data;
    //Serial.print("old data: ");
    //Serial.println(au16data[data_index+DATA_LEN]);
    if (ref_obj->status == 1){
        new_data = au16data[data_index+DATA_LEN] + ref_obj->rate;
    } else{
        new_data = au16data[data_index+DATA_LEN] - ref_obj->rate;
    }

    // 溢位檢查
    if (new_data > ref_obj->max){
        new_data = ref_obj->max;
    } else if (new_data < 0){
        new_data = 0;
    }
    au16data[data_index] = new_data;
    Serial.print("new data: ");
    Serial.println(new_data);
}

// 設定模式回呼
void set_mod_callback(void *ptr){
    cycle_value_callback(ptr);
  
    // 紀錄模式
    int mod_ref_index;
    find_ref(&b_mode_next, &mod_ref_index);
    BtnReferenceCycleMode *mod_ref_obj = ref_list[mod_ref_index];

    int speed_ref_index;
    find_ref(&b_speed_up, &speed_ref_index);
    BtnReferenceCycleMode *speed_ref_obj = ref_list[speed_ref_index];
    
    LedBlinkModeSave mode_data = {
        0, true,
        au16data[mod_ref_obj->modbus_data_index],
        100,
        au16data[speed_ref_obj->modbus_data_index]};
    put_save_data(mode_data, 0);
}

// 避讓回呼
void avoid_callback(void *ptr){
    // 找關聯
    int mod_ref_index = 0;
    find_ref(ptr, &mod_ref_index);
    BtnReferenceCycleMode *mod_ref_obj = ref_list[mod_ref_index];
    // 寫入數值
    binary_data[mod_ref_obj->modbus_data_index] = mod_ref_obj->status;
    
    // 螢幕變色
    NexVariable *var = (NexVariable *)mod_ref_obj->ref;
    char page_name[10] = "opening";
    var->setGlobalValue(page_name, mod_ref_obj->status);
    // 播放音效
    by8301_play_audio(mod_ref_obj->status);
}

// 避讓硬控按鈕
void avoid_hardware_controll(){
    if (digitalRead(AVOID_LEFT_PIN) == HIGH){
        avoid_callback(&b_aud_left);
    }
    if (digitalRead(AVOID_FRONT_PIN) == HIGH){
        avoid_callback(&b_aud_front);
    }
    if (digitalRead(AVOID_RIGHT_PIN) == HIGH){
        avoid_callback(&b_aud_right);
    }
}
void avoid_hardware_controll_init(){
    pinMode(AVOID_LEFT_PIN, INPUT);   // 左
    pinMode(AVOID_FRONT_PIN, INPUT);   // 前
    pinMode(AVOID_RIGHT_PIN, INPUT);   // 右
}

// 讀取EEPROM裡的資料
void read_mode(){
    // 紀錄模式
    LedBlinkModeSave mode_data = get_save_data(0);
    int mod_ref_index;
    find_ref(&b_mode_next, &mod_ref_index);
    BtnReferenceCycleMode *mod_ref_obj = ref_list[mod_ref_index];

    int speed_ref_index;
    find_ref(&b_speed_up, &speed_ref_index);
    BtnReferenceCycleMode *speed_ref_obj = ref_list[speed_ref_index];

    au16data[mod_ref_obj->modbus_data_index] = mode_data.mode_code;
    au16data[speed_ref_obj->modbus_data_index] = mode_data.speed;
}

/**
 * @param data_len  資料長度
 * @param data  2進位資料陣列
 * @return  10進制數字
 */
int encode_binary(int data_len, const int data[]){
    int res = 0;
    int int_buff;
    for(int i = 0; i<data_len; i++){
        int_buff = data[i]%2;
        int_buff = int_buff << i;
        res += int_buff;
    }
    return res;
}

/**
 * @param data   輸入10進制數字
 * @param res_len  存放結果的緩衝區大小
 * @param start_index  放置結果的起點
 * @param res   存結果的陣列
 * @return  總結果長度
 */
void decode_binary(int data, int res[BINARY_DATA_LEN*2]){
    for(int index = BINARY_DATA_LEN; index<BINARY_DATA_LEN*2; index++){
        res[index] = data%2;
        data /= 2;
    }
}

// 模擬modbus 功能
void virtual_modbus(){
    if(!VIRTUAL_MODBUS){return;}
    for(int i = 0; i<DATA_LEN; i++){
        au16data[DATA_LEN+i] = au16data[i];
    }
}
void virtual_modbus2(){
    if(!VIRTUAL_MODBUS){return;}
    for(int i = 0; i<BINARY_DATA_LEN; i++){
        binary_data[i] = binary_data[i+BINARY_DATA_LEN];
    }
}
// 列印按鈕們的狀態
void show_binary_data(){
    Serial.println();
    Serial.println("-----------------------");
    Serial.println("send:");
    for(int i=0;i<BINARY_DATA_LEN;i++){
        Serial.print(binary_data[i]);
        Serial.print("  "); 
    }
    Serial.println();
    Serial.println("recive:");
    for(int i=BINARY_DATA_LEN;i<BINARY_DATA_LEN*2;i++){
        Serial.print(binary_data[i]);
        Serial.print("  ");
    }
    Serial.println();
    Serial.println("hmi:");
    for(int i=0;i<BINARY_DATA_LEN;i++){
        Serial.print(hmi_binary_data[i]);
        Serial.print("  "); 
    }
    Serial.println();
    Serial.println("-----------------------");
}

// 初始化音效設定
void init_BY8301Controller(){
    pinMode(IO1, OUTPUT);
    pinMode(IO2, OUTPUT);
    pinMode(IO3, OUTPUT);
    pinMode(BUSY, INPUT);
    pinMode(ALERT, INPUT);
    
    digitalWrite(IO1, 1);
    digitalWrite(IO2, 1);
    digitalWrite(IO3, 1);
}

// 播放聲音
void by8301_play_audio(Audio cmd){
    digitalWrite(IO1, cmd == 1 ? 0:1);
    digitalWrite(IO2, cmd == 2 ? 0:1);
    digitalWrite(IO3, cmd == 3 ? 0:1);
};

// 初始化輸出音源控制狀態
void init_aud_source(){
    pinMode(AUD_SOURCE_0, OUTPUT);
    pinMode(AUD_SOURCE_1, OUTPUT);
    pinMode(AUD_SOURCE_2, OUTPUT);
}

// 輸出音源狀態
void output_aud_source_status(int code){
    digitalWrite(AUD_SOURCE_0, (code == 1 ? 1:0));
    digitalWrite(AUD_SOURCE_1, (code == 2 ? 1:0));
    digitalWrite(AUD_SOURCE_2, (code == 3 ? 1:0));
}


//-------------------------------------------------------------------------------------------------
//playSirenSound
//-------------------------------------------------------------------------------------------------
void playSirenSound(byte volume, unsigned int period_us){
  // 啟動條件
  if (busy_status || digitalRead(ALERT) == LOW){
      noToneAC();
      return;
  }

  
  static unsigned long t_start = micros(); //us
  unsigned long t_now = micros(); //us
  if (t_now - t_start >= period_us)
  {
    t_start = t_now; //us; update
    static unsigned int sirenIndex = 0;
    unsigned int freq = pgm_read_word(sineLookupTable + sirenIndex); 
    sirenIndex++;
    if (sirenIndex >= NUM_SINEWAVE_ELEMENTS) 
    sirenIndex = 0; //start back at beginning of sine wave 
    toneAC(freq+635, volume);    
  }
}

void playBeep1(){
  //local vars
  static unsigned long t_start1 = micros(); //us
  if (t_now - t_start1 >= 5000)
  {
    t_start1 = t_now; //us; update
    speed1 =  speed1 + step1 ;
    if ((speed1 == 0) || (speed1 == 200)) {
      step1 = -step1; 
    }
    toneAC(550+speed1);
 }
}
