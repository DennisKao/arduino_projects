//
// Created by elijah on 2019/1/21.
//

#ifndef LIBRARIES_NEXANIMATE_H
#define LIBRARIES_NEXANIMATE_H

#include "NexTouch.h"
#include "NexHardware.h"

class NexAnimate: public NexTouch{
public:
    /**
     * @copydoc NexObject::NexObject(uint8_t pid, uint8_t cid, const char *name);
     */
    NexAnimate(uint8_t pid, uint8_t cid, const char *name);

    /**
     * Set en attribute of component
     *
     * @param number - 1 to enable & 0 to disable
     * @return true if success, false for failure
     */
    bool setEn(uint32_t number);
};


#endif //LIBRARIES_NEXANIMATE_H
