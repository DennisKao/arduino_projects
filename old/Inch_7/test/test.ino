#include "ReadLMag.h"
ReadLMag reader = ReadLMag(9);

void setup() {
    Serial.begin(9600);
    reader.reset();
    Serial.println("init");
}
unsigned long stamp = 0;
void loop() {
  
    unsigned long now = millis();
    
    bool res = reader.poll(now);
    
    if(now - stamp>3000){
        stamp = now;
        reader.test();
        reader.print_data();
    }
    
    
    
    if(Serial.available()){
        char cmd = Serial.read();
        if (cmd == 't'){
            bool res = reader.poll(now);
            Serial.println(res);
        }else if (cmd == 'p'){
            reader.test();
        }
    }
    
}
