#define IO1 31
#define IO2 33
#define IO3 35
#define IO4 37
#define IO5 39
#define BUSY 41

enum Audio{AVOID_LEFT = 1, AVOID_FRONT, AVOID_RIGHT};
bool busy = false;
// 初始化
void init_BY8301Controller(){
    pinMode(IO1, OUTPUT);
    pinMode(IO2, OUTPUT);
    pinMode(IO3, OUTPUT);
    pinMode(BUSY, INPUT);
    digitalWrite(IO1, 1);
    digitalWrite(IO2, 1);
    digitalWrite(IO3, 1);
}
// 播放聲音
void by8301_play_audio(Audio cmd){
    digitalWrite(IO1, cmd == 1 ? 0:1);
    digitalWrite(IO2, cmd == 2 ? 0:1);
    digitalWrite(IO3, cmd == 3 ? 0:1);
};

void setup() {
    Serial.begin(9600);
    init_BY8301Controller();
    Serial.println("init!!");
}

void loop() {
  if (Serial.available()) {
      char cmd = Serial.read();
      if (cmd == '1'){
          by8301_play_audio(AVOID_LEFT);
      }
      if (cmd == '2'){
          by8301_play_audio(AVOID_RIGHT);
      }
      if (cmd == '3'){
          by8301_play_audio(AVOID_FRONT);
      }
  }

  bool busy_now = digitalRead(BUSY);
  if (busy_now != busy){
      if (busy_now){
          Serial.println("busy!!");
      }else{
          Serial.println("not busy!!");
      }
      busy = busy_now;
  }
}
