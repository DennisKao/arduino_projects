﻿#include <Nextion.h>
#include "ModbusRtu.h"

#define BINARY_DATA_LEN 15
#define DATA_LEN 16

uint16_t modbus_data[DATA_LEN*2] = {0};
uint16_t binary_data[BINARY_DATA_LEN * 2] = { 0 };
Modbus slave(2, 2, 34);

NexGauge z0 = NexGauge(0, 1, "z0");
NexGauge z1 = NexGauge(0, 2, "z1");
NexGauge z2 = NexGauge(0, 3, "z2");
NexGauge z3 = NexGauge(0, 4, "z3");
NexGauge z4 = NexGauge(0, 5, "z4");
NexGauge z5 = NexGauge(0, 6, "z5");
NexGauge z6 = NexGauge(0, 7, "z6");
NexGauge z7 = NexGauge(0, 8, "z7");
NexGauge z8 = NexGauge(0, 9, "z8");
NexGauge z9 = NexGauge(0, 10, "z9");
NexButton b0 = NexButton(0, 11, "b0");
NexButton b1 = NexButton(0, 12, "b1");

NexGauge *gauge_ptr_array[] = {
	&z0,
	&z1,
	&z2,
	&z3,
	/*&z4,
	&z5,
	&z6,
	&z7,
	&z8,
	&z9,*/
	NULL
};

NexTouch *nex_listen_list[] = {
	&b0,
	NULL
};
void setup() {
	Serial.begin(9600);
	slave.begin(9600);
	nexInit();
	b0.attachPush(b0_push, &b0);
	b0.attachPop(b0_pop, &b0);
	Serial.println("connect test init");
}


uint32_t color_code = 1000;
unsigned long st = 0;

void loop() {
	nexLoop(nex_listen_list);
	slave.poll(modbus_data, 32);
	decode_binary(modbus_data[16], binary_data);

	if (binary_data[6 + BINARY_DATA_LEN] == 1 && color_code == 1000) {
		color_code = 60000;
		Serial.println(millis() - st);
		b1.Set_background_color_bco(color_code);
		
	}
	else if (binary_data[6 + BINARY_DATA_LEN] == 0 && color_code == 60000) {
		color_code = 1000;
		Serial.println(millis() - st);
		b1.Set_background_color_bco(color_code);
		
	}
}

void b0_push(void *ptr) {
	Serial.print("push--");
	st = millis();
	modbus_data[0] = 64;
}

void b0_pop(void *ptr) {
	modbus_data[0] = 0;
}

/**
 * @param data_len  資料長度
 * @param data  2進位資料陣列
 * @return  10進制數字
 */
int encode_binary(int data_len, const uint16_t data[]) {
	int res = 0;
	int int_buff;
	for (int i = 0; i < data_len; i++) {
		int_buff = data[i] % 2;
		int_buff = int_buff << i;
		res += int_buff;
	}
	return res;
}

/**
 * @param data   輸入10進制數字
 * @param res_len  存放結果的緩衝區大小
 * @param start_index  放置結果的起點
 * @param res   存結果的陣列
 * @return  總結果長度
 */
void decode_binary(const int data, uint16_t res[BINARY_DATA_LEN * 2]) {
	int data_buffer = data;
	for (int index = BINARY_DATA_LEN; index < BINARY_DATA_LEN * 2; index++) {
		res[index] = data_buffer % 2;
		data_buffer /= 2;
	}
}