// The Arduino Pro Mini is a simple & cheap board, ideal for leaving inside of
// your model. It only has a single Serial port, and the default SBUS code
// doesn't run on it. You can get it to work by not using the timer, but that
// means you can't use delay in the loop() function anymore.

#include <SBUS.h>
SBUS sbus(Serial3);

void setup()
{
	Serial.begin(9600);
	sbus.begin(false);
}

void loop()
{
	sbus.process();
	for (int i = 1; i < 11; i++) {
		int value = sbus.getChannel(i);
		String res;
		res += "ch ";
		res += i;
		res += ":";
		res += value;
		res += "   ";
		Serial.print(res);
	}
	Serial.println();

	delay(200);
}
