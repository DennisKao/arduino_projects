#include "FastLED.h"
#define LIGHT_STRIP_NUMBER 34	// 燈條數量
#define LIGHT_SQUARE_NUMBER 256		// 燈片數量

CRGB leds_0[LIGHT_STRIP_NUMBER + LIGHT_SQUARE_NUMBER];

void setup() {
	Serial.begin(9600);
	LEDS.addLeds<WS2812, 2, RGB>(leds_0, 0, LIGHT_STRIP_NUMBER);    // 燈條
	LEDS.addLeds<WS2812, 18, RGB>(leds_0, LIGHT_STRIP_NUMBER, LIGHT_SQUARE_NUMBER);    // 燈片
	LEDS.setBrightness(100);
	LEDS.clear();
	Serial.println("led_test init");
}

const int light_square_color_list_len = 3;
CRGB light_square_color_list[light_square_color_list_len] = {
	CRGB(255, 255, 255),
	CRGB(255, 255, 255),
	CRGB(255, 255, 255)
};
//CRGB(255, 0, 0),
//CRGB(0, 255, 0),
//CRGB(0, 0, 255)

void loop() {
	unsigned long now = millis();
	light_strip_loop(now);
	light_square_loop(now);
	FastLED.show();
}

/*
*	下一個LED燈預計點亮位置，並在全部循環完畢時從頭開始
*/
int count_next_lightint_index(const int current_index) {
	int index = current_index;
	index++;
	if (index >= LIGHT_STRIP_NUMBER) {
		index = 0;
	}
	return index;
}
void turn_off_light_strip() {
	for (int i = 0; i < LIGHT_STRIP_NUMBER; i++) {
		leds_0[i] = CRGB(0, 0, 0);
	}
}
void light_current_index(int current_index) {
	leds_0[current_index] = CRGB(255, 255, 255);
}
void light_strip_loop(unsigned long now) {
	const unsigned long time_interval = 100;
	static unsigned long timestamp = 0;
	if (now < timestamp) { return; }
	timestamp = now + time_interval;

	static int light_strip_current_lightint_index = 0;
	turn_off_light_strip();
	light_current_index(light_strip_current_lightint_index);
	light_strip_current_lightint_index = count_next_lightint_index(light_strip_current_lightint_index);
}


void light_square(int color_index) {
	CRGB color = light_square_color_list[color_index];
	for (int i = 0; i < LIGHT_SQUARE_NUMBER; i++) {
		int led_index = LIGHT_STRIP_NUMBER + i;
		leds_0[led_index] = color;
	}
}
int count_next_color_index(const int color_index) {
	int index = color_index;
	index++;
	if (index >= light_square_color_list_len) {
		index = 0;
	}
	return index;
}
void light_square_loop(unsigned long now) {
	const unsigned long time_interval = 1000;
	static unsigned long timestamp = 0;
	if (now < timestamp) { return; }
	timestamp = now + time_interval;

	static int color_index = 0;
	light_square(color_index);
	color_index = count_next_color_index(color_index);
}