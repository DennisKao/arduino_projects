﻿// LedModbusTranslation.h

#ifndef _LEDMODBUSTRANSLATION_h
#define _LEDMODBUSTRANSLATION_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Wire.h>
struct ModbusData {
	byte led_mode_setting;
	byte led_speed;
	byte led_flash_light_display_mode;
	byte water_level;
	byte led_square_switch;
};
// 為了保證接收端格式一致，所以需要這個struct

//give a name to the group of data
ModbusData modbus_data;
extern ModbusModel *modbus_model;

//define slave i2c address
#define I2C_SLAVE_ADDRESS 8

void write_modbus_data_to_struct() {
	modbus_data.led_mode_setting = *modbus_model->led_display_mode;
	modbus_data.led_speed = *modbus_model->led_flash_speed;
	modbus_data.led_flash_light_display_mode = *modbus_model->led_flash_type;
	int water_leavel = *modbus_model->water_tank_level / 10;
	modbus_data.water_level = water_leavel;
	modbus_data.led_square_switch = *modbus_model->led_square_status;
}

void send_data_to_led_contriller(unsigned long now) {
	static unsigned long send_time_stamp = 0;
	if (now - send_time_stamp > 100) {
		write_modbus_data_to_struct();
		send_time_stamp = now;
		Wire.beginTransmission(I2C_SLAVE_ADDRESS); // transmit to device #8
		Wire.write(modbus_data.led_mode_setting);
		Wire.write(modbus_data.led_speed);
		Wire.write(modbus_data.led_flash_light_display_mode);
		Wire.write(modbus_data.water_level/10);
		Wire.write(modbus_data.led_square_switch);
		Wire.endTransmission();
	}
}




#endif

