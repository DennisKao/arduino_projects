// ModbusModel.h

#ifndef _MODBUSMODEL_h
#define _MODBUSMODEL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class ModbusModel {
public:
	uint16_t *pto;
	uint16_t *pump_pressure;
	uint16_t *temp_alert;
	/*uint16_t *mag_amplification_0;
	uint16_t *mag_amplification_1;
	uint16_t *mag_amplification_2;
	uint16_t *mag_amplification_3;*/
	uint16_t *water_tank_gate_status;
	uint16_t *water_tank_level;
	uint16_t *water_tank_capacity;
	uint16_t *pump_temp;
	uint16_t *pump_empty_alert;
	uint16_t *led_display_mode;
	uint16_t *led_flash_speed;
	uint16_t *led_flash_type;
	uint16_t *led_square_status;
	uint16_t *cafsgate1;
	uint16_t *cafsgate2;
	uint16_t *cclean1;
	uint16_t *cclean2;
	uint16_t *cafspump1;
	uint16_t *cafspump2;
	uint16_t *airgaug1;
	uint16_t *airgaug2;
	uint16_t *wcspeed1;
	uint16_t *wcspeed2;
	uint16_t *wctotal1;
	uint16_t *wctotal2;
	uint16_t *chespeed1;
	uint16_t *chespeed2;
	uint16_t *chetotal1;
	uint16_t *chetotal2;
	uint16_t *mixper1;
	uint16_t *mixper2;
	uint16_t *chetank_t;
	uint16_t *chetank_p;
	uint16_t *chefrom1;
	uint16_t *chefrom2;
	uint16_t *ptoalert;
	uint16_t *chepumpal;
	uint16_t *airtempal;
	uint16_t *cafspto;
	uint16_t *error_msg;
	uint16_t *mag_flow_value_0;
	uint16_t *mag_flow_value_1;
	uint16_t *mag_flow_value_2;
	uint16_t *mag_flow_value_3;
	uint16_t *mag_total_value_0;
	uint16_t *mag_total_value_1;
	uint16_t *mag_total_value_2;
	uint16_t *mag_total_value_3;
	uint16_t *estimated_end_time_min;
	uint16_t *estimated_end_time_sec;
	uint16_t *mag_total_flow;
	uint16_t *mag_total;

	ModbusModel(const int len, uint16_t *modbus_data_array) {
		this->pto = modbus_data_array + 0;
		this->pump_pressure = modbus_data_array + 1;
		this->temp_alert = modbus_data_array + 2;
		/*this->mag_amplification_0 = modbus_data_array + 3;
		this->mag_amplification_1 = modbus_data_array + 4;
		this->mag_amplification_2 = modbus_data_array + 5;
		this->mag_amplification_3 = modbus_data_array + 6;*/
		this->water_tank_gate_status = modbus_data_array + 3;
		this->water_tank_level = modbus_data_array + 4;
		this->water_tank_capacity = modbus_data_array + 5;
		this->pump_temp = modbus_data_array + 6;
		this->pump_empty_alert = modbus_data_array + 7;
		this->led_display_mode = modbus_data_array + 8;
		this->led_flash_speed = modbus_data_array + 9;
		this->led_flash_type = modbus_data_array + 10;
		this->led_square_status = modbus_data_array + 11;
		this->cafsgate1 = modbus_data_array + 12;
		this->cafsgate2 = modbus_data_array + 13;
		this->cclean1 = modbus_data_array + 14;
		this->cclean2 = modbus_data_array + 15;
		this->cafspump1 = modbus_data_array + 16;
		this->cafspump2 = modbus_data_array + 17;
		this->airgaug1 = modbus_data_array + 18;
		this->airgaug2 = modbus_data_array + 19;
		this->wcspeed1 = modbus_data_array + 20;
		this->wcspeed2 = modbus_data_array + 21;
		this->wctotal1 = modbus_data_array + 22;
		this->wctotal2 = modbus_data_array + 23;
		this->chespeed1 = modbus_data_array + 24;
		this->chespeed2 = modbus_data_array + 25;
		this->chetotal1 = modbus_data_array + 26;
		this->chetotal2 = modbus_data_array + 27;
		this->mixper1 = modbus_data_array + 28;
		this->mixper2 = modbus_data_array + 29;
		this->chetank_t = modbus_data_array + 30;
		this->chetank_p = modbus_data_array + 31;
		this->chefrom1 = modbus_data_array + 32;
		this->chefrom2 = modbus_data_array + 33;
		this->ptoalert = modbus_data_array + 34;
		this->chepumpal = modbus_data_array + 35;
		this->airtempal = modbus_data_array + 36;
		this->cafspto = modbus_data_array + 37;
		this->error_msg = modbus_data_array + 38;
		this->mag_flow_value_0 = modbus_data_array + 39;
		this->mag_flow_value_1 = modbus_data_array + 40;
		this->mag_flow_value_2 = modbus_data_array + 41;
		this->mag_flow_value_3 = modbus_data_array + 42;
		this->mag_total_value_0 = modbus_data_array + 43;
		this->mag_total_value_1 = modbus_data_array + 44;
		this->mag_total_value_2 = modbus_data_array + 45;
		this->mag_total_value_3 = modbus_data_array + 46;
		this->estimated_end_time_min = modbus_data_array + 47;
		this->estimated_end_time_sec = modbus_data_array + 48;
		this->mag_total_flow = modbus_data_array + 49;
		this->mag_total = modbus_data_array + 50;
		if (48 >= len) { Serial.println("modbus_data_array index out of range!!!"); }
	}
	
};

#endif

