#include "ModbusModel.h"
#include "ModbusPoll.h"
#include "LedModbusTranslation.h"
#ifdef UBRR3H	// 判斷是mega還是nano
#include "NextionController.h"
#include "NextionExtension.h"
#endif // UBRR3H



// ---- Modbus ----
//#include <ModbusRtu.h>
#define DATA_LEN 100
uint16_t modbus_data_array[DATA_LEN] = {0}; // 13 ~ 16 要給led指令用
ModbusModel *modbus_model;


uint16_t total_water_value_left = 3700;
#ifdef UBRR2H
#define MODBUS_SERIAL_PORT 2
#define MODBUS_CONTROL_PIN 31
#else
#define MODBUS_SERIAL_PORT 0
#define MODBUS_CONTROL_PIN 10
#endif // UBRR2H

Modbus slave(3, MODBUS_SERIAL_PORT, MODBUS_CONTROL_PIN);

#ifdef UBRR3H
NextionController nextion_controller = NextionController();

#endif // UBRR3H

void setup() {
	modbus_model = new ModbusModel(DATA_LEN, modbus_data_array);
	// modbus
	slave.begin(57600);
	//Led
	Wire.begin();
	// hmi
#ifdef UBRR3H
	Serial.begin(9600);
    nexInit();
	nextion_controller.attach_modbus_model(*modbus_model);
	Serial.println("10 inch v2.03 init");
#endif // UBRR3H
}


void loop() {
    // ---- Modbus ----
    slave.poll(modbus_data_array, DATA_LEN);
	send_data_to_led_contriller(millis());
	

#ifdef UBRR3H
	// 更新資料到螢幕
	nextion_controller.send_data_to_nextion_hmi_pool();
	debug_tools();
#endif // UBRR3H
	
}

void debug_tools() {
	if (Serial.available()) {
		char cmd[20] = { '\0' };
		int index = 0;
		while (Serial.available()) {
			cmd[index++] = Serial.read();
			delay(5);
		}
		if (strcmp(cmd, "help") == 0) {
			Serial.println("mag_data\t\t顯示流量計資料");
		}
		else if (strcmp(cmd, "modbus") == 0) {
			for (int i = 0; i < DATA_LEN; i++) {
				Serial.print(modbus_data_array[i]);
				Serial.print("  ");
			}
			Serial.println();
		}
		else if (strcmp(cmd, "pto") == 0) {
			modbus_data_array[0] = 1;
		}
	}
}



