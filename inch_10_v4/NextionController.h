// NextionController.h

#ifndef _NEXTIONCONTROLLER_h
#define _NEXTIONCONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
#include "NextionExtension.h"
#include "ModbusModel.h"
#include "ReadLMag.h"

class NextionController {
public:
	NextionController() {
		this->attach_all_hmi_user_func();
	}
	void send_data_to_nextion_hmi_pool() {
		const unsigned long cool_down_time = 50;
		static unsigned long timestamp = 0;
		unsigned long now = millis();
		if (now - timestamp < cool_down_time) { return; }
		int  index = 0;
		while (nex_display_list[index] != NULL)
		{
			NexVariableCostom *target = nex_display_list[index++];
			if (target->data_link->render_if_need()) {
				timestamp = now;
				return;
			}
		}
	}
	void attach_modbus_model(ModbusModel model) {
		va_pto.data_link->recive_val_from_modbus_ptr = model.pto;
		va_pump_p.data_link->recive_val_from_modbus_ptr = model.pump_pressure;
		va_temp_alert.data_link->recive_val_from_modbus_ptr = model.temp_alert;
		va_tank_gate.data_link->recive_val_from_modbus_ptr = model.water_tank_gate_status;
		va_tank_now.data_link->recive_val_from_modbus_ptr = model.water_tank_level;
		va_tank_total.data_link->recive_val_from_modbus_ptr = model.water_tank_capacity;
		va_pump_deg.data_link->recive_val_from_modbus_ptr = model.pump_temp;
		va_pump_empty.data_link->recive_val_from_modbus_ptr = model.pump_empty_alert;
		va_cafsgate1.data_link->recive_val_from_modbus_ptr = model.cafsgate1;
		va_cafsgate2.data_link->recive_val_from_modbus_ptr = model.cafsgate2;
		va_cclean1.data_link->recive_val_from_modbus_ptr = model.cclean1;
		va_cclean2.data_link->recive_val_from_modbus_ptr = model.cclean2;
		va_cafspump1.data_link->recive_val_from_modbus_ptr = model.cafspump1;
		va_cafspump2.data_link->recive_val_from_modbus_ptr = model.cafspump2;
		va_airgaug1.data_link->recive_val_from_modbus_ptr = model.airgaug1;
		va_airgaug2.data_link->recive_val_from_modbus_ptr = model.airgaug2;
		va_wcspeed1.data_link->recive_val_from_modbus_ptr = model.wcspeed1;
		va_wcspeed2.data_link->recive_val_from_modbus_ptr = model.wcspeed2;
		va_wctotal1.data_link->recive_val_from_modbus_ptr = model.wctotal1;
		va_wctotal2.data_link->recive_val_from_modbus_ptr = model.wctotal2;
		va_chespeed1.data_link->recive_val_from_modbus_ptr = model.chespeed1;
		va_chespeed2.data_link->recive_val_from_modbus_ptr = model.chespeed2;
		va_chetotal1.data_link->recive_val_from_modbus_ptr = model.chetotal1;
		va_chetotal2.data_link->recive_val_from_modbus_ptr = model.chetotal2;
		va_mixper1.data_link->recive_val_from_modbus_ptr = model.mixper1;
		va_mixper2.data_link->recive_val_from_modbus_ptr = model.mixper2;
		va_chetank_t.data_link->recive_val_from_modbus_ptr = model.chetank_t;
		va_chetank_p.data_link->recive_val_from_modbus_ptr = model.chetank_p;
		va_chefrom1.data_link->recive_val_from_modbus_ptr = model.chefrom1;
		va_chefrom2.data_link->recive_val_from_modbus_ptr = model.chefrom2;
		va_ptoalert.data_link->recive_val_from_modbus_ptr = model.ptoalert;
		va_chepumpal.data_link->recive_val_from_modbus_ptr = model.chepumpal;
		va_airtempal.data_link->recive_val_from_modbus_ptr = model.airtempal;
		va_cafspto.data_link->recive_val_from_modbus_ptr = model.cafspto;
		va_error_msg.data_link->recive_val_from_modbus_ptr = model.error_msg;
		va_wout_nL1.data_link->recive_val_from_modbus_ptr = model.mag_flow_value_0;
		va_wout_nL2.data_link->recive_val_from_modbus_ptr = model.mag_flow_value_1;
		va_wout_nR2.data_link->recive_val_from_modbus_ptr = model.mag_flow_value_2;
		va_wout_nR1.data_link->recive_val_from_modbus_ptr = model.mag_flow_value_3;
		va_wtotal_L1.data_link->recive_val_from_modbus_ptr = model.mag_total_value_0;
		va_wtotal_L2.data_link->recive_val_from_modbus_ptr = model.mag_total_value_1;
		va_wtotal_R2.data_link->recive_val_from_modbus_ptr = model.mag_total_value_2;
		va_wtotal_R1.data_link->recive_val_from_modbus_ptr = model.mag_total_value_3;
		va_tank_timer.data_link->recive_val_from_modbus_ptr = model.estimated_end_time_min;
		va_tank_timer2.data_link->recive_val_from_modbus_ptr = model.estimated_end_time_sec;
		va_wf_total.data_link->recive_val_from_modbus_ptr = model.mag_total_flow;
		va_wtotal.data_link->recive_val_from_modbus_ptr = model.mag_total;
	}
	//void set_mag_datas(ReadLMag mag_0, ReadLMag mag_1, ReadLMag mag_2, ReadLMag mag_3) {
	//	this->va_wout_nL1.data_link->send_val_to_hmi = mag_0.data.instant_flow_value * 60;
	//	this->va_wout_nL2.data_link->send_val_to_hmi = mag_1.data.instant_flow_value *60;
	//	this->va_wout_nR2.data_link->send_val_to_hmi = mag_2.data.instant_flow_value * 60;
	//	this->va_wout_nR1.data_link->send_val_to_hmi = mag_3.data.instant_flow_value * 60;

	//	// 更新累計流量到data link
	//	va_wtotal_L1.data_link->send_val_to_hmi = mag_0.data.total_positive*10/1000;	// *10 因為螢幕用了虛擬小數點, /1000 換算 L to m^3
	//	va_wtotal_L2.data_link->send_val_to_hmi = mag_1.data.total_positive*10/1000;
	//	va_wtotal_R2.data_link->send_val_to_hmi = mag_2.data.total_positive*10/1000;
	//	va_wtotal_R1.data_link->send_val_to_hmi = mag_3.data.total_positive*10/1000;
	//}
	/*void set_estimated_end_time(int estimated_end_time) {
		this->va_tank_timer.data_link->send_val_to_hmi = this->va_tank_timer2.data_link->send_val_to_hmi = estimated_end_time;
	}*/
private:
	NexPage page_opening = NexPage(0, 0, "opening");
	NexVariableCostom va_pto = NexVariableCostom(0, 1, "va_pto", new NexDataLink());
	NexVariableCostom va_pump_p = NexVariableCostom(0, 19, "va_pump_p", new NexDataLink());
	NexVariableCostom va_temp_alert = NexVariableCostom(0, 27, "va_temp_alert", new NexDataLink());
	NexVariableCostom va_wout_nR1 = NexVariableCostom(0, 3, "va_wout_nR1", new NexDataLink());
	NexVariableCostom va_wout_nR2 = NexVariableCostom(0, 4, "va_wout_nR2", new NexDataLink());
	NexVariableCostom va_wout_nL1 = NexVariableCostom(0, 5, "va_wout_nL1", new NexDataLink());
	NexVariableCostom va_wout_nL2 = NexVariableCostom(0, 6, "va_wout_nL2", new NexDataLink());
	NexVariableCostom va_wtotal_L1 = NexVariableCostom(0, 23, "va_wtotal_L1", new NexDataLink());
	NexVariableCostom va_wtotal_L2 = NexVariableCostom(0, 24, "va_wtotal_L2", new NexDataLink());
	NexVariableCostom va_wtotal_R1 = NexVariableCostom(0, 25, "va_wtotal_R1", new NexDataLink());
	NexVariableCostom va_wtotal_R2 = NexVariableCostom(0, 26, "va_wtotal_R2", new NexDataLink());
	NexVariableCostom va_tank_gate = NexVariableCostom(0, 13, "va_tank_gate", new NexDataLink());
	NexVariableCostom va_tank_total = NexVariableCostom(0, 11, "va_tank_total", new NexDataLink()); // 總水量
	NexVariableCostom va_tank_now = NexVariableCostom(0, 12, "va_tank_now", new NexDataLink()); // 目前剩餘水量百分比
	NexVariableCostom va_tank_timer = NexVariableCostom(0, 14, "va_tank_timer", new NexDataLink());// 剩餘時間(分
	NexVariableCostom va_tank_timer2 = NexVariableCostom(0, 20, "va_tank_timer2", new NexDataLink());// 剩餘時間(秒
	NexVariableCostom va_pump_rpm = NexVariableCostom(0, 15, "va_pump_rpm", new NexDataLink());
	NexVariableCostom va_pump_deg = NexVariableCostom(0, 16, "va_pump_temp", new NexDataLink());	//溫度
	NexVariableCostom va_pump_empty = NexVariableCostom(0, 17, "va_pump_empty", new NexDataLink());
	NexVariableCostom va_cafsgate1 = NexVariableCostom(0, 28, "va_cafsgate1", new NexDataLink());
	NexVariableCostom va_cafsgate2 = NexVariableCostom(0, 29, "va_cafsgate2", new NexDataLink());
	NexVariableCostom va_cclean1 = NexVariableCostom(0, 30, "va_cclean1", new NexDataLink());
	NexVariableCostom va_cclean2 = NexVariableCostom(0, 31, "va_cclean2", new NexDataLink());
	NexVariableCostom va_cafspump1 = NexVariableCostom(0, 32, "va_cafspump1", new NexDataLink());
	NexVariableCostom va_cafspump2 = NexVariableCostom(0, 33, "va_cafspump2", new NexDataLink());
	NexVariableCostom va_airgaug1 = NexVariableCostom(0, 34, "va_airgaug1", new NexDataLink());
	NexVariableCostom va_airgaug2 = NexVariableCostom(0, 35, "va_airgaug2", new NexDataLink());
	NexVariableCostom va_wcspeed1 = NexVariableCostom(0, 36, "va_wcspeed1", new NexDataLink());
	NexVariableCostom va_wcspeed2 = NexVariableCostom(0, 37, "va_wcspeed2", new NexDataLink());
	NexVariableCostom va_wctotal1 = NexVariableCostom(0, 38, "va_wctotal1", new NexDataLink());
	NexVariableCostom va_wctotal2 = NexVariableCostom(0, 39, "va_wctotal2", new NexDataLink());
	NexVariableCostom va_chespeed1 = NexVariableCostom(0, 40, "va_chespeed1", new NexDataLink());
	NexVariableCostom va_chespeed2 = NexVariableCostom(0, 41, "va_chespeed2", new NexDataLink());
	NexVariableCostom va_chetotal1 = NexVariableCostom(0, 42, "va_chetotal1", new NexDataLink());
	NexVariableCostom va_chetotal2 = NexVariableCostom(0, 43, "va_chetotal2", new NexDataLink());
	NexVariableCostom va_mixper1 = NexVariableCostom(0, 44, "va_mixper1", new NexDataLink());
	NexVariableCostom va_mixper2 = NexVariableCostom(0, 45, "va_mixper2", new NexDataLink());
	NexVariableCostom va_chetank_t = NexVariableCostom(0, 46, "va_chetank_t", new NexDataLink());
	NexVariableCostom va_chetank_p = NexVariableCostom(0, 47, "va_chetank_p", new NexDataLink());
	NexVariableCostom va_chefrom1 = NexVariableCostom(0, 48, "va_chefrom1", new NexDataLink());
	NexVariableCostom va_chefrom2 = NexVariableCostom(0, 49, "va_chefrom2", new NexDataLink());
	NexVariableCostom va_ptoalert = NexVariableCostom(0, 50, "va_ptoalert", new NexDataLink());
	NexVariableCostom va_chepumpal = NexVariableCostom(0, 51, "va_chepumpal", new NexDataLink());
	NexVariableCostom va_airtempal = NexVariableCostom(0, 52, "va_airtempal", new NexDataLink());
	NexVariableCostom va_cafspto = NexVariableCostom(0, 53, "va_cafspto", new NexDataLink());
	NexVariableCostom va_error_msg = NexVariableCostom(0, 54, "va_error_msg", new NexDataLink());
	NexVariableCostom va_wf_total = NexVariableCostom(0, 55, "va_wf_total", new NexDataLink());
	NexVariableCostom va_wtotal = NexVariableCostom(0, 56, "va_wtotal", new NexDataLink());

	NexPage page_main = NexPage(1, 0, "main_re");

	NexVariableCostom *nex_display_list[50] = {
	&va_pto,
	&va_pump_p,
	&va_temp_alert,
	&va_wout_nR1,
	&va_wout_nR2,
	&va_wout_nL1,
	&va_wout_nL2,
	&va_wtotal_L1,
	&va_wtotal_L2,
	&va_wtotal_R1,
	&va_wtotal_R2,
	&va_tank_now,
	&va_tank_gate,
	&va_tank_timer,
	&va_tank_timer2,
	&va_pump_rpm,
	&va_pump_deg,
	&va_pump_empty,
	&va_tank_total,
	&va_cafsgate1,
	&va_cafsgate2,
	&va_cclean1,
	&va_cclean2,
	&va_cafspump1,
	&va_cafspump2,
	&va_airgaug1,
	&va_airgaug2,
	&va_wcspeed1,
	&va_wcspeed2,
	&va_wctotal1,
	&va_wctotal2,
	&va_chespeed1,
	&va_chespeed2,
	&va_chetotal1,
	&va_chetotal2,
	&va_mixper1,
	&va_mixper2,
	&va_chetank_t,
	&va_chetank_p,
	&va_chefrom1,
	&va_chefrom2,
	&va_ptoalert,
	&va_chepumpal,
	&va_airtempal,
	&va_cafspto,
	&va_error_msg,
	&va_wf_total,
	&va_wtotal,
	NULL
	};
	static bool control_by_modbus(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == *var_ptr->data_link->recive_val_from_modbus_ptr) { return false; }
		var_ptr->data_link->current_data_of_hmi = *var_ptr->data_link->recive_val_from_modbus_ptr;
		char page_name[10] = "opening";
		var_ptr->setGlobalValue(page_name, *var_ptr->data_link->recive_val_from_modbus_ptr);
		return true;
	}
	static bool control_by_self(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return false; }
		var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
		char page_name[10] = "opening";
		var_ptr->setGlobalValue(page_name, var_ptr->data_link->send_val_to_hmi);
		return true;
	}
	static bool display_water_flow_value(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return false; }
		var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
		char page_name[10] = "opening";
		uint16_t send_val_to_hmi_cut_digit_in_ones = var_ptr->data_link->send_val_to_hmi / 10 * 10;		// 去掉個位數
		var_ptr->setGlobalValue(page_name, send_val_to_hmi_cut_digit_in_ones);
		return true;
	}
	static bool display_water_total_value(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return false; }
		var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
		char page_name[10] = "opening";
		uint16_t send_val_to_hmi_cut_digit_in_ones = var_ptr->data_link->send_val_to_hmi;
		var_ptr->setGlobalValue(page_name, send_val_to_hmi_cut_digit_in_ones);
		return true;
	}
	static bool display_left_time_minutes(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return false; }
		var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
		char page_name[10] = "opening";
		var_ptr->setGlobalValue(page_name, var_ptr->data_link->send_val_to_hmi / 60);
		return true;
	}
	static bool display_left_time_second(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == var_ptr->data_link->send_val_to_hmi) { return false; }
		var_ptr->data_link->current_data_of_hmi = var_ptr->data_link->send_val_to_hmi;
		char page_name[10] = "opening";
		var_ptr->setGlobalValue(page_name, var_ptr->data_link->send_val_to_hmi % 60);
		return true;
	}
	static bool va_tank_now_callback(void *var, void *null_ptr) {
		NexVariableCostom *var_ptr = (NexVariableCostom *)var;
		if (var_ptr->data_link->current_data_of_hmi == *var_ptr->data_link->recive_val_from_modbus_ptr) { return false; }
		var_ptr->data_link->current_data_of_hmi = *var_ptr->data_link->recive_val_from_modbus_ptr;
		char page_name[10] = "opening";
		var_ptr->setGlobalValue(page_name, var_ptr->data_link->current_data_of_hmi);
		return true;
	}
	void attach_all_hmi_user_func() {
		this->va_pto.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_pto, NULL);
		this->va_pump_p.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_pump_p, NULL);
		this->va_temp_alert.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_temp_alert, NULL);
		this->va_wout_nR1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wout_nR1, NULL);
		this->va_wout_nR2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wout_nR2, NULL);
		this->va_wout_nL1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wout_nL1, NULL);
		this->va_wout_nL2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wout_nL2, NULL);
		this->va_wtotal_L1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wtotal_L1, NULL);
		this->va_wtotal_L2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wtotal_L2, NULL);
		this->va_wtotal_R1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wtotal_R1, NULL);
		this->va_wtotal_R2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wtotal_R2, NULL);
		this->va_tank_now.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_tank_now, NULL);
		this->va_tank_total.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_tank_total, NULL);
		this->va_tank_gate.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_tank_gate, NULL);
		this->va_tank_timer.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_tank_timer, NULL);
		this->va_tank_timer2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_tank_timer2, NULL);
		this->va_pump_rpm.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_pump_rpm, NULL);
		this->va_pump_deg.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_pump_deg, NULL);
		this->va_pump_empty.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_pump_empty, NULL);
		this->va_cafsgate1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cafsgate1, NULL);
		this->va_cafsgate2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cafsgate2, NULL);
		this->va_cclean1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cclean1, NULL);
		this->va_cclean2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cclean2, NULL);
		this->va_cafspump1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cafspump1, NULL);
		this->va_cafspump2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cafspump2, NULL);
		this->va_airgaug1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_airgaug1, NULL);
		this->va_airgaug2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_airgaug2, NULL);
		this->va_wcspeed1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wcspeed1, NULL);
		this->va_wcspeed2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wcspeed2, NULL);
		this->va_wctotal1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wctotal1, NULL);
		this->va_wctotal2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wctotal2, NULL);
		this->va_chespeed1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chespeed1, NULL);
		this->va_chespeed2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chespeed2, NULL);
		this->va_chetotal1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chetotal1, NULL);
		this->va_chetotal2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chetotal2, NULL);
		this->va_mixper1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_mixper1, NULL);
		this->va_mixper2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_mixper2, NULL);
		this->va_chetank_t.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chetank_t, NULL);
		this->va_chetank_p.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chetank_p, NULL);
		this->va_chefrom1.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chefrom1, NULL);
		this->va_chefrom2.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chefrom2, NULL);
		this->va_ptoalert.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_ptoalert, NULL);
		this->va_chepumpal.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_chepumpal, NULL);
		this->va_airtempal.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_airtempal, NULL);
		this->va_cafspto.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_cafspto, NULL);
		this->va_error_msg.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_error_msg, NULL);
		this->va_wf_total.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wf_total, NULL);
		this->va_wtotal.data_link->attach_hmi_user_func(this->control_by_modbus, &this->va_wtotal, NULL);
	}

};

#endif

