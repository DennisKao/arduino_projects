#include <ModbusRtu.h>

// data array for modbus network sharing
uint16_t au16data[16] = {
  0, 1415, 9265, 4, 2, 7182, 28182, 8, 0, 0, 0, 0, 0, 0, 1, -1 };

Modbus slave(3, 0, 10); // this is slave @1 and RS-232 or USB-FTDI

void setup() {
	slave.begin( 57600 ); // baud-rate at 19200
	pinMode(13, OUTPUT);
	//Serial.begin(9600);
}

void loop() {
	slave.poll( au16data, 16 );
	digitalWrite(13, au16data[0] == 0 ? LOW:HIGH);
	//Serial.println(au16data[0]);
}
