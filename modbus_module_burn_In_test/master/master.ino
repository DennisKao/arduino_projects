#include <ModbusRtu.h>

uint16_t au16data[16] = {0};
uint8_t u8state;

Modbus master(0, 0, 10); // this is master and RS-232 or USB-FTDI


modbus_t telegram;

unsigned long u32wait;

void setup() {
	master.begin( 57600 ); // baud-rate at 19200
	master.setTimeOut( 200 ); // if there is no answer in 2000 ms, roll over
	u32wait = millis() + 300;
	u8state = 0; 
	pinMode(13, OUTPUT);
}

void loop() {
	switch( u8state ) {
	case 0: 
		if (millis() > u32wait) u8state++; // wait state
		break;
	case 1: 
		change_test_value();

		telegram.u8id = 3; // slave address
		telegram.u8fct = 16; // function code (this one is registers read)
		telegram.u16RegAdd = 0; // start address in slave
		telegram.u16CoilsNo = 1; // number of elements (coils or registers) to read
		telegram.au16reg = au16data; // pointer to a memory array in the Arduino

		master.query( telegram ); // send query (only once)
		u8state++;
		break;
	case 2:
		master.poll(); // check incoming messages
		if (master.getState() == COM_IDLE) {
			u8state = 0;
			u32wait = millis() + 50;
			uint8_t err_code = master.getLastError();
			light_control(err_code == 255);
		}
		break;
	}
}

void light_control(bool status) {
	digitalWrite(13, status ? HIGH:LOW);
}
void change_test_value() {
	au16data[0] = au16data[0] == 0 ? 1 : 0;
}