const unsigned long power_off_time = 5000;		// 電源開關時間(單位: ms)
const unsigned long power_on_time = 60000;
void(*resetFunc) (void) = 0;

void setup() {
	pinMode(10, OUTPUT);
	pinMode(13, OUTPUT);
	Serial.begin(9600);
}


void loop() {
	power_control();
}

unsigned long wait_time = power_off_time;
void power_control() {
	unsigned long now = millis();
	static unsigned long time_stamp = 0;

	if (now - time_stamp < wait_time) { return; }
	time_stamp = now;

	int power_status = digitalRead(10);
	digitalWrite(10, power_status == 0 ? 1 : 0);
	digitalWrite(13, power_status == 0 ? 0 : 1);
	
	wait_time = power_status == 0 ? power_off_time : power_on_time;
}